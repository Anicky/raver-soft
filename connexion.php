<?php
$pageTitre = "Connexion";
$pageConnexion = true;
include_once("haut.php");
require_once("acces-compte.php");
?>

<h1><?php echo $pageTitre; ?></h1>

<form method="post" action="connexion-effectuee.html" class="centre" id="formConnexion">
    <div class="contenuPage">
        <table class="formulaire">
            <tr>
                <td class="label">
                    <label for="login">Pseudo</label>
                </td>
                <td>
                    <input type="text" name="login" id="login" size="40" maxlength="100" />
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="password">Mot de passe</label>
                </td>
                <td>
                    <input type="password" name="password" id="password" size="40" maxlength="50" />
                </td>
            </tr>
        </table>
    </div>
    <input class="bouton" id="back" type="button" onclick="goToUrl('')" value="Annuler" />
    <input class="bouton" id="changePassword" type="button" onclick="goToUrl('mot-de-passe-oublie.html')" value="Mot de passe oublié" />
    <input type="hidden" name="tokenValidation" value="<?php echo $_SESSION['tokenValidation']; ?>" />
    <input class="bouton" type="submit" value="Valider" />
</form>
<div id="dialogbox">
    <?php require_once("loading.php"); ?>
</div>
<script>
    $(document).ready(function() {
        $("#dialogbox").dialog({
            autoOpen : false,
            modal : true,
            width : 500,
            resizable : false,
            draggable : false,
            show : "fade",
            hide : "fade",
            title : "Connexion",
            buttons : {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            }
        });
    });
    $("#formConnexion").validate({
        rules: {
            login : {
                required : true
            },
            password : {
                required : true
            }
        }, 
        messages: { 
            login : {
                required : "Vous devez indiquer votre pseudo."
            },
            password : {
                required : "Vous devez indiquer votre mot de passe."
            }
        } 
    });
    $("#formConnexion").ajaxForm({
        target: "#dialogbox",
        beforeSubmit : function() {
            $("#dialogbox").dialog('open');
        }
    });
</script>
<?php
include_once("google-analytics.php");
include_once("bas.php");
?>