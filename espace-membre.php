<div class="membre">
    <?php
    if (isset($_SESSION['utilisateur'])) {
        $requete = "SELECT * FROM membres WHERE id = ?";
        $reponse = $bdd->prepare($requete);
        $reponse->bindValue(1, $session_id, PDO::PARAM_INT);
        $reponse->execute();
        $donnees = $reponse->fetch();
        ?>
        <div id="compte">
            <ul>
                <li<?php
                if (isset($pageCompte)) {
                    echo " class=\"actif\"";
                }
                ?>>
                    <a href="compte.html" title="Gérer son compte">
                            <?php
                            if ($donnees['avatar'] != null) {
                                ?>
                            <img src="img/avatars/<?php echo $donnees['avatar']; ?>" alt="<?php echo $donnees['pseudo']; ?>" class="avatar_header" />
                            <?php
                        } else {
                            ?>
                            <img src="img/no_avatar.png" alt="<?php echo $donnees['pseudo']; ?>" class="avatar_header" />
                            <?php
                        }
                        ?>
                    </a>
                    <ul>
                        <li<?php
                        if (isset($pageCompte)) {
                            echo " class=\"actif\"";
                        }
                        ?>>
                            <a href="compte.html" title="Gérer son compte">
                                <img src="img/fleche1_droite.png" alt="" />
                                Compte
                            </a>
                        </li>
                        <li>
                            <a href="deconnexion.html" title="Se déconnecter">
                                <img src="img/fleche1_droite.png" alt="" />
                                Déconnexion
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <?php
        $reponse->closeCursor();
    } else {
        ?>
        <a href="connexion.html" title="Se connecter"<?php
        if (isset($pageConnexion)) {
            echo " id=\"actif\"";
        }
        ?> class="bouton">Connexion</a><?php if (getParametre($bdd, "inscription_ouverte")) { ?>
            <a class="bouton" href="inscription.html" title="Pas encore de compte ? S'inscrire"<?php
               if (isset($pageInscription)) {
                   echo " id=\"actif\"";
               }
               ?>>Inscription</a>
               <?php
           }
       }
       ?>
</div>