<?php

require_once("bdd_config.php");

if (isset($_POST['login'])) {
    // Variables
    $login = $_POST['login'];
    $password = $_POST['password'];
    // Traitement
    if ($_SESSION['tokenValidation'] == $_POST['tokenValidation']) {
        if ($login != "" || $password != "") {
            $requete = "SELECT membres.id, password, roles.nom AS nomRole, actif
                FROM membres, roles
                WHERE membres.id_role = roles.id
                AND membres.pseudo = ?";
            $reponse = $bdd->prepare($requete);
            $reponse->bindValue(1, $login, PDO::PARAM_STR);
            $reponse->execute();
            $donnees = $reponse->fetch();
            $ok = false;
            $token = "";
            if ($donnees != null) {
                if (testerMotDePasse($password, $donnees['password']) == $donnees['password'] && ($donnees['actif'])) {
                    $ok = true;
                    $token = creerToken();
                    $requeteToken = "UPDATE membres SET token = ? WHERE id = ?";
                    $reponseToken = $bdd->prepare($requeteToken);
                    $reponseToken->bindValue(1, $token, PDO::PARAM_STR);
                    $reponseToken->bindValue(2, $donnees['id'], PDO::PARAM_INT);
                    $reponseToken->execute();
                    $reponseToken->closeCursor();
                }
            }
            $reponse->closeCursor();
            if ($ok) {
                $_SESSION['utilisateur'] = $token;
                ?>
                <p>Vous êtes maintenant connecté !</p>
                <script>
                    window.location.href = "./";
                </script>
                <?php

            } else {
                ?>
                <p>
                    - Votre pseudo et/ou votre mot de passe sont peut-être incorrects.<br />
                    - Votre compte est peut-être inactif (vérifiez votre adresse e-mail pour effectuer l'étape de validation du compte)
                </p>
                <?php

            }
        } else {
            ?>
            <p>Vous n'avez pas rempli tous les champs.</p>
            <?php

        }
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>