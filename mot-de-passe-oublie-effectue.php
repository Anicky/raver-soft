<?php

require_once("bdd_config.php");
require_once("acces-compte.php");

if (isset($_POST['email'])) {
    // Variables
    $email = $_POST['email'];
    // Traitement
    if ($_SESSION['tokenValidation'] == $_POST['tokenValidation']) {
        if ($email != "") {

            $requete = "SELECT * FROM membres WHERE email = ?";
            $reponse = $bdd->prepare($requete);
            $reponse->bindValue(1, $email, PDO::PARAM_STR);
            $reponse->execute();
            $donnees = $reponse->fetch();
            if ($donnees != null) {
                $pseudo = $donnees['pseudo'];
                $password = $donnees['password'];
                ?>
                <script>
                    $("#dialogbox").bind('dialogclose', function() {
                        window.location.href = "./";
                    });
                </script>
                <?php

                require_once("fonctions-mails.php");
                if (mail_motDePasseOublie($pseudo, $password, $email)) {
                    ?>
                    <p>Un mail pour réinitialiser votre mot de passe vient de vous être envoyé.</p>
                    <?php

                } else {
                    ?>
                    <p>Le mail de réinitialisation de mot de passe n'a pas pu être envoyé.</p>
                    <?php

                }
            } else {
                ?>
                <p>Impossible de trouver votre email.</p>
                <?php

            }
            $reponse->closeCursor();
        } else {
            ?>
            <p>Vous n'avez pas rempli tous les champs.</p>
            <?php

        }
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>