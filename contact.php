<?php
$pageTitre = "Contact";
$pageContact = true;
include_once("haut.php");
?>

<h1>Nous contacter</h1>

<?php
$requete = "SELECT * FROM membres, roles WHERE membres.id_role = roles.id AND (roles.nom = 'Membre' OR roles.nom = 'Administrateur') ORDER BY nom ASC, pseudo ASC";
$reponse = $bdd->query($requete);

while ($donnees = $reponse->fetch()) {
    ?>
    <div class="contact_membre" id="<?php echo encodeUrl($donnees['pseudo']); ?>">
        <?php
        if ($donnees['avatar'] != null) {
            ?>
            <img src="img/avatars/<?php echo securite_sortie($donnees['avatar']); ?>" alt="Avatar" class="avatar" />
            <?php
        } else {
            ?>
            <img src="img/no_avatar.png" alt="Avatar" class="avatar" />
            <?php
        }
        ?>
        <div class="gauche">
            <div class="pseudo"><?php echo securite_sortie($donnees['pseudo']); ?></div>
            <br />
            <div class="role">
                <?php echo $donnees['nom']; ?>
            </div>
            <br />
            <div class="email">
                Email : <a href="mailto:<?php echo securite_sortie($donnees['email']); ?>" title="Envoyer un email au membre"><?php echo securite_sortie($donnees['email']); ?></a>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <?php
}
$reponse->closeCursor();

include_once("google-analytics.php");
include_once("bas.php");
?>