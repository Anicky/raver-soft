</section>
<div class="clear"></div>
</div> <!-- Fin Contenu -->

<footer>
    <p>
        <?php afficheTexte($bdd, "footer"); ?>
    </p>
</footer>

</div>

<?php
if (isset($bdd)) {
    $bdd = null;
}
ob_flush();
?>
<!--[if !IE]> <-->
<script>
    function loadFragment(name) {
        if (!name) {
            name = '';
        }
        var urlToLoad = 'ajax/' + name;
        $.get(urlToLoad, null, processNewContent);

        function processNewContent(newContent) {
            $('section').html(newContent);
        }
    }
    function getFragmentName() {
        return document.location.toString().replace("<?php echo URL; ?>", "");
    }
    function clickHandler(event) {
        var url = event.target.getAttribute('href');
        loadFragment(url);
        window.history.pushState({link: url}, '', url);
        window.scrollTo(0, 0);
        return event.preventDefault();
    }
    $(document).ready(
            function() {
                var landingPage = getFragmentName();
                if (!landingPage) {
                    landingPage = '';
                }
                window.history.replaceState({link: landingPage}, '', landingPage);
                $(window).bind('popstate',
                        function(event) {
                            if (event.originalEvent.state) {
                                loadFragment(event.originalEvent.state.link);
                            }
                        });
            }
    );
</script>
<!--> <![endif]-->
</body>
</html>
