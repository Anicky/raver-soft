<h1>News par catégories</h1>
<div class="contenuPage">
    <?php
    $requete = "SELECT nom FROM tags WHERE id IN (SELECT id_tag FROM news_tags, news WHERE news_tags.id_news = news.id AND publiee = true) ORDER BY nom ASC";
    $reponse = $bdd->query($requete);
    if ($reponse->rowCount() > 0) {
        ?>
        <ul class="listeSansPuces">
            <?php
            while ($donnees = $reponse->fetch()) {
                ?>
                <li><img src="img/fleche2_droite.png" alt="" />
                    <a href="categorie/<?php echo encodeUrl($donnees['nom']); ?>.html" title="<?php echo $donnees['nom']; ?>"><?php echo $donnees['nom']; ?></a></li>
                <?php
            }
            ?>
        </ul>
        <?php
    } else {
        ?>
        <p>Pas de catégories</p>
        <?php
    }
    $reponse->closeCursor();
    ?>
</div>