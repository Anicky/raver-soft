<?php
include_once "lib/markdown.php";
?>
<div class="contenuPage" id="tabs">
    <ul>
        <li><a href="<?php echo $_SERVER['REQUEST_URI']; ?>#tabs-1">Tâches réalisées</a></li>
        <li><a href="<?php echo $_SERVER['REQUEST_URI']; ?>#tabs-2">Tâches à faire</a></li>
        <li><a href="<?php echo $_SERVER['REQUEST_URI']; ?>#tabs-3">Bugs à corriger</a></li>
        <li><a href="<?php echo $_SERVER['REQUEST_URI']; ?>#tabs-4">Propositions d'améliorations</a></li>
    </ul>
    <div id="tabs-1">
        <?php
        // Affichage des tâches réalisées
        $request = json_decode(file_get_contents('https://bitbucket.org/api/2.0/repositories/Anicky/yllisan-skies/commits'));
        if (!empty($request->values)) {
            foreach ($request->values as $commit) {
                ?>
                <div class="avancement">
                    <div class="infos">
                        <div class="date">
                            <?php print date_format(date_create($commit->date), 'd/m/Y'); ?>
                        </div>
                        <div class="titre_avancement">
                            <?php print $commit->message; ?>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <?php
            }
        } else {
            ?>
            <p>Aucune pour le moment.</p>
            <?php
        }
        ?>
    </div>
    <div id="tabs-2">
        <?php
        // Affichage des tâches à faire
        $request = json_decode(file_get_contents('https://bitbucket.org/api/1.0/repositories/Anicky/yllisan-skies/issues?limit=50&kind=task&status=!resolved'));
        if (!empty($request->issues)) {
            foreach ($request->issues as $issue) {
                ?>
                <div class="avancement">
                    <div class="infos clickable">
                        <div class="date">
                            <?php print date_format(date_create($issue->created_on), 'd/m/Y'); ?>
                        </div>
                        <div class="titre_avancement">
                            <?php print $issue->title; ?>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <p>
                        <?php print Markdown($issue->content); ?>
                    </p>
                </div>
                <?php
            }
        } else {
            ?>
            <p>Aucune pour le moment.</p>
            <?php
        }
        ?>
    </div>
    <div id="tabs-3">
        <?php
        // Affichage des bugs
        $request = json_decode(file_get_contents('https://bitbucket.org/api/1.0/repositories/Anicky/yllisan-skies/issues?limit=50&kind=bug&status=!resolved'));
        if (!empty($request->issues)) {
            foreach ($request->issues as $issue) {
                ?>
                <div class="avancement">
                    <div class="infos clickable">
                        <div class="date">
                            <?php print date_format(date_create($issue->created_on), 'd/m/Y'); ?>
                        </div>
                        <div class="titre_avancement">
                            <?php print $issue->title; ?>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <p>
                        <?php print Markdown($issue->content); ?>
                    </p>
                </div>
                <?php
            }
        } else {
            ?>
            <p>Aucun pour le moment.</p>
            <?php
        }
        ?>
    </div>
    <div id="tabs-4">
        <?php
        // Affichage des améliorations
        $request = json_decode(file_get_contents('https://bitbucket.org/api/1.0/repositories/Anicky/yllisan-skies/issues?limit=50&kind=proposal&kind=enhancement&status=!resolved'));
        if (!empty($request->issues)) {
            foreach ($request->issues as $issue) {
                ?>
                <div class="avancement">
                    <div class="infos clickable">
                        <div class="date">
                            <?php print date_format(date_create($issue->created_on), 'd/m/Y'); ?>
                        </div>
                        <div class="titre_avancement">
                            <?php print $issue->title; ?>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <p>
                        <?php print Markdown($issue->content); ?>
                    </p>
                </div>
                <?php
            }
        } else {
            ?>
            <p>Aucune pour le moment.</p>
            <?php
        }
        ?>
    </div>
</div>
<script>
    $(function() {
        $("#tabs").tabs();
        $('.avancement .infos').click(function() {
            $(this).nextUntil('.avancement .infos').toggle('fast');
            return false;
        }).nextUntil('.avancement .infos').hide();
    });
</script>