<?php
$news_id = $donneesNews['news_id'];

$news_pseudo = "";
$news_avatar = "";
$news_email = "";
$auteur = securite_sortie($donneesNews['auteur']);
if ($auteur == null) {
    $requeteAuteurNews = "SELECT pseudo, avatar, email FROM membres WHERE id = ?";
    $reponseAuteurNews = $bdd->prepare($requeteAuteurNews);
    $reponseAuteurNews->bindValue(1, $donneesNews['id_auteur'], PDO::PARAM_INT);
    $reponseAuteurNews->execute();
    $donneesAuteurNews = $reponseAuteurNews->fetch();
    if ($donneesAuteurNews != null) {
        $news_pseudo = securite_sortie($donneesAuteurNews['pseudo']);
        $news_avatar = securite_sortie($donneesAuteurNews['avatar']);
        $news_email = securite_sortie($donneesAuteurNews['email']);
    }
    $reponseAuteurNews->closeCursor();
}

// Commentaires
$nombre_commentaires = nombreElements($bdd, "commentaires WHERE id_news = " . $news_id);
?>
<article>
    <?php if (isset($preview)) { ?>
        <h2>
            <a class="ajax" href="news/<?php echo $donneesNews['news_id'] . "-" . encodeUrl($donneesNews['titre']); ?>.html" title="Lire la news et les commentaires">
                <?php echo securite_sortie($donneesNews['titre']); ?>
            </a>
        </h2>
        <?php
    }
    ?>
    <div class="infos">
        <div class="date"><?php echo jourSemaine($donneesNews['jourSemaine']) . " " . $donneesNews['dateFormatee']; ?></div>
        <div class="auteur">Posté par
            <?php
            if ($auteur != null) {
                ?>
                <strong><?php echo $auteur; ?></strong>
                <?php
            } else if ($news_email != "") {
                ?>
                <a href="contact.html#<?php echo encodeUrl($news_pseudo); ?>" title="Voir plus d'informations sur le membre"><?php echo securite_sortie($news_pseudo); ?></a>
                <?php
            } else {
                ?>
                <strong><?php echo $news_pseudo; ?></strong>
                <?php
            }
            ?>
        </div>
        <div class="comments"<?php if (!isset($preview)) { echo " id=\"commentaires_nombre\""; } ?>>
            <?php
            require("commentaires-nombre.php");
            ?>
        </div>
        <div class="clear"></div>
    </div>
    <?php
    if (getParametre($bdd, "news_afficher_avatar") != 0) {
        ?>
        <a href="contact.html#<?php echo encodeUrl($news_pseudo); ?>" title="Voir plus d'informations sur le membre">
            <?php
            if ($news_avatar != null) {
                ?>
                <img src="img/avatars/<?php echo $news_avatar; ?>" alt="Avatar" class="avatar" />
                <?php
            } else {
                ?>
                <img src="img/no_avatar.png" alt="Avatar" class="avatar" />
                <?php
            }
            ?>
        </a>
        <?php
    }
    if (isset($preview)) {
        $nombre_mots = getParametre($bdd, "nombre_mots_preview");

        $preview = substr($donneesNews['texte'], 0, strpos($donneesNews['texte'], '[more]'));
        if (!$preview) {
            echo affichePreview($donneesNews['texte'], $nombre_mots);
        } else {
            echo $preview;
        }
    } else {
        echo str_replace("[more]", "", $donneesNews['texte']);
    }
    ?>
    <div class="clear"></div>
    <hr />
    <?php
    $requete3 = "SELECT nom FROM tags, news_tags WHERE news_tags.id_tag = tags.id AND id_news = ? ORDER BY nom ASC";
    $reponse3 = $bdd->prepare($requete3);
    $reponse3->bindValue(1, $news_id, PDO::PARAM_INT);
    $reponse3->execute();
    $nombre_tags = $reponse3->rowCount();
    if ($nombre_tags > 0) {
        $i = 0;
        ?>
        <div class="tags">
            <?php
            if ($nombre_tags == 1) {
                echo "Catégorie : ";
            } else {
                echo "Catégories : ";
            }
            while ($donnees3 = $reponse3->fetch()) {
                $i++;
                echo "<strong>" . securite_sortie($donnees3['nom']) . "</strong>";
                if ($i < $nombre_tags) {
                    echo ", ";
                }
            }
            $reponse3->closeCursor();
            ?>
        </div>
        <?php
    } if (isset($preview)) {
        ?>
        <div class="commentaires">
            <a class="ajax" href="news/<?php echo $donneesNews['news_id'] . "-" . encodeUrl($donneesNews['titre']); ?>.html" title="Lire l'article en entier">[Lire la suite de l'article]</a>
            <?php
            if ($donneesNews['commentaires']) {
                ?>
                ou
                <a class="ajax" href="news/<?php echo $donneesNews['news_id'] . "-" . encodeUrl($donneesNews['titre']); ?>.html#commentaires" title="Lire les commentaires ou rédiger un commentaire sur l'article">
                    <?php if ($nombre_commentaires == 0) {
                        ?>
                        [Rédiger un commentaire]
                        <?php
                    } else if ($nombre_commentaires == 1) {
                        ?>
                        [Lire le commentaire]
                        <?php
                    } else {
                        ?>
                        [Lire les <?php echo $nombre_commentaires; ?> commentaires]
                        <?php
                    }
                    ?> 
                </a>
                <?php
            }
            ?>
        </div>
        <?php
    }
    ?>
    <div class="clear"></div>
</article>