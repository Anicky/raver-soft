<?php
$pageTitre = "Mot de passe oublié";
include_once("haut.php");
require_once("acces-compte.php");
?>

<h1><?php echo $pageTitre; ?></h1>

<form method="post" action="mot-de-passe-oublie-effectue.html" class="centre" id="formMdpOubli">
    <div class="contenuPage">
        <table class="formulaire">
            <tr>
                <td class="label">
                    <label for="email">Email</label>
                </td>
                <td>
                    <input type="text" name="email" id="email" size="40" maxlength="255" />
                </td>
            </tr>
        </table>
    </div>
    <input class="bouton" id="back" type="button" onclick="goToUrl('connexion.html')" value="Annuler" />
    <input type="hidden" name="tokenValidation" value="<?php echo $_SESSION['tokenValidation']; ?>" />
    <input class="bouton" type="submit" value="Valider" />
</form>
<div id="dialogbox">
    <?php require_once("loading.php"); ?>
</div>
<script>
    $(document).ready(function() {
        $("#dialogbox").dialog({
            autoOpen : false,
            modal : true,
            width : 500,
            resizable : false,
            draggable : false,
            show : "fade",
            hide : "fade",
            title : "Mot de passe oublié",
            buttons : {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            }
        });
    });
    $("#formMdpOubli").validate({
        rules: {
            email : {
                required : true,
                email : true,
                maxlength : 255
            }
        }, 
        messages: { 
            email : {
                required : "Vous devez rentrer un email valide.",
                email : "Vous devez rentrer un email valide.",
                maxlength : "L'email ne peut pas dépasser 255 caractères."
            }
        } 
    });
    $("#formMdpOubli").ajaxForm({
        target: "#dialogbox",
        beforeSubmit : function() {
            $("#dialogbox").dialog('open');
        }
    });
</script>
<?php
include_once("google-analytics.php");
include_once("bas.php");
?>