<?php
if (!isset($bdd)) {
    require_once("bdd_config.php");
}
?>
<div id="menu">
    <ul>
        <?php
        $requeteCategoriesNews = "SELECT nom FROM tags WHERE id IN (SELECT id_tag FROM news_tags, news WHERE news_tags.id_news = news.id AND publiee = true) ORDER BY nom ASC";
        $reponseCategoriesNews = $bdd->query($requeteCategoriesNews);
        if ($reponseCategoriesNews->rowCount() > 0) {
            ?>
            <li<?php
            if ((isset($pageAccueil)) || (isset($categorie_menu))) {
                echo " class=\"actif\"";
            }
            ?>>
                <a href="" title="News">News</a>
                <ul>
                    <?php
                    while ($donneesCategoriesNews = $reponseCategoriesNews->fetch()) {
                        ?>
                        <li<?php
                        if (isset($categorie_menu)) {
                            if ($categorie_menu == encodeUrl($donneesCategoriesNews["nom"])) {
                                echo " class=\"actif\"";
                            }
                        }
                        ?>>
                            <a href="categorie/<?php echo encodeUrl($donneesCategoriesNews['nom']); ?>.html" title="<?php echo $donneesCategoriesNews['nom']; ?>">
                                <img src="img/fleche1_droite.png" alt="" />
                                <?php echo $donneesCategoriesNews['nom']; ?>
                            </a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </li>
            <?php
        }
        $reponseCategoriesNews->closeCursor();
        $requete = "SELECT * FROM rubriques ORDER BY ordre ASC";
        $reponse = $bdd->query($requete);

        while ($donnees = $reponse->fetch()) {
            $rubrique_id = $donnees["id"];
            $pagePrincipale = getPagePrincipale($bdd, $rubrique_id);

            $urlPage = "";
            if ($pagePrincipale != null) {
                $requete3 = "SELECT nom FROM pages WHERE id = ?";
                $reponse3 = $bdd->prepare($requete3);
                $reponse3->bindValue(1, $pagePrincipale, PDO::PARAM_INT);
                $reponse3->execute();
                $donnees3 = $reponse3->fetch();
                $urlPage = encodeUrl($donnees3["nom"]);
                $reponse3->closeCursor();
            }

            // On compte les pages de la rubrique
            $requete2 = "SELECT * FROM pages WHERE rubrique_id = ? ORDER BY ordre ASC";
            $reponse2 = $bdd->prepare($requete2);
            $reponse2->bindValue(1, $rubrique_id, PDO::PARAM_INT);
            $reponse2->execute();

            $nombre_pages = $reponse2->rowCount();
            ?>

            <li<?php
            if (isset($_GET['rubrique'])) {
                if ($_GET['rubrique'] == encodeUrl($donnees["nom"])) {
                    echo " class=\"actif\"";
                }
            }
            ?>>
                <a href="<?php
                if ($pagePrincipale != null)
                    echo encodeUrl($donnees["nom"]) . "/" . $urlPage;
                else
                    echo encodeUrl($donnees["nom"]);
                ?>.html" title="<?php echo securite_sortie($donnees["nom"]); ?>">
                       <?php
                       echo $donnees["nom"];
                       if ($nombre_pages > 0) {
                           ?>
                    </a>
                    <ul>
                        <?php
                        while ($donnees2 = $reponse2->fetch()) {
                            ?>
                            <li<?php
                            if (isset($_GET['page'])) {
                                if ($_GET['page'] == encodeUrl($donnees2["nom"]) && $_GET['rubrique'] == encodeUrl($donnees["nom"])) {
                                    echo " class=\"actif\"";
                                }
                            }
                            ?>>
                                <a href="<?php echo encodeUrl($donnees["nom"]) . "/" . encodeUrl($donnees2["nom"]); ?>.html" title="<?php echo securite_sortie($donnees2["nom"]); ?>">
                                    <img src="img/fleche1_droite.png" alt="" />
                                    <?php echo securite_sortie($donnees2["nom"]); ?>
                                </a>
                            </li>
                            <?php
                        }
                        $reponse2->fetch();
                        ?>
                    </ul>
                    <?php
                } else {
                    ?>
                    </a>
                    <?php
                }
                ?>
            </li>
            <?php
        }
        $reponse->closeCursor();
        ?>
        <li <?php
        if (isset($pageContact)) {
            echo " class=\"actif\"";
        }
        ?>>
            <a href="contact.html" title="Nous contacter">
                Contact
            </a>
        </li>
        <?php
        if (isset($_SESSION['utilisateur'])) {
            if ($session_role == "Membre" || $session_role == "Administrateur") {
                ?>
                <li <?php
                if (isset($pageAdmin)) {
                    echo " class=\"actif\"";
                }
                ?>>
                    <a href="admin" title="Accéder à l'administration du site">Administration</a>
                    <?php include_once("menu-admin.php"); ?>
                </li>
                <?php
            }
        }
        ?>
    </ul>
</div>