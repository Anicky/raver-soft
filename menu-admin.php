<ul>
    <li <?php
if (isset($pageArticles)) {
    echo " class=\"actif\"";
}
?>>
        <a href="admin" title="Gestion des articles">
            <img src="img/fleche1_droite.png" alt="" />
            Gestion des articles
        </a>
    </li>
    <li <?php
        if (isset($pageRubriques)) {
            echo " class=\"actif\"";
        }
?>>
        <a href="admin/rubriques.html" title="Gestion des rubriques">
            <img src="img/fleche1_droite.png" alt="" />
            Gestion des rubriques
        </a>
    </li>
    <li <?php
        if (isset($pageCategories)) {
            echo " class=\"actif\"";
        }
?>>
        <a href="admin/categories.html" title="Gestion des catégories">
            <img src="img/fleche1_droite.png" alt="" />
            Gestion des catégories
        </a>
    </li>
    <?php
    if ($session_role == "Administrateur") {
        ?>
        <li <?php
    if (isset($pageMembres)) {
        echo " class=\"actif\"";
    }
        ?>>
            <a href="admin/membres.html" title="Gestion des membres">
                <img src="img/fleche1_droite.png" alt="" />
                Gestion des membres
            </a>
        </li>
        <?php
    }
    ?>
    <li <?php
    if (isset($pageTelechargements)) {
        echo " class=\"actif\"";
    }
    ?>>
        <a href="admin/telechargements.html" title="Gestion des téléchargements">
            <img src="img/fleche1_droite.png" alt="" />
            Gestion des téléchargements
        </a>
    </li>
    <?php
    if ($session_role == "Administrateur") {
        ?>
        <li <?php
    if (isset($pageTextes)) {
        echo " class=\"actif\"";
    }
        ?>>
            <a href="admin/textes.html" title="Gestion des textes">
                <img src="img/fleche1_droite.png" alt="" />
                Gestion des textes
            </a>
        </li>
        <li <?php
        if (isset($pageParametres)) {
            echo " class=\"actif\"";
        }
        ?>>
            <a href="admin/parametres.html" title="Gestion des paramètres">
                <img src="img/fleche1_droite.png" alt="" />
                Gestion des paramètres
            </a>
        </li>
        <li <?php
        if (isset($pageStylesCSS)) {
            echo " class=\"actif\"";
        }
        ?>>
            <a href="admin/styles-css.html" title="Gestion des styles CSS">
                <img src="img/fleche1_droite.png" alt="" />
                Gestion des styles CSS
            </a>
        </li>
        <?php
    }
    ?>
    <li <?php
    if (isset($pageStatistiques)) {
        echo " class=\"actif\"";
    }
    ?>>
        <a href="admin/statistiques.html" title="Statistiques">
            <img src="img/fleche1_droite.png" alt="" />
            Statistiques
        </a>
    </li>
</ul>