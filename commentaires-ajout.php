<?php
require_once("bdd_config.php");
if (isset($_POST['commentaire'])) {
    if ($_SESSION['tokenValidation'] == $_POST['tokenValidation']) {

        $check_securite = FALSE;
        if (isset($_SESSION['utilisateur'])) {
            $check_securite = TRUE;
        } else {
            $securite = $_POST['securite'];
            if ($securite == 4) {
                $check_securite = TRUE;
            }
        }
        if ($check_securite) {
            $newsId = $_POST['id_news'];
            if (isset($_POST['pseudo'])) {
                $pseudo = $_POST['pseudo'];
                if ($pseudo == "") {
                    $pseudo = "Anonyme";
                }
            }
            $commentaire = $_POST['commentaire'];
            $requete = "SELECT commentaires FROM news WHERE id = ?";
            $reponse = $bdd->prepare($requete);
            $reponse->bindValue(1, $newsId, PDO::PARAM_INT);
            $reponse->execute();
            $donnees = $reponse->fetch();
            $commentaire_ok = false;
            if ($donnees != null) {
                if (($donnees['commentaires']) && ($commentaire != "")) {
                    if (isset($_SESSION['utilisateur'])) {
                        $requete2 = "INSERT INTO commentaires (id_news, date, id_auteur, texte) VALUES (?, " . getHeureActuelle() . ", ?, ?)";
                        $reponse2 = $bdd->prepare($requete2);
                        $reponse2->bindValue(1, $newsId, PDO::PARAM_INT);
                        $reponse2->bindValue(2, $session_id, PDO::PARAM_INT);
                        $reponse2->bindValue(3, $commentaire, PDO::PARAM_STR);
                        $reponse2->execute();
                        $reponse2->closeCursor();
                    } else {
                        $requete2 = "INSERT INTO commentaires (id_news, date, auteur, texte) VALUES (?, " . getHeureActuelle() . ", ?, ?)";
                        $reponse2 = $bdd->prepare($requete2);
                        $reponse2->bindValue(1, $newsId, PDO::PARAM_INT);
                        $reponse2->bindValue(2, $pseudo, PDO::PARAM_STR);
                        $reponse2->bindValue(3, $commentaire, PDO::PARAM_STR);
                        $reponse2->execute();
                        $reponse2->closeCursor();
                    }
                    $commentaire_ok = true;
                }
            }
            $reponse->closeCursor();
            if ($commentaire_ok) {
                ?>
                <script>
                    $.get("commentaires-liste.html", {id_news: '<?php echo $newsId; ?>'}, function(html) {
                        $("#liste_commentaires").html(html);
                        $("#commentaire").val("");
                    });
                    $.get("commentaires-nombre.html", {id_news: '<?php echo $newsId; ?>'}, function(html) {
                        $("#commentaires_nombre").html(html);
                    });
                </script>
                <p>Votre commentaire a bien été sauvegardé.</p>
                <?php
            } else {
                ?>
                <p>Une erreur s'est produite : votre commentaire n'a pas pu être sauvegardé.</p>
                <?php
            }
        } else {
            ?>
            <p>Vous n'avez pas indiqué la bonne réponse pour la mesure de sécurité.</p>
            <?php
        }
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php
    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php
}
?>