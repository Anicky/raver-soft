<?php
if (!isset($bdd)) {
    require_once("bdd_config.php");
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <base href="<?php echo URL; ?>" />
        <meta name="description" content="<?php echo afficheTexte($bdd, "description"); ?>" />
        <meta name="keywords" content="<?php echo afficheTexte($bdd, "keywords"); ?>" />
        <link rel="alternate" type="application/rss+xml" href="news.rss" />
        <link rel="icon" type="image/png" href="favicon.png" />
        <!--[if IE]><link rel="shortcut icon" type="image/x-icon" href="favicon.ico" /><![endif]-->
        <link rel="stylesheet" href="css/style_01.css" />
        <link rel="stylesheet" href="css/jquery-ui.css" />
        <link rel="stylesheet" href="css/menu.css" />
        <!--[if lt IE 9]><link rel="stylesheet" href="css/style_01_ie.css" /><![endif]-->
        <link rel="stylesheet" href="css/style_01_1000px.css" media="screen and (max-width: 1000px)" />
        <link rel="stylesheet" href="css/style_01_1000px.css" media="screen and (max-device-width: 1000px)" />
        <link rel="stylesheet" href="css/style_01_650px.css" media="screen and (max-width: 650px)" />
        <link rel="stylesheet" href="css/style_01_650px.css" media="screen and (max-device-width: 650px)" />
        <script src="js/jquery.js" charset="utf-8"></script>
        <script src="js/jquery-ui.js" charset="utf-8"></script>
        <script src="js/jquery-validation.js" charset="utf-8"></script>
        <script src="js/jquery-form.js" charset="utf-8"></script>
        <script src="js/jquery-ddsmoothmenu.js" charset="utf-8"></script>
        <!--[if lt IE 9]><script src="js/html5.js" charset="utf-8"></script><![endif]-->
        <script>
            $(document).ready(function() {
                ddsmoothmenu.init({
                    mainmenuid: "menu",
                    classname: "menu",
                    arrowswap: true
                });
<?php
if (isset($_SESSION['utilisateur'])) {
    ?>
                    ddsmoothmenu.init({
                        mainmenuid: "compte",
                        classname: "menu",
                        arrowswap: true
                    });
    <?php
}
?>
                $.ajaxSetup({
                    cache: false
                });
            });
            function goToUrl(url) {
                window.location.href = "<?php echo URL; ?>" + url;
            }
        </script>
        <title>Raver Soft<?php
            if (isset($pageTitre)) {
                echo " - " . $pageTitre;
            }
            ?></title>
    </head>
    <body>
        <div id="contenu">
            <div class="wrapper">
                <header>
                    <div class="conteneur">
                        <a href="" title="Accueil du site" id="logo"></a>
                        <nav>
                            <?php include_once("menu.php"); ?> 
                        </nav>
                        <?php include_once("espace-membre.php"); ?>
                        <div class="clear"></div>
                    </div>
                </header>
                <section>