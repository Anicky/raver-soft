<?php
require_once("bdd_config.php");
if (!isset($_SESSION['utilisateur'])) {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php
} else {
    ?>

    <form action="compte-mot-de-passe-modifie.html" method="post" id="formMotDePasse">
        <label for="mdp_ancien">Mot de passe actuel</label><br /><br />
        <input type="password" name="mdp_ancien" id="mdp_ancien" size="40" maxlength="100" /><br />
        <label for="mdp_nouveau1">Nouveau mot de passe</label><br /><br />
        <input type="password" name="mdp_nouveau1" id="mdp_nouveau1" size="40" maxlength="100" /><br />
        <label for="mdp_nouveau2">Confirmation du nouveau mot de passe</label><br /><br />
        <input type="password" name="mdp_nouveau2" id="mdp_nouveau2" size="40" maxlength="100" /><br />
        <input type="hidden" name="tokenValidation" value="<?php echo $_SESSION['tokenValidation']; ?>" />
    </form>
    <script>
        $("#formMotDePasse").validate({
            rules: {
                mdp_ancien : {
                    required : true
                },
                mdp_nouveau1 : {
                    required : true,
                    maxlength : 100,
                    minlength : 6
                },
                mdp_nouveau2 : {
                    required : true,
                    maxlength : 100,
                    minlength : 6,
                    equalTo : "#mdp_nouveau1"
                }
            },
            messages: { 
                mdp_ancien : {
                    required : "Vous devez rentrer votre ancien de mot de passe."
                },
                mdp_nouveau1 : {
                    required : "Vous devez rentrer un nouveau mot de passe.",
                    maxlength : "Le mot de passe ne peut pas dépasser 100 caractères.",
                    minlength : "Le mot de passe doit faire au moins 6 caractères."
                },
                mdp_nouveau2 : {
                    required : "Vous devez confirmer le nouveau mot de passe.",
                    maxlength : "Le mot de passe ne peut pas dépasser 100 caractères.",
                    minlength : "Le mot de passe doit faire au moins 6 caractères.",
                    equalTo : "Vous n'avez pas rentré le même mot de passe."
                }
            } 
        });
        $("#formMotDePasse").ajaxForm({
            target: "#dialogbox2",
            beforeSubmit : function() {
                $("#dialogbox2").dialog('option', 'buttons', {
                    "Fermer" : function() {
                        $(this).dialog("close");
                    }
                });
            }
        });
        $("#dialogbox2").dialog('option', 'buttons', { 
            "Annuler" : function() {
                $(this).dialog("close");
            },
            "Valider" : function() {
                $("#formMotDePasse").submit();
            }
        });
    </script>
    <?php
}
?>