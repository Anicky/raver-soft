<?php
require_once("bdd_config.php");
$rubrique = "";
$page = "";
if (isset($_GET["rubrique"])) {
    $rubrique = $_GET["rubrique"];
}
if (isset($_GET["page"])) {
    $page = $_GET["page"];
}

// Page
if ($page != "" && $rubrique != "") {
    $requetePage = "SELECT pages.nom AS nom_page,
        rubriques.nom AS nom_rubrique,
        pages.texte AS contenuPage,
        pages.id AS idPage
        FROM pages, rubriques
        WHERE pages.rubrique_id = rubriques.id
        AND rubriques.url = ?
        AND pages.url = ?";
    $reponsePage = $bdd->prepare($requetePage);
    $reponsePage->bindValue(1, $rubrique, PDO::PARAM_STR);
    $reponsePage->bindValue(2, $page, PDO::PARAM_STR);
    $reponsePage->execute();
    $donneesPage = $reponsePage->fetch();
    if ($donneesPage != null) {
        $pageTitre = securite_sortie($donneesPage['nom_rubrique']) . " : " . securite_sortie($donneesPage['nom_page']);
        $pageId = $donneesPage['idPage'];

        if (!estCache($bdd, $session_id)) {
            $requeteVues = "UPDATE pages SET vues = vues + 1 WHERE id = ?";
            $reponseVues = $bdd->prepare($requeteVues);
            $reponseVues->bindValue(1, $pageId, PDO::PARAM_INT);
            $reponseVues->execute();
            $reponseVues->closeCursor();
        }

        include_once("haut.php");
        ?>
        <h1><?php echo $pageTitre; ?></h1>
        <?php
        if ($donneesPage["contenuPage"] != "") {
            ?>
            <div class="contenuPage">
                <?php
                echo $donneesPage["contenuPage"];
                ?>
            </div>
            <?php
            if ($page == "avancement") {
                include_once("projet-avancement.php");
            }
        }
    } else {
        header("Location: " . URL . "page-introuvable.html");
        exit;
    }
    $reponsePage->closeCursor();
} else if ($rubrique != "") {
    $requetePage = "SELECT * FROM rubriques WHERE url = ?";
    $reponsePage = $bdd->prepare($requetePage);
    $reponsePage->bindValue(1, $rubrique, PDO::PARAM_STR);
    $reponsePage->execute();
    $donneesPage = $reponsePage->fetch();
    if ($donneesPage != null) {
        $pageTitre = securite_sortie($donneesPage['nom']);

        if (!estCache($bdd, $session_id)) {
            $requeteVues = "UPDATE rubriques SET vues = vues + 1 WHERE id = ?";
            $reponseVues = $bdd->prepare($requeteVues);
            $reponseVues->bindValue(1, $donneesPage['id'], PDO::PARAM_INT);
            $reponseVues->execute();
            $reponseVues->closeCursor();
        }

        include_once("haut.php");
        ?>
        <h1><?php echo $pageTitre; ?></h1>
        <div class="contenuPage">
            <?php echo $donneesPage["texte"]; ?>
        </div>
        <?php
    } else {
        header("Location: " . URL . "page-introuvable.html");
        exit;
    }
    $reponsePage->closeCursor();
} else {
    header("Location: " . URL . "page-introuvable.html");
    exit;
}
include_once("google-analytics.php");
include_once("bas.php");
?>