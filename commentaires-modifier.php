<?php
require_once("bdd_config.php");

if (!isset($_SESSION['utilisateur'])) {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php
} else {
    $id = "";
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
        $ok = false;
        $auteur = "";
        $texte = "";
        $requete = "SELECT id_auteur, auteur, texte FROM commentaires WHERE id = ?";
        $reponse = $bdd->prepare($requete);
        $reponse->bindValue(1, $id, PDO::PARAM_INT);
        $reponse->execute();
        $donnees = $reponse->fetch();
        if (($donnees['id_auteur'] == $session_id) || ($session_role == "Administrateur")) {
            $ok = true;
            $texte = securite_sortie($donnees['texte']);
            $auteur = securite_sortie($donnees['auteur']);
        }
        $reponse->closeCursor();
        if ($ok) {
            ?>
            <form action="commentaires-modification.html" method="post" id="modifierCommentaire">
                <?php if ($auteur != "") { ?>
                    <label for="auteur">Auteur</label><br /><br />
                    <input type="text" name="auteur" id="auteur" value="<?php echo $auteur; ?>" /><br /><br />
                <?php } ?>
                <label for="texte">Texte</label><br /><br />
                <textarea name="texte" id="texte"><?php echo $texte; ?></textarea>
                <input type="hidden" name="id" value="<?php echo $id; ?>" />
                <input type="hidden" name="tokenValidation" value="<?php echo $_SESSION['tokenValidation']; ?>" />
            </form>
            <script>
                $("#modifierCommentaire").ajaxForm({
                    target: "#dialogbox",
                    beforeSubmit : function() {
                        $("#dialogbox").dialog('option', 'buttons', {
                            "Fermer" : function() {
                                $(this).dialog("close");
                            }
                        });
                    }
                });
                $("#dialogbox").dialog('option', 'buttons', { 
                    "Annuler" : function() {
                        $(this).dialog("close");
                    },
                    "Valider" : function() {
                        $("#modifierCommentaire").submit();
                    }
                });
            </script>
            <?php
        } else {
            ?>
            <p>Le commentaire recherché est introuvable.</p>
            <?php
        }
    } else {
        ?>
        <p>Le commentaire recherché est introuvable.</p>
        <?php
    }
}
?>