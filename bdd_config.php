<?php
ob_start();
session_start();

define("HOST", "localhost");
define("BASE", "raversoft");
define("USER", "root");
define("PASS", "");
define("URL", "https://localhost/raversoft/");

date_default_timezone_set('Europe/Paris');

$bdd = new PDO("mysql:host=" . HOST . ";dbname=" . BASE, USER, PASS);
$bdd->query("SET NAMES 'utf8'");

$session_id = "";
$session_role = "";
if (isset($_SESSION['utilisateur'])) {
    $requeteUtilisateur = "SELECT membres.id AS user_id, roles.nom AS user_role
        FROM membres, roles
        WHERE membres.id_role = roles.id
        AND membres.token = ?";
    $reponseUtilisateur = $bdd->prepare($requeteUtilisateur);
    $reponseUtilisateur->bindValue(1, $_SESSION['utilisateur'], PDO::PARAM_STR);
    $reponseUtilisateur->execute();
    $donneesUtilisateur = $reponseUtilisateur->fetch();
    if ($donneesUtilisateur != null) {
        $session_id = $donneesUtilisateur['user_id'];
        $session_role = $donneesUtilisateur['user_role'];
    }
    $reponseUtilisateur->closeCursor();
}
require_once("fonctions.php");
if (!isset($_SESSION['tokenValidation'])) {
    $_SESSION['tokenValidation'] = creerToken();
}
?>