<?php
if ($numeroPage == 1) {
    ?>
    <a href="./<?php
    if (isset($nomTag)) {
        echo "categorie/" . encodeUrl($nomTag) . ".html";
    }
    ?>" class="bouton_gauche ajax" title="Voir les articles plus récents">
        <img src="img/fleche2_gauche.png" alt=">" />
        Articles plus récents
    </a>
    <?php
} else if ($numeroPage > 1) {
    ?>
    <a href="<?php
    if (isset($nomTag)) {
        echo "categorie/" . encodeUrl($nomTag) . "/page-" . ($numeroPage - 1);
    } else {
        echo "news/page-" . ($numeroPage - 1);
    }
    ?>.html" class="bouton_gauche ajax" title="Voir les articles plus récents">
        <img src="img/fleche2_gauche.png" alt=">" />
        Articles plus récents
    </a>
    <?php
}

if (isset($nomTag)) {
    $nombreArticles = nombreElements($bdd, "news, tags, news_tags
        WHERE news_tags.id_tag = tags.id
        AND news_tags.id_news = news.id
        AND tags.nom = '" . $nomTag . "';");
} else {
    $nombreArticles = nombreElements($bdd, "news");
}
if ((($numeroPage + 1) * $nombre_news) < ($nombreArticles)) {
    ?>
    <a href="<?php
    if (isset($nomTag)) {
        echo "categorie/" . encodeUrl($nomTag) . "/page-" . ($numeroPage + 1);
    } else {
        echo "news/page-" . ($numeroPage + 1);
    }
    ?>.html" class="bouton_droite ajax" title="Voir les articles plus anciens">
        <img src="img/fleche2_droite.png" alt=">" />
        Articles plus anciens
    </a>
    <?php
}
?>
<div class="clear"></div>