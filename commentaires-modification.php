<?php
require_once("bdd_config.php");

if (!isset($_SESSION['utilisateur'])) {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php
} else {
    $id = "";
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
        if ($_SESSION['tokenValidation'] == $_POST['tokenValidation']) {
            $auteur = "";
            if (isset($_POST['auteur'])) {
                $auteur = $_POST['auteur'];
            }
            $texte = $_POST['texte'];
            $id_news = 0;
            $requete = "SELECT id_auteur, auteur, texte, id_news FROM commentaires WHERE id = ?";
            $reponse = $bdd->prepare($requete);
            $reponse->bindValue(1, $id, PDO::PARAM_INT);
            $reponse->execute();
            $donnees = $reponse->fetch();
            $commentaire_ok = false;
            if (($donnees['id_auteur'] == $session_id) || ($session_role == "Administrateur")) {
                $id_news = $donnees['id_news'];
                $commentaire_ok = true;
                if (($donnees['auteur'] != null) && ($auteur != "")) {
                    $requete2 = "UPDATE commentaires SET texte = ?, AUTEUR = ? WHERE id = ?";
                    $reponse2 = $bdd->prepare($requete2);
                    $reponse2->bindValue(1, $texte, PDO::PARAM_STR);
                    $reponse2->bindValue(2, $auteur, PDO::PARAM_STR);
                    $reponse2->bindValue(3, $id, PDO::PARAM_INT);
                    $reponse2->execute();
                    $reponse2->closeCursor();
                } else {
                    $requete2 = "UPDATE commentaires SET texte = ? WHERE id = ?";
                    $reponse2 = $bdd->prepare($requete2);
                    $reponse2->bindValue(1, $texte, PDO::PARAM_STR);
                    $reponse2->bindValue(2, $id, PDO::PARAM_INT);
                    $reponse2->execute();
                    $reponse2->closeCursor();
                }
            }
            $reponse->closeCursor();
            if ($commentaire_ok) {
                ?>
                <script>
                    $.get("commentaires-liste.html", {id_news : '<?php echo $id_news; ?>'}, function(html) {
                        $("#liste_commentaires").html(html);
                        $("#commentaire").val("");
                    });
                    $.get("commentaires-nombre.html", {id_news : '<?php echo $id_news; ?>'}, function(html) {
                        $("#commentaires_nombre").html(html);
                    });
                </script>
                <p>Le commentaire a bien été modifié.</p>
                <?php
            } else {
                ?>
                <p>Une erreur s'est produite : vous ne pouvez pas modifier ce commentaire.</p>
                <?php
            }
        } else {
            ?>
            <script>
                window.location.href = "./";
            </script>
            <?php
        }
    } else {
        ?>
        <p>Le commentaire recherché est introuvable.</p>
        <?php
    }
}
?>