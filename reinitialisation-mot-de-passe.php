<?php
$pageTitre = "Réinitialisation de votre mot de passe";
include_once("haut.php");
require_once("acces-compte.php");
?>
<h1><?php echo $pageTitre; ?></h1>
<div class="contenuPage">
    <?php
    if (isset($_GET['pseudo'])) {
        $pseudo = $_GET['pseudo'];
        $password = $_GET['password'];
        if (($pseudo != "") && ($password != "")) {
            $requete = "SELECT id, pseudo, password, email FROM membres";
            $reponse = $bdd->query($requete);
            $trouve = false;
            $id = "";
            while ((!$trouve) && ($donnees = $reponse->fetch())) {
                if ($pseudo == (crypterSansSalt($donnees['pseudo'])) && (testerMotDePasse($password, $donnees['password']))) {
                    $id = $donnees['id'];
                    $email = $donnees['email'];
                    $pseudo = $donnees['pseudo'];
                    $trouve = true;
                }
            }
            if ($trouve) {
                $password = chaineAleatoire(8);
                require_once("fonctions-mails.php");
                if (mail_reinitialisationMotDePasse($pseudo, $password, $email)) {
                    $requete2 = "UPDATE membres SET password = ? WHERE id = ?";
                    $reponse2 = $bdd->prepare($requete2);
                    $reponse2->bindValue(1, crypter($password), PDO::PARAM_STR);
                    $reponse2->bindValue(2, $id, PDO::PARAM_INT);
                    $reponse2->execute();
                    $reponse2->closeCursor();
                    ?>
                    <p>
                        Votre mot de passe vient d'être réinitialisé !<br /><br />
                        Un mail vous a été envoyé pour que vous puissiez connaître votre nouveau de mot de passe.
                    </p>
                    <?php
                } else {
                    ?>
                    <p>Votre mot de passe n'a pas pu être réinitialisé.</p>
                    <?php
                }
            } else {
                ?>
                <p>Impossible de trouver votre compte : vérifiez que vous avez bien copié le lien de l'email que vous avez reçu.</p>
                <?php
            }
            $reponse->closeCursor();
        } else {
            header("Location: " . URL . "page-introuvable.html");
            exit;
        }
    } else {
        header("Location: " . URL . "page-introuvable.html");
        exit;
    }
    ?>

</div>
<?php
include_once("google-analytics.php");
include_once("bas.php");
?>