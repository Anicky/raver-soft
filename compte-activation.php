<?php
$pageTitre = "Activation de votre compte";
include_once("haut.php");
require_once("acces-compte.php");
?>
<h1><?php echo $pageTitre; ?></h1>
<div class="contenuPage">
    <?php
    if (isset($_GET['pseudo'])) {
        $pseudo = $_GET['pseudo'];
        $password = $_GET['password'];
        if (($pseudo != "") && ($password != "")) {
            $requete = "SELECT id, pseudo, password, actif FROM membres";
            $reponse = $bdd->query($requete);
            $trouve = false;
            $actif = false;
            $id = "";
            while ((!$trouve) && ($donnees = $reponse->fetch())) {
                if ($pseudo == (crypterSansSalt($donnees['pseudo'])) && (testerMotDePasse($password, $donnees['password']))) {
                    $actif = $donnees['actif'];
                    $id = $donnees['id'];
                    $trouve = true;
                }
            }
            if ($trouve) {
                if (!$actif) {
                    $requete2 = "UPDATE membres SET actif = true WHERE id = ?";
                    $reponse2 = $bdd->prepare($requete2);
                    $reponse2->bindValue(1, $id, PDO::PARAM_INT);
                    $reponse2->execute();
                    $reponse2->closeCursor();
                    ?>
                    <p>Votre compte est maintenant activé ! vous pouvez désormais vous connecter avec vos identifiants !</p>
                    <?php
                } else {
                    ?>
                    <p>Votre compte est déjà activé : vous pouvez vous connecter avec vos identifiants !</p>
                    <?php
                }
            } else {
                ?>
                <p>Impossible de trouver votre compte : vérifiez que vous avez bien copié le lien de l'email que vous avez reçu.</p>
                <?php
            }
            $reponse->closeCursor();
        } else {
            header("Location: " . URL . "page-introuvable.html");
            exit;
        }
    } else {
        header("Location: " . URL . "page-introuvable.html");
        exit;
    }
    ?>
</div>
<?php
include_once("google-analytics.php");
include_once("bas.php");
?>