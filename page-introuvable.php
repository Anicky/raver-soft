<?php
$pageTitre = "Page introuvable";
include_once("haut.php");
?>
<h1>Erreur : page introuvable</h1>
<article>
    <p>Une erreur s'est produite : la page que vous recherchez est actuellement introuvable.</p>
    <p>Il se peut qu'elle ait été effacée ou renommée. N'hésitez pas à faire un tour sur le site pour voir si elle n'a pas simplement changé de place !</p>
</article>
<?php
include_once("google-analytics.php");
include_once("bas.php");
?>