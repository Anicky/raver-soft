<?php
$pageTitre = "Inscription";
$pageInscription = true;
include_once("haut.php");
require_once("acces-compte.php");

if (getParametre($bdd, "inscription_ouverte")) {
    ?>
    <h1><?php echo $pageTitre; ?></h1>

    <form method="post" action="inscription-effectuee.html" class="centre" enctype="multipart/form-data" id="inscription">
        <div class="contenuPage">
            <table class="formulaire">
                <tr>
                    <td class="label">
                        <label for="pseudo">Pseudo</label>
                    </td>
                    <td>
                        <input type="text" name="pseudo" id="pseudo" size="40" maxlength="100" onkeyup="verifier_infos()" />
                        <div id="verification_pseudo" class="verification"></div>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        <label for="email">Email</label>
                    </td>
                    <td>
                        <input type="text" name="email" id="email" size="40" maxlength="255" onkeyup="verifier_infos()" />
                        <div id="verification_email" class="verification"></div>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        <label for="password">Mot de passe</label>
                    </td>
                    <td>
                        <input type="password" name="password" id="password" size="40" maxlength="100" />
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        <label for="avatar">Avatar</label>
                    </td>
                    <td>
                        <div id="checkbox_avatar">
                            <input type="radio" name="selection_avatar" id="selection_avatar_aucun" value="aucun" checked="checked" />
                            <label for="selection_avatar_aucun">Pas d'avatar</label>
                            <input type="radio" name="selection_avatar" id="selection_avatar_choisir" value="choisir" />
                            <label for="selection_avatar_choisir">Choisir un avatar</label>
                        </div>
                        <div id="avatar_choisir">
                            <input type="file" name="avatar" id="avatar" size="40" />
                            <div class="warning">La taille maximum pour l'envoi d'une image est de <?php echo getParametre($bdd, "image_tailleMax_ko"); ?> Ko.</div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <input id="back" class="bouton" type="button" onclick="goToUrl('')" value="Annuler" />
        <input type="hidden" name="tokenValidation" value="<?php echo $_SESSION['tokenValidation']; ?>" />
        <input class="bouton" type="submit" value="Sauvegarder" id="boutonSubmit" />
    </form>
    <div id="dialogbox">
        <?php require_once("loading.php"); ?>
    </div>
    <script>
                            $(document).ready(function() {
                                $("#checkbox_avatar").buttonset();
                                $("#dialogbox").dialog({
                                    autoOpen: false,
                                    modal: true,
                                    resizable: false,
                                    draggable: false,
                                    show: "fade",
                                    hide: "fade",
                                    title: "Inscription",
                                    buttons: {
                                        "Fermer": function() {
                                            $(this).dialog("close");
                                        }
                                    }
                                });
                            });
                            $("input:radio[name=selection_avatar]").change(function() {
                                choix_avatar = $(this).val();
                                if (choix_avatar == 'aucun') {
                                    $("#avatar_choisir").hide();
                                } else if (choix_avatar == 'choisir') {
                                    $("#avatar_choisir").show();
                                }
                            });
                            $("#inscription").validate({
                                rules: {
                                    pseudo: {
                                        required: true,
                                        maxlength: 100
                                    },
                                    email: {
                                        required: true,
                                        email: true,
                                        maxlength: 255
                                    },
                                    password: {
                                        required: true,
                                        maxlength: 100,
                                        minlength: 6
                                    }
                                },
                                messages: {
                                    pseudo: {
                                        required: "Vous devez rentrer un pseudo.",
                                        maxlength: "Le pseudo ne peut pas dépasser 100 caractères."
                                    },
                                    email: {
                                        required: "Vous devez rentrer un email valide.",
                                        email: "Vous devez rentrer un email valide.",
                                        maxlength: "L'email ne peut pas dépasser 255 caractères."
                                    },
                                    password: {
                                        required: "Vous devez rentrer un mot de passe.",
                                        maxlength: "Le mot de passe ne peut pas dépasser 100 caractères.",
                                        minlength: "Le mot de passe doit faire au moins 6 caractères"
                                    }
                                }
                            });
                            $("#inscription").ajaxForm({
                                target: "#dialogbox",
                                beforeSubmit: function() {
                                    $("#dialogbox").dialog('open');
                                }
                            });
                            function verifier_infos() {
                                $("#boutonSubmit").removeAttr("disabled");
                                var pseudo = $("#pseudo").val();
                                var email = $("#email").val();
                                if (pseudo != "" || email != "") {
                                    $.get("membres-verification.html", {champ: 'pseudo', valeur: pseudo}, function(html) {
                                        $("#verification_pseudo").html(html);
                                    });
                                    $.get("membres-verification.html", {champ: 'email', valeur: email}, function(html) {
                                        $("#verification_email").html(html);
                                    });
                                }
                            }
    </script>

    <?php
    include_once("google-analytics.php");
    include_once("bas.php");
} else {
    header("Location: ./");
    exit;
}
?>