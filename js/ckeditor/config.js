﻿/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	config.resize_enabled = false;
    config.language = 'fr';
    config.uiColor = '#9AB8F3';
	config.toolbar = 'MyToolbar';
	config.height = '500px';
	config.toolbar_MyToolbar =
	[
		{ name: 'document', items : [ 'Source' ] },
		{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
		{ name: 'insert', items : [ 'Image','Table','HorizontalRule' ] },
		{ name: 'styles', items : [ 'Format', 'FontSize' ] },
		{ name: 'basicstyles', items : [ 'Bold','Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
		{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote', '-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
		{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
		{ name: 'tools', items : [ 'ShowBlocks', 'Maximize' ] }
	];
	config.protectedSource.push(/<\?[\s\S]*?\?>/g);
    config.extraPlugins = 'codemirror';
	config.entities = false;
	config.toolbarCanCollapse = false;
	config.enterMode = CKEDITOR.ENTER_P;
    config.shiftEnterMode = CKEDITOR.ENTER_BR;
};

CKEDITOR.on( 'instanceReady', function( ev )
{
	var writer = ev.editor.dataProcessor.writer; 
	// The character sequence to use for every indentation step.
	writer.indentationChars = '    ';
 
	var dtd = CKEDITOR.dtd;
	// Elements taken as an example are: block-level elements (div or p), list items (li, dd), and table elements (td, tbody).
	for ( var e in CKEDITOR.tools.extend( {}, dtd.$block, dtd.$listItem, dtd.$tableContent ) )
	{
		ev.editor.dataProcessor.writer.setRules( e, {
			// Indicates that an element creates indentation on line breaks that it contains.
			indent : true,
			// Inserts a line break before a tag.
			breakBeforeOpen : true,
			// Inserts a line break after a tag.
			breakAfterOpen : true,
			// Inserts a line break before the closing tag.
			breakBeforeClose : true,
			// Inserts a line break after the closing tag.
			breakAfterClose : true
		});
	}
 
	for ( var e in CKEDITOR.tools.extend( {}, dtd.$list, dtd.$listItem, dtd.$tableContent ) )
	{
		ev.editor.dataProcessor.writer.setRules( e, {			
			indent : true,
		});
	}
 
	// You can also apply the rules to a single element.
	ev.editor.dataProcessor.writer.setRules( 'table',
	{ 		
		indent : true
	});	
 
	ev.editor.dataProcessor.writer.setRules( 'form',
	{ 		
		indent : true
	});		
});
