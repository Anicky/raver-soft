<?php
require_once("bdd_config.php");
$id = "";
if (isset($_GET["id"])) {
    $id = $_GET["id"];
}
$categorie_menu = $id;

// Page
if ($id != "") {
    $numeroPage = 0;
    if (isset($_GET['numeroPage'])) {
        $numeroPage = $_GET['numeroPage'];
    }
    $requete = "SELECT id, nom FROM tags WHERE url = ?";
    $reponse = $bdd->prepare($requete);
    $reponse->bindValue(1, $id, PDO::PARAM_STR);
    $reponse->execute();
    $donnees = $reponse->fetch();
    $trouve = false;
    $nomTag = "";
    if ($donnees != null) {
        $trouve = true;
        $nomTag = $donnees['nom'];
        $id = $donnees['id'];
    }
    $reponse->closeCursor();
    if ($trouve) {
        $pageTitre = securite_sortie($nomTag) . " : News";
        $nombre_news = getParametre($bdd, "nombre_news_page");
        $requeteNews = "SELECT news.id AS news_id,
        titre,
        DATE_FORMAT(date, '%d/%m/%Y à %Hh%i') AS dateFormatee,
        DATE_FORMAT(date, '%w') AS jourSemaine,
        auteur,
        id_auteur,
        texte,
        commentaires
        FROM news, tags, news_tags
        WHERE tags.id = ?
        AND publiee = true
        AND news_tags.id_tag = tags.id
        AND news_tags.id_news = news.id
        ORDER BY date DESC
        LIMIT ?,?";
        $numero_depart = $numeroPage * $nombre_news;
        $reponseNews = $bdd->prepare($requeteNews);
        $reponseNews->bindValue(1, $id, PDO::PARAM_INT);
        $reponseNews->bindValue(2, (int) trim($numero_depart), PDO::PARAM_INT);
        $reponseNews->bindValue(3, (int) trim($nombre_news), PDO::PARAM_INT);
        $reponseNews->execute();
        $nombre_news_page = $reponseNews->rowCount();

        if (!isset($ajax_request)) {
            include_once("haut.php");
        } else {
            ?>
            <script>
                document.title = "Raver Soft - " + "<?php echo $pageTitre; ?>";
            </script>
            <?php
        }
        ?>
        <h1><?php echo $pageTitre; ?></h1>
        <?php
        if ($nombre_news_page > 0) {
            ?>
            <?php
            while ($donneesNews = $reponseNews->fetch()) {
                $preview = true;
                require("news-affichage.php");
            }
            $reponseNews->closeCursor();
            require_once("news-suivant-precedent.php");
        } else {
            ?>
            <div class="contenuPage">
                Pas de news pour le moment.
            </div>
            <?php
        }
    } else {
        header("Location: " . URL . "page-introuvable.html");
        exit;
    }
} else {
    header("Location: " . URL . "page-introuvable.html");
    exit;
}
?>
<!--[if !IE]> <--><script src="js/ajax.js" charset="utf-8"></script><!--> <![endif]-->
<?php
include_once("google-analytics.php");
if (!isset($ajax_request)) {
    include_once("bas.php");
}
?>