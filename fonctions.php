<?php

/**
 * Fonctions utiles pour le site Raver Soft
 * @author Anicky (Jérémie JALOUZET)
 */
function getHeureActuelle() {
    return "NOW() + INTERVAL 6 HOUR";
}

/**
 * Transforme une chaîne de caractères avec accents en chaîne de caractères sans
 * accents. Optimisé pour l'UTF-8.
 * @param La chaine de caractères avec accents
 * @return La chaîne de caractères sans accents
 */
function enleveAccents($chaine) {
    return strtr($chaine, array('Á' => 'A', 'À' => 'A', 'Â' => 'A', 'Ä' => 'A',
                'Ã' => 'A', 'Å' => 'A', 'Ç' => 'C', 'É' => 'E', 'È' => 'E',
                'Ê' => 'E', 'Ë' => 'E', 'Í' => 'I', 'Ï' => 'I', 'Î' => 'I',
                'Ì' => 'I', 'Ñ' => 'N', 'Ó' => 'O', 'Ò' => 'O', 'Ô' => 'O',
                'Ö' => 'O', 'Õ' => 'O', 'Ú' => 'U', 'Ù' => 'U', 'Û' => 'U',
                'Ü' => 'U', 'Ý' => 'Y', 'á' => 'a', 'à' => 'a', 'â' => 'a',
                'ä' => 'a', 'ã' => 'a', 'å' => 'a', 'ç' => 'c', 'é' => 'e',
                'è' => 'e', 'ê' => 'e', 'ë' => 'e', 'í' => 'i', 'ì' => 'i',
                'î' => 'i', 'ï' => 'i', 'ñ' => 'n', 'ó' => 'o', 'ò' => 'o',
                'ô' => 'o', 'ö' => 'o', 'õ' => 'o', 'ú' => 'u', 'ù' => 'u',
                'û' => 'u', 'ü' => 'u', 'ý' => 'y', 'ÿ' => 'y'));
}

/**
 * Transforme une chaîne de caractères en chaîne de caractères "propre" pour les
 * URL (Enlève les accents, remplace les caractères spéciaux, met la chaîne de
 * caractères en minuscules)
 * @param La chaîne de caractères à transformer
 * @return La chaîne de caractères "propre" (pour les URL)
 */
function encodeUrl($chaine) {
    return htmlspecialchars(preg_replace('#[^a-zA-Z0-9\-\._]#', '-', strtolower(enleveAccents($chaine))));
}

/**
 * A partir d'un numéro de jour de la semaine, renvoie le nom du jour en français
 * @param type $numero Le numéro du jour de la semaine renvoyé par MySQL(0 = Dimanche, 6 = Samedi)
 * @return Le jour de la semaine (en français)
 */
function jourSemaine($numero) {
    $jour = "";
    if ($numero == 0) {
        $jour = "Dimanche";
    } else if ($numero == 1) {
        $jour = "Lundi";
    } else if ($numero == 2) {
        $jour = "Mardi";
    } else if ($numero == 3) {
        $jour = "Mercredi";
    } else if ($numero == 4) {
        $jour = "Jeudi";
    } else if ($numero == 5) {
        $jour = "Vendredi";
    } else if ($numero == 6) {
        $jour = "Samedi";
    }
    return $jour;
}

/**
 * Affiche le début d'une chaîne de caractères (par exemple un article)
 * @param $chaine La chaîne de caractères
 * @param $nombre_mots Le nombre de mots à afficher
 * @return Une chaîne de caractères raccourcie
 */
function affichePreview($chaine, $nombre_mots) {
    $nouvelle_chaine = "";
    $tableau = explode(" ", $chaine);
    $i = 0;
    $taille_chaine = count($tableau);
    while (($i < $nombre_mots) && ($i < $taille_chaine)) {
        $nouvelle_chaine.=" " . "$tableau[$i]";
        $i++;
    }
    if ($nombre_mots < $taille_chaine) {
        $nouvelle_chaine .= "...";
    }
    return $nouvelle_chaine;
}

/**
 * Sécurise une chaine de caractères lorsqu'on souhaite la mettre dans une base de données (évite les injections SQL)
 * @param $chaine La chaîne de caractères
 * @return La chaîne de caractères sécurisée
 */
function securite_sql($chaine) {
    if (ctype_digit($chaine)) {
        $chaine = intval($chaine);
    } else {
        $chaine = addcslashes(mysql_real_escape_string($chaine), '%_');
    }
    return $chaine;
}

function securite_bdd($chaine) {
    return stripslashes($chaine);
}

/**
 * Sécurise une chaîne de caractères lorsqu'on souhaite l'afficher (évite les failles XSS)
 * @param $chaine La chaîne de caractères
 * @return La chaîne de caractères sécurisée
 */
function securite_sortie($chaine) {
    return htmlspecialchars($chaine);
}

/**
 * Créé une chaîne de caractères aléatoire (utile pour créer un mot de passe) en fonction d'un certain nombre de caractères.
 * @param Le nombre de caractères de la chaîne
 * @return La chaîne de caractères
 */
function chaineAleatoire($nombreCaracteres) {
    $password = "";
    $caracteres = array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
        "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", 0, 1, 2, 3, 4, 5, 6,
        7, 8, 9, "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
    shuffle($caracteres);
    for ($i = 0; $i < $nombreCaracteres; $i++) {
        $password .= $caracteres[$i];
    }
    return $password;
}

function crypterSansSalt($chaine) {
    return sha1($chaine);
}

function crypterAvecSalt($chaine, $salt) {
    return sha1($chaine . $salt) . $salt;
}

function crypter($chaine) {
    return crypterAvecSalt($chaine, chaineAleatoire(8));
}

function testerMotDePasse($chaine, $motdepasse) {
    return crypterAvecSalt($chaine, substr($motdepasse, -8));
}

function creerToken() {
    return crypterAvecSalt(time(), chaineAleatoire(8));
}

/**
 * Envoie une image au serveur, selon une taille maximum.
 * @param $fichier Le fichier à uploader
 * @param $tailleMaxKo La taille maximum de l'image (en Ko)
 * @return Le nom du fichier créé (si vide, on considère qu'il y a eu une erreur)
 */
function envoiImage($fichier, $tailleMaxKo) {
    $retour = "";
    $repertoire = "img/avatars/";
    if (isset($fichier) AND $fichier['error'] == 0) {
        if ($fichier['size'] <= ($tailleMaxKo * 1000)) {
            $infosFichier = pathinfo($fichier['name']);
            $extension_upload = $infosFichier['extension'];
            $extensions_autorisees = array('jpg', 'jpeg', 'gif', 'png', 'bmp', 'JPG', 'JPEG', 'GIF', 'PNG', 'BMP');
            $nomFichier = encodeUrl(time() . "_" . $fichier['name']);
            if (in_array($extension_upload, $extensions_autorisees)) {
                if (move_uploaded_file($fichier['tmp_name'], $repertoire . $nomFichier)) {
                    $retour = $nomFichier;
                }
            }
        }
    }
    return $retour;
}

/**
 * Retourne le nombre d'éléments de la base de donnée selon une condition
 * @param $bdd La base de données
 * @param $condition La condition
 * @return Un nombre d'éléments
 */
function nombreElements($bdd, $condition) {
    $reponse = $bdd->query("SELECT COUNT(*) AS nombre FROM " . $condition . "");
    $donnees = $reponse->fetch();
    $valeur = $donnees['nombre'];
    $reponse->closeCursor();
    return $valeur;
}

/**
 * Affiche un texte se trouvant dans la base de données
 * @param $bdd La base de données
 * @param $nom Le nom du texte
 * @return Le texte
 */
function afficheTexte($bdd, $nom) {
    $reponse = $bdd->prepare("SELECT valeur FROM textes WHERE nom = ?");
    $reponse->bindValue(1, $nom, PDO::PARAM_STR);
    $reponse->execute();
    $donnees = $reponse->fetch();
    $texte = $donnees['valeur'];
    $reponse->closeCursor();
    echo $texte;
}

/**
 * Retourne la valeur d'un paramètre se trouvant dans la base de données
 * @param $bdd La base de données
 * @param $nom Le nom du paramètre
 * @return La valeur du paramètre
 */
function getParametre($bdd, $nom) {
    $reponse = $bdd->prepare("SELECT valeur FROM parametres WHERE nom = ?");
    $reponse->bindValue(1, $nom, PDO::PARAM_STR);
    $reponse->execute();
    $donnees = $reponse->fetch();
    $valeur = $donnees['valeur'];
    $reponse->closeCursor();
    return $valeur;
}

/**
 * Retourne l'id d'une page principale en fonction de l'id d'une rubrique
 * @param $bdd La base de données
 * @param $id_rubrique L'id de la rubrique
 * @return L'id de la page
 */
function getPagePrincipale($bdd, $id_rubrique) {
    $pagePrincipale = "";
    $requete = "SELECT id_page FROM pages_principales WHERE id_rubrique = ?";
    $reponse = $bdd->prepare($requete);
    $reponse->bindValue(1, $id_rubrique, PDO::PARAM_INT);
    $reponse->execute();
    $donnees = $reponse->fetch();
    if ($donnees != null) {
        $pagePrincipale = encodeUrl($donnees["id_page"]);
    }
    $reponse->closeCursor();
    return $pagePrincipale;
}

function estCache($bdd, $session_id) {
    $valeur = 0;
    if (isset($_SESSION['utilisateur'])) {
        $reponse = $bdd->prepare("SELECT cache FROM membres WHERE id = ?");
        $reponse->bindValue(1, $session_id, PDO::PARAM_INT);
        $reponse->execute();
        $donnees = $reponse->fetch();
        $valeur = $donnees['cache'];
        $reponse->closeCursor();
    }
    return $valeur;
}

function suppressionMembre($bdd, $idConnecte, $idMembre) {
    $pseudo = "";
    $role = "";
    $ok = false;
    // récupération du rôle
    $requete = "SELECT nom FROM membres, roles WHERE membres.id = ? AND membres.id_role = roles.id";
    $reponse = $bdd->prepare($requete);
    $reponse->bindValue(1, $idConnecte, PDO::PARAM_INT);
    $reponse->execute();
    $donnees = $reponse->fetch();
    if ($donnees != null) {
        $role = $donnees['nom'];
    }
    $reponse->closeCursor();

    if ($role != "") {
        // récupération pseudo
        $requete = "SELECT pseudo FROM membres WHERE id = ?";
        $reponse = $bdd->prepare($requete);
        $reponse->bindValue(1, $idMembre, PDO::PARAM_INT);
        $reponse->execute();
        $donnees = $reponse->fetch();
        if ($donnees != null) {
            if (($role == "Administrateur") || ($idMembre == $idConnecte)) {
                $pseudo = $donnees['pseudo'];
                $ok = true;
            }
        }
        $reponse->closeCursor();
        if ($ok) {
            // commentaires
            $requete = "UPDATE commentaires SET id_auteur = null, auteur = ? WHERE id_auteur = ?";
            $reponse = $bdd->prepare($requete);
            $reponse->bindValue(1, $pseudo, PDO::PARAM_STR);
            $reponse->bindValue(2, $idMembre, PDO::PARAM_INT);
            $reponse->execute();
            $reponse->closeCursor();

            // news
            $requete = "UPDATE news SET id_auteur = null, auteur = ? WHERE id_auteur = ?";
            $reponse = $bdd->prepare($requete);
            $reponse->bindValue(1, $pseudo, PDO::PARAM_STR);
            $reponse->bindValue(2, $idMembre, PDO::PARAM_INT);
            $reponse->execute();
            $reponse->closeCursor();

            // suppression
            $requete = "DELETE FROM membres WHERE id = ?";
            $reponse = $bdd->prepare($requete);
            $reponse->bindValue(1, $idMembre, PDO::PARAM_INT);
            $reponse->execute();
            $reponse->closeCursor();
        }
    }
    return $ok;
}

?>
