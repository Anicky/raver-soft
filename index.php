<?php
$pageAccueil = true;
if (!isset($ajax_request)) {
    include_once("haut.php");
} else {
    require_once("bdd_config.php");
    ?>
    <script>
        document.title = "Raver Soft";
    </script>
    <?php
}
?>

<h1>Dernières news
    <a href="news.rss" title="S'abonner au flux RSS de Raver Soft">
        <img src="img/rss.png" alt="RSS" />
    </a>
</h1>
<?php
$numeroPage = 0;
if (isset($_GET['numeroPage'])) {
    $numeroPage = $_GET['numeroPage'];
}
$nombre_news = getParametre($bdd, "nombre_news_page");

$requeteNews = "SELECT news.id AS news_id,
        titre,
        auteur,
        id_auteur,
        DATE_FORMAT(date, '%d/%m/%Y à %Hh%i') AS dateFormatee,
        DATE_FORMAT(date, '%w') AS jourSemaine,
        texte,
        commentaires
        FROM news
        WHERE publiee = true
        ORDER BY date DESC
        LIMIT ?,?";
$numero_depart = $numeroPage * $nombre_news;
$reponseNews = $bdd->prepare($requeteNews);
$reponseNews->bindValue(1, (int) trim($numero_depart), PDO::PARAM_INT);
$reponseNews->bindValue(2, (int) trim($nombre_news), PDO::PARAM_INT);
$reponseNews->execute();
if ($reponseNews->rowCount() > 0) {
    while ($donneesNews = $reponseNews->fetch()) {
        $preview = true;
        require("news-affichage.php");
    }
} else {
    ?>
    <div class="contenuPage">
        Pas de news pour le moment.
    </div>
    <?php
}
$reponseNews->closeCursor();
?>
<!--[if !IE]> <--><script src="js/ajax.js" charset="utf-8"></script><!--> <![endif]-->
<?php
require_once("news-suivant-precedent.php");
include_once("google-analytics.php");
if (!isset($ajax_request)) {
    include_once("bas.php");
}
?>