<?php

$type = "";
$id = "";
if (isset($_GET['id'])) {
    $id = $_GET['id'];
}
if (isset($_GET['type'])) {
    $type = $_GET['type'];
}
require_once("bdd_config.php");
if (($id != "") && ($type != "")) {

    $requete = "SELECT telechargements.id AS idDownload, telechargements.url, extension, types_telechargements.nom AS type
        FROM telechargements, types_telechargements
        WHERE telechargements.id_type = types_telechargements.id
        AND telechargements.url = ?
        AND types_telechargements.url = ?";
    $reponse = $bdd->prepare($requete);
    $reponse->bindValue(1, $id, PDO::PARAM_STR);
    $reponse->bindValue(2, $type, PDO::PARAM_STR);
    $reponse->execute();
    $donnees = $reponse->fetch();
    if ($donnees != null) {
        $url = URL . "ressources/" . encodeUrl($donnees['type']) . "/" . securite_sortie($donnees['url']) . "." . securite_sortie($donnees['extension']);

        if (!estCache($bdd, $session_id)) {
            $requeteVues = "UPDATE telechargements SET nombre = nombre + 1 WHERE id = ?";
            $reponseVues = $bdd->prepare($requeteVues);
            $reponseVues->bindValue(1, $donnees['idDownload'], PDO::PARAM_INT);
            $reponseVues->execute();
            $reponseVues->closeCursor();
        }

        $reponse->closeCursor();
        header("Location: " . $url);
        exit;
    } else {
        $reponse->closeCursor();
        header("Location: " . URL . "page-introuvable.html");
        exit;
    }
} else {
    header("Location: " . URL . "page-introuvable.html");
    exit;
}
?>
