<?php

function getPassageLigne($email) {
    $passage_ligne = "\r\n";
    if (preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $email)) {
        $passage_ligne = "\n";
    }
    return $passage_ligne;
}

function getHeaders($email) {
    $passage_ligne = getPassageLigne($email);
    $entete = "From: \"Raver Soft\" <raversoft@gmail.com>" . $passage_ligne;
    $entete .= "Reply-To: \"Raver Soft\" <raversoft@gmail.com>" . $passage_ligne;
    $entete .= "MIME-Version: 1.0" . $passage_ligne;
    $entete .= "Content-Type: text/html; charset=utf-8" . $passage_ligne;
    $entete .= "Content-Transfer-Encoding: 8bit" . $passage_ligne;
    $entete .= "Date: " . date("D, j M Y H:i:s -0600") . $passage_ligne;
    return $entete;
}

function mail_inscription($pseudo, $password, $email) {
    $pseudo = securite_sortie($pseudo);
    $password = securite_sortie($password);
    $email = securite_sortie($email);
    $lien = URL . "compte-activation-" . crypterSansSalt($pseudo) . "-" . crypter($password) . ".html";
    $message = "Bonjour " . $pseudo . ",<br /><br />
        Vous venez de vous inscrire sur Raver Soft !<br /><br />
        Votre login : " . $pseudo . "<br />
        Votre mot de passe : " . $password . "<br /><br />
        Afin de pouvoir vous connecter, vous devez d'abord activer votre compte.<br />
        Pour cela, cliquez sur le lien suivant (ou faites un copier-coller du lien et inscrivez-le dans votre navigateur) :<br /><br />
        <a href=\"" . $lien . "\" title=\"Activation de votre compte\">" . $lien . "</a><br /><br />
        L'administrateur de Raver Soft";
    $sujet = "[Raver Soft] Confirmation d'inscription";
    return mail($email, $sujet, $message, getHeaders($email));
}

function mail_motDePasseOublie($pseudo, $password, $email) {
    $pseudo = securite_sortie($pseudo);
    $password = securite_sortie($password);
    $email = securite_sortie($email);
    $lien = URL . "reinitialisation-mot-de-passe-" . crypterSansSalt($pseudo) . "-" . crypter($password) . ".html";
    $message = "Bonjour " . $pseudo . ",<br /><br />
        Ce mail vous est envoyé car vous avez oublié votre mot de passe.<br /><br />
        Si vous souhaitez réinitialiser votre mot de passe et en obtenir ainsi un nouveau,
        cliquez sur le lien suivant (ou faites un copier-coller du lien et inscrivez-le dans votre navigateur) :<br /><br />
        <a href=\"" . $lien . "\" title=\"Réinitialisation de votre mot de passe\">" . $lien . "</a><br /><br />
        Si ce mail vous a été envoyé par erreur, merci de ne pas en tenir compte et de le supprimer.<br /><br />
        L'administrateur de Raver Soft";
    $sujet = "[Raver Soft] Mot de passe oublié";
    return mail($email, $sujet, $message, getHeaders($email));
}

function mail_reinitialisationMotDePasse($pseudo, $password, $email) {
    $pseudo = securite_sortie($pseudo);
    $password = securite_sortie($password);
    $email = securite_sortie($email);
    $message = "Bonjour " . $pseudo . ",<br /><br />
        Vous venez de demander à réinitialiser votre mot de passe. Voici vos identifiants : <br /><br />
        Votre login : " . $pseudo . "<br />
        Votre mot de passe : " . $password . "<br /><br />
        Après vous être connecté, vous pourrez changer votre mot de passe dans les paramètres de votre compte.<br /><br />
        L'administrateur de Raver Soft";
    $sujet = "[Raver Soft] Réinitialisation du mot de passe";
    return mail($email, $sujet, $message, getHeaders($email));
}

function mail_motDePasseModifie($pseudo, $password, $email) {
    $pseudo = securite_sortie($pseudo);
    $password = securite_sortie($password);
    $email = securite_sortie($email);
    $message = "Bonjour " . $pseudo . ",<br /><br />
        Ce mail vous est envoyé car vous avez modifié votre mot de passe.<br /><br />
        Votre nouveau mot de passe est : " . $password . "<br /><br />
        Conservez-le précieusement, vous êtes le seul à connaître.<br /><br />
        Si vous l'oubliez, il vous sera toujours possible d'en récupérer un nouveau sur le site.<br /><br />
        L'administrateur de Raver Soft";
    $sujet = "[Raver Soft] Mot de passe modifié";
    return mail($email, $sujet, $message, getHeaders($email));
}

function mail_suppressionCompte($pseudo, $password, $email) {
    $pseudo = securite_sortie($pseudo);
    $email = securite_sortie($email);
    $lien = URL . "compte-suppression-" . crypterSansSalt($pseudo) . "-" . crypter($password) . ".html";
    $message = "Bonjour " . $pseudo . ",<br /><br />
        Ce mail vous est envoyé car vous désirez supprimer votre compte.<br /><br />
        Si vous souhaitez réellement supprimer votre compte,
        cliquez sur le lien suivant (ou faites un copier-coller du lien et inscrivez-le dans votre navigateur) :<br /><br />
        <a href=\"" . $lien . "\" title=\"Suppression de votre compte\">" . $lien . "</a><br /><br />
        Si ce mail vous a été envoyé par erreur, merci de ne pas en tenir compte et de le supprimer.<br /><br />
        L'administrateur de Raver Soft";
    $sujet = "[Raver Soft] Suppression du compte";
    return mail($email, $sujet, $message, getHeaders($email));
}

?>