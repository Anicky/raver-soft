<?php
$pageTitre = "Gestion du compte";
$pageCompte = true;
include_once("haut.php");
require_once("acces-compte2.php");

$pseudo = "";
$email = "";
$avatar = "";
$cache = 0;
$image_tailleMax_ko = getParametre($bdd, "image_tailleMax_ko");

$requete = "SELECT pseudo, email, avatar, cache FROM membres WHERE id = ?";
$reponse = $bdd->prepare($requete);
$reponse->bindValue(1, $session_id, PDO::PARAM_INT);
$reponse->execute();
$donnees = $reponse->fetch();
if (($donnees != null)) {
    $pseudo = securite_sortie($donnees['pseudo']);
    $email = securite_sortie($donnees['email']);
    $avatar = securite_sortie($donnees['avatar']);
    $cache = $donnees['cache'];
} else {
    header("Location: " . URL . "page-introuvable.html");
    exit;
}
$reponse->closeCursor();
?>

<h1><?php echo $pageTitre; ?></h1>

<form method="post" action="compte-modifier.html" class="centre" id="formCompte">
    <div class="contenuPage">
        <table class="formulaire">
            <tr>
                <td class="label">
                    <label for="pseudo">Pseudo</label>
                </td>
                <td>
                    <input type="text" name="pseudo" id="pseudo" value="<?php echo $pseudo; ?>" size="40" maxlength="100" onkeyup="verifier_infos()" />
                    <div id="verification_pseudo" class="verification"></div>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="email">Email</label>
                </td>
                <td>
                    <input type="text" name="email" id="email" value="<?php echo $email; ?>" size="40" maxlength="255" onkeyup="verifier_infos()" />
                    <div id="verification_email" class="verification"></div>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="avatar">Avatar</label>
                </td>
                <td>
                    <div id="checkbox_avatar">
                        <input type="radio" name="selection_avatar" id="selection_avatar_aucun" value="aucun"<?php
if ($avatar == "") {
    echo "checked=\"checked\"";
}
?> />
                        <label for="selection_avatar_aucun">Pas d'avatar</label>
                        <?php if ($avatar != "") { ?>
                            <input type="radio" name="selection_avatar" id="selection_avatar_garder" value="garder"<?php
                        if ($avatar != "") {
                            echo "checked=\"checked\"";
                        }
                            ?> />
                            <label for="selection_avatar_garder">Garder l'avatar actuel</label>
                        <?php } ?>
                        <input type="radio" name="selection_avatar" id="selection_avatar_choisir" value="choisir" />
                        <label for="selection_avatar_choisir">Choisir un avatar</label>
                    </div>
                    <div id="avatar_garder">
                        <?php if ($avatar != "") {
                            ?>
                            <img src="img/avatars/<?php echo $avatar; ?>" alt="Avatar introuvable" />
                            <?php
                        }
                        ?>
                    </div>
                    <div id="avatar_choisir">
                        <input type="file" name="avatar" id="avatar" size="40" />
                        <div class="warning">La taille maximum pour l'envoi d'une image est de <?php echo $image_tailleMax_ko; ?> Ko.</div>
                    </div>
                </td>
            </tr>
            <?php
            if (($session_role == "Administrateur") || ($session_role == "Membre")) {
                ?>
                <tr>
                    <td class="label">
                        <label for="avatar">Mode invisible</label>
                    </td>
                    <td>
                        <div id="checkbox_invisible">
                            <input type="radio" name="cache" id="cache_off" value="0"<?php
            if (!$cache) {
                echo "checked=\"checked\"";
            }
                ?> />
                            <label for="cache_off">Désactivé</label>
                            <input type="radio" name="cache" id="cache_on" value="1"<?php
                               if ($cache) {
                                   echo "checked=\"checked\"";
                               }
                ?> />
                            <label for="cache_on">Activé</label>
                        </div>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
    </div>
    <input type="hidden" name="tokenValidation" value="<?php echo $_SESSION['tokenValidation']; ?>" />
    <input class="bouton" id="removeAccount" type="button" onclick="supprimerCompte()" value="Supprimer le compte" />
    <input class="bouton" id="changePassword" type="button" onclick="modifierMotDePasse()" value="Changer le mot de passe" />
    <input class="bouton" type="submit" value="Sauvegarder" id="boutonSubmit" />
</form>
<?php
if ($avatar != "") {
    ?>
    <script>
        $("#avatar_garder").show();
    </script>
    <?php
}
?>
<div id="dialogbox">
    <?php require_once("loading.php"); ?>
</div>
<div id="dialogbox2">
    <?php require_once("loading.php"); ?>
</div>
<div id="dialogbox3">
    <?php require_once("loading.php"); ?>
</div>
<script>
    $(document).ready(function() {
        $("#checkbox_avatar").buttonset();
<?php
if (($session_role == "Administrateur") || ($session_role == "Membre")) {
    ?>
                $("#checkbox_invisible").buttonset();
    <?php
}
?>
        $("#dialogbox").dialog({
            autoOpen : false,
            modal : true,
            resizable : false,
            draggable : false,
            show : "fade",
            hide : "fade",
            title : "Supprimer le compte",
            buttons : {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            }
        });
        $("#dialogbox2").dialog({
            autoOpen : false,
            width : 400,
            modal : true,
            resizable : false,
            draggable : false,
            show : "fade",
            hide : "fade",
            title : "Changer le mot de passe",
            buttons : {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            }
        });
        $("#dialogbox3").dialog({
            autoOpen : false,
            modal : true,
            resizable : false,
            draggable : false,
            show : "fade",
            hide : "fade",
            title : "Modifier les informations du compte",
            buttons : {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            }
        });
    });
    $("input:radio[name=selection_avatar]").change(function() {
        choix_avatar = $(this).val();
        if (choix_avatar == 'aucun') {
            $("#avatar_garder").hide();
            $("#avatar_choisir").hide();
        } else if (choix_avatar == 'garder') {
            $("#avatar_garder").show();
            $("#avatar_choisir").hide();
        } else if (choix_avatar == 'choisir') {
            $("#avatar_garder").hide();
            $("#avatar_choisir").show();
        }
    });
    $("#formCompte").validate({
        rules: {
            pseudo : {
                required : true,
                maxlength : 100
            },
            email : {
                required : true,
                email : true,
                maxlength : 255
            }
        },
        messages: { 
            pseudo : {
                required : "Vous devez rentrer un pseudo.",
                maxlength : "Le pseudo ne peut pas dépasser 100 caractères."
            },
            email : {
                required : "Vous devez rentrer un email valide.",
                email : "Vous devez rentrer un email valide.",
                maxlength : "L'email ne peut pas dépasser 255 caractères."
            }
        } 
    });
    $("#formCompte").ajaxForm({
        target: "#dialogbox3",
        beforeSubmit : function() {
            $("#dialogbox3").dialog('open');
        }
    });
    function supprimerCompte() {
        $.post("compte-supprimer.html", {}, function(html) {
            $("#dialogbox").dialog('option', 'buttons', {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            });
            $("#dialogbox").html(html);
            $("#dialogbox").dialog('open');
        });
    }
    function modifierMotDePasse() {
        $.post("compte-mot-de-passe.html", {}, function(html) {
            $("#dialogbox2").dialog('option', 'buttons', {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            });
            $("#dialogbox2").html(html);
            $("#dialogbox2").dialog('open');
        });
    }
    function verifier_infos() {
        $("#boutonSubmit").removeAttr("disabled");
        var idMembre = '<?php echo encodeUrl($pseudo); ?>';
        var pseudo = $("#pseudo").val();
        var email = $("#email").val();
        if (pseudo != "" || email != "") {
            if (idMembre != '') {
                $.get("membres-verification.html", {champ : 'pseudo', valeur : pseudo, id : idMembre}, function(html) {
                    $("#verification_pseudo").html(html);
                });
                $.get("membres-verification.html", {champ : 'email', valeur : email, id : idMembre}, function(html) {
                    $("#verification_email").html(html);
                });
            } else {
                $.get("membres-verification.html", {champ : 'pseudo', valeur : pseudo}, function(html) {
                    $("#verification_pseudo").html(html);
                });
                $.get("membres-verification.html", {champ : 'email', valeur : email}, function(html) {
                    $("#verification_email").html(html);
                });
            }
        }
    }
</script>
<?php
include_once("google-analytics.php");
include_once("bas.php");
?>