<?php
require_once("bdd_config.php");

if (!isset($_SESSION['utilisateur'])) {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php
} else {
    $id = "";
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
        $ok = false;
        $requete = "SELECT id_auteur FROM commentaires WHERE id = ?";
        $reponse = $bdd->prepare($requete);
        $reponse->bindValue(1, $id, PDO::PARAM_INT);
        $reponse->execute();
        $donnees = $reponse->fetch();
        if (($donnees['id_auteur'] == $session_id) || ($session_role == "Administrateur")) {
            $ok = true;
        }
        $reponse->closeCursor();
        if ($ok) {
            ?>
            <script>
                $("#dialogbox").dialog('option', 'buttons', { 
                    "Non" : function() {
                        $(this).dialog("close");
                    },
                    "Oui" : function() {
                        $.post("commentaires-suppression.html", {id : '<?php echo $id; ?>'}, function(html) {
                            $("#dialogbox").dialog('option', 'buttons', {
                                "Fermer" : function() {
                                    $(this).dialog("close");
                                }
                            });
                            $("#dialogbox").html(html);
                        });
                    }
                });
            </script>
            <p>Etes-vous sûr de vouloir supprimer ce commentaire ?</p>
            <?php
        } else {
            ?>
            <p>Le commentaire recherché est introuvable.</p>
            <?php
        }
    } else {
        ?>
        <p>Le commentaire recherché est introuvable.</p>
        <?php
    }
}
?>