<?php
require_once("bdd_config.php");

$news_id = "";
if (isset($_GET["id"])) {
    $news_id = $_GET["id"];
}

if ($news_id != "") {
    $requeteNews = "SELECT id AS news_id,
        titre,
        DATE_FORMAT(date, '%d/%m/%Y à %Hh%i') AS dateFormatee,
        DATE_FORMAT(date, '%w') AS jourSemaine,
        auteur,
        id_auteur,
        texte,
        commentaires
        FROM news
        WHERE id = ?";
    $reponseNews = $bdd->prepare($requeteNews);
    $reponseNews->bindValue(1, $news_id, PDO::PARAM_INT);
    $reponseNews->execute();
    $donneesNews = $reponseNews->fetch();
    if ($donneesNews != null) {
        $pageTitre = securite_sortie($donneesNews['titre']);

        if (!estCache($bdd, $session_id)) {
            $requeteVues = "UPDATE news SET vues = vues + 1 WHERE id = ?";
            $reponseVues = $bdd->prepare($requeteVues);
            $reponseVues->bindValue(1, $donneesNews['news_id'], PDO::PARAM_INT);
            $reponseVues->execute();
            $reponseVues->closeCursor();
        }

        if (!isset($ajax_request)) {
            include_once("haut.php");
        } else {
            ?>
            <script>
                document.title = "Raver Soft - " + "<?php echo $pageTitre; ?>";
            </script>
            <?php
        }
        ?>
        <h1><?php echo $pageTitre; ?></h1>
        <?php
        require("news-affichage.php");

        if ($donneesNews['commentaires']) {
            ?>
            <div id="commentaires">
                <div id="liste_commentaires">
                    <?php
                    require_once("commentaires-liste.php");
                    ?>
                </div>
                <form class="centre" id="envoyerCommentaire" method="post" action="commentaires-ajout.html">
                    <div class="contenuPage">
                        <h2>Rédiger un commentaire</h2>
                        <table class="formulaire">
                            <?php if (!isset($_SESSION['utilisateur'])) { ?>
                                <tr>
                                    <td class="label">
                                        <label for="pseudo">Pseudo</label>
                                    </td>
                                    <td>
                                        <input type="text" name="pseudo" id="pseudo" size="40" maxlength="100" onkeyup="verifier_pseudo()" />
                                        <div id="verification_pseudo" class="verification"></div>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td class="label">
                                    <label for="commentaire">Commentaire</label>
                                </td>
                                <td>
                                    <textarea name="commentaire" id="commentaire" rows="10" cols="100"></textarea>
                                </td>
                            </tr>
                            <?php if (!isset($_SESSION['utilisateur'])) { ?>
                                <tr>
                                    <td class="label">
                                        <label for="securite">Mesure de sécurité :<br />2 + 2 = ?</label>
                                    </td>
                                    <td>
                                        <input type="text" name="securite" id="securite" size="5" maxlength="5" />
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                    <input type="hidden" name="id_news" value="<?php echo $news_id; ?>" />
                    <input type="hidden" name="tokenValidation" value="<?php echo $_SESSION['tokenValidation']; ?>" />
                    <input class="bouton" type="submit" value="Envoyer" id="boutonSubmit" />
                </form>
            </div>
            <div id="dialogbox">
                <?php require_once("loading.php"); ?>
            </div>
            <script>
                $(document).ready(function() {
                    $("#dialogbox").dialog({
                        autoOpen: false,
                        modal: true,
                        resizable: false,
                        draggable: false,
                        show: "fade",
                        hide: "fade",
                        title: "Ajout d'un commentaire",
                        buttons: {
                            "Fermer": function() {
                                $(this).dialog("close");
                            }
                        }
                    });
                });
                function verifier_pseudo() {
                    var pseudo = $("#pseudo").val();
                    if (pseudo != "") {
                        $.get("membres-verification.html", {champ: 'pseudo', valeur: pseudo}, function(html) {
                            $("#verification_pseudo").html(html);
                        });
                    }
                }
                $("#envoyerCommentaire").validate({
                    rules: {
                        pseudo: {
                            maxlength: 100
                        },
                        commentaire: {
                            required: true
                        }
                    },
                    messages: {
                        pseudo: {
                            maxlength: "Le pseudo ne peut pas dépasser 100 caractères."
                        },
                        commentaire: {
                            required: "Vous devez rentrer un commentaire."
                        }
                    }
                });
                $("#envoyerCommentaire").ajaxForm({
                    target: "#dialogbox",
                    beforeSubmit: function() {
                        $("#dialogbox").dialog('open');
                    }
                });
            </script>
            <?php
        }
    } else {
        header("Location: " . URL . "page-introuvable.html");
        exit;
    }
    $reponseNews->closeCursor();
} else {
    header("Location: " . URL . "page-introuvable.html");
    exit;
}
?>
<script>

    var linkEls = $('a.ajax');
    for (var i = 0, l = linkEls.length; i < l; i++) {
        linkEls[i].addEventListener('click', clickHandler, true);
    }

</script>
<?php
include_once("google-analytics.php");
if (!isset($ajax_request)) {
    include_once("bas.php");
}
?>