<div class="contenuPage" id="commentaire_<?php echo $donneesCommentaires['idCommentaire']; ?>">
    <?php
    $pseudo = "Anonyme";
    $avatar = "";
    $email = "";
    $idAuteur = "";
    if ($donneesCommentaires['id_auteur'] != null) {
        $requeteAuteur = "SELECT pseudo, email, avatar, membres.id AS idAuteur
                                    FROM membres, commentaires
                                    WHERE commentaires.id_auteur = membres.id
                                    AND commentaires.id = ?";
        $reponseAuteur = $bdd->prepare($requeteAuteur);
        $reponseAuteur->bindValue(1, $donneesCommentaires['idCommentaire'], PDO::PARAM_INT);
        $reponseAuteur->execute();
        $donneesAuteur = $reponseAuteur->fetch();
        $idAuteur = $donneesAuteur['idAuteur'];
        $avatar = securite_sortie($donneesAuteur['avatar']);
        $email = securite_sortie($donneesAuteur['email']);
        $pseudo = securite_sortie($donneesAuteur['pseudo']);
        $reponseAuteur->closeCursor();
    } else {
        $pseudo = securite_sortie($donneesCommentaires['auteur']);
    }
    if ($avatar != null) {
        ?>
        <img src="img/avatars/<?php echo $avatar; ?>" alt="Avatar" class="avatar" />
        <?php
    } else {
        ?>
        <img src="img/no_avatar.png" alt="Avatar" class="avatar" />
        <?php
    }
    ?>
    Posté par <span class="auteur">
        <?php if ($email != "") {
            ?>
            <a href="mailto:<?php echo $email; ?>" title="Envoyer un mail"><?php echo $pseudo; ?></a>
            <?php
        } else {
            echo $pseudo;
        }
        ?></span> -
    <span class="date">
        <?php echo jourSemaine($donneesCommentaires['jourCommentaire']) . " " . $donneesCommentaires['dateCommentaire']; ?>
    </span>
    <?php
    if (isset($_SESSION['utilisateur'])) {
        if (($session_role == "Administrateur") || ($idAuteur == $session_id)) {
            ?>
            <div class="droite">
                <a href="javascript:void(0)" title="Modifier le commentaire" onclick="modifier_commentaire(<?php echo $donneesCommentaires['idCommentaire']; ?>)"><img src="img/edit.png" alt="" /></a>
                <a href="javascript:void(0)" title="Supprimer le commentaire" onclick="supprimer_commentaire(<?php echo $donneesCommentaires['idCommentaire']; ?>)"><img src="img/delete.png" alt="" /></a>
            </div>
            <script>
                function modifier_commentaire(id) {
                    $("#dialogbox").dialog('option', 'buttons', { 
                        "Fermer" : function() {
                            $(this).dialog("close");
                        }
                    });
                    $.post("commentaires-modifier.html", {id : id}, function(html) {
                        $("#dialogbox").html(html);
                        $("#dialogbox").dialog('open');
                    });    
                }
                function supprimer_commentaire(id) {
                    $("#dialogbox").dialog('option', 'buttons', { 
                        "Fermer" : function() {
                            $(this).dialog("close");
                        }
                    });
                    $.post("commentaires-supprimer.html", {id : id}, function(html) {
                        $("#dialogbox").html(html);
                        $("#dialogbox").dialog('open');
                    });    
                }
            </script>
            <?php
        }
    }
    ?>
    <hr />
    <?php echo nl2br(securite_sortie($donneesCommentaires['texteCommentaire'])); ?>
    <div class="clear"></div>
</div>