<?php
if (isset($_GET['id_news'])) {
    require_once("bdd_config.php");
    $news_id = $_GET['id_news'];
}
$requeteCommentaires = "SELECT commentaires.texte AS texteCommentaire,
                            commentaires.id_auteur AS id_auteur,
                            commentaires.auteur,
                            commentaires.id AS idCommentaire,
                            DATE_FORMAT(commentaires.date, '%d/%m/%Y à %Hh%i') AS dateCommentaire,
                            DATE_FORMAT(commentaires.date, '%w') AS jourCommentaire
                            FROM commentaires, news
                            WHERE commentaires.id_news = news.id
                            AND news.id = ?
                            ORDER BY commentaires.date ASC";
$reponseCommentaires = $bdd->prepare($requeteCommentaires);
$reponseCommentaires->bindValue(1, $news_id, PDO::PARAM_INT);
$reponseCommentaires->execute();
$nombreCommentaires = $reponseCommentaires->rowCount();
$i = 0;
if ($nombreCommentaires > 0) {
    ?>    
    <div class="titre">Commentaires</div>
    <?php
    while ($donneesCommentaires = $reponseCommentaires->fetch()) {
        $i++;
        require("commentaires-affichage.php");
    }
    ?>
    <?php
}
$reponseCommentaires->closeCursor();
?>