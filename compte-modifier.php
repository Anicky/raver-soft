<?php

require_once("bdd_config.php");
require_once("acces-compte2.php");

if (isset($_POST['pseudo'])) {
    // Variables
    $pseudo = $_POST['pseudo'];
    $email = $_POST['email'];
    $avatar = "";
    // Traitement
    if ($_SESSION['tokenValidation'] == $_POST['tokenValidation']) {
        if ($pseudo != "" && $email != "") {
            if (($session_role == "Administrateur") || ($session_role == "Membre")) {
                $cache = $_POST['cache'];
                $requete = "UPDATE membres SET cache = ? WHERE id = ?";
                $reponse = $bdd->prepare($requete);
                $reponse->bindValue(1, $cache, PDO::PARAM_BOOL);
                $reponse->bindValue(2, $session_id, PDO::PARAM_INT);
                $reponse->execute();
                $reponse->closeCursor();
            }

            if ($_POST['selection_avatar'] == "garder") {
                $requete = "UPDATE membres SET pseudo = ?, email = ? WHERE id = ?";
                $reponse = $bdd->prepare($requete);
                $reponse->bindValue(1, $pseudo, PDO::PARAM_STR);
                $reponse->bindValue(2, $email, PDO::PARAM_STR);
                $reponse->bindValue(3, $session_id, PDO::PARAM_INT);
                $reponse->execute();
                $reponse->closeCursor();
            } else if ($_POST['selection_avatar'] == "choisir") {
                if (isset($_FILES['avatar'])) {
                    $tailleMax = getParametre($bdd, "image_tailleMax_ko");
                    $avatar = envoiImage($_FILES['avatar'], $tailleMax);
                }
                $requete = "UPDATE membres SET pseudo = ?, email = ?, avatar = ? WHERE id = ?";
                $reponse = $bdd->prepare($requete);
                $reponse->bindValue(1, $pseudo, PDO::PARAM_STR);
                $reponse->bindValue(2, $email, PDO::PARAM_STR);
                $reponse->bindValue(3, $avatar, PDO::PARAM_STR);
                $reponse->bindValue(4, $session_id, PDO::PARAM_INT);
                $reponse->execute();
                $reponse->closeCursor();
            } else {
                $requete = "UPDATE membres SET pseudo = ?, email = ?, avatar = '' WHERE id = ?";
                $reponse = $bdd->prepare($requete);
                $reponse->bindValue(1, $pseudo, PDO::PARAM_STR);
                $reponse->bindValue(2, $email, PDO::PARAM_STR);
                $reponse->bindValue(3, $session_id, PDO::PARAM_INT);
                $reponse->execute();
                $reponse->closeCursor();
            }
            ?>
            <script>
                $("#dialogbox3").bind('dialogclose', function() {
                    window.location.href = "compte.html";
                });
            </script>
            <p>Vos informations ont bien été modifiées.</p>
            <?php

        } else {
            ?>
            <p>Vous n'avez pas rempli tous les champs.</p>
            <?php

        }
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>