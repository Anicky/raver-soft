<?php

require_once("bdd_config.php");

if (!isset($_SESSION['utilisateur'])) {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

} else {
    ?>
    <script>
        $("#dialogbox").dialog('option', 'buttons', { 
            "Fermer" : function() {
                $(this).dialog("close");
            }
        });
    </script>
    <?php

    $id = "";
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
        $id_news = "";
        $ok = false;
        $requete = "SELECT id_auteur, id_news FROM commentaires WHERE id = ?";
        $reponse = $bdd->prepare($requete);
        $reponse->bindValue(1, $id, PDO::PARAM_INT);
        $reponse->execute();
        $donnees = $reponse->fetch();
        if (($donnees['id_auteur'] == $session_id) || ($session_role == "Administrateur")) {
            $id_news = $donnees['id_news'];
            $ok = true;
        }
        $reponse->closeCursor();
        if ($ok) {
            $requete = "DELETE FROM commentaires WHERE id = ?";
            $reponse = $bdd->prepare($requete);
            $reponse->bindValue(1, $id, PDO::PARAM_INT);
            $reponse->execute();
            $reponse->closeCursor();
            ?>
            <script>
                $.get("commentaires-liste.html", {id_news : '<?php echo $id_news; ?>'}, function(html) {
                    $("#liste_commentaires").html(html);
                });
                $.get("commentaires-nombre.html", {id_news : '<?php echo $id_news; ?>'}, function(html) {
                    $("#commentaires_nombre").html(html);
                });
            </script>
            <p>Le commentaire a bien été supprimé.</p>
            <?php

        } else {
            ?>
            <p>Impossible de supprimer le commentaire.</p>
            <?php

        }
    } else {
        ?>
        <p>Impossible de supprimer le commentaire.</p>
        <?php

    }
}
?>