<?php

require_once("bdd_config.php");
if (isset($_SESSION['utilisateur'])) {
    $requete = "UPDATE membres SET token = NULL WHERE id = ?";
    $reponse = $bdd->prepare($requete);
    $reponse->bindValue(1, $session_id, PDO::PARAM_INT);
    $reponse->execute();
    $reponse->closeCursor();
    session_destroy();
    header('Location: ' . $_SERVER['HTTP_REFERER']);
    exit;
} else {
    header("Location: ./");
    exit;
}
?>