<?php

$commentaires_actives = false;

if (isset($_GET['id_news'])) {
    require_once("bdd_config.php");
    $news_id = securite_bdd($_GET['id_news']);
    $nombre_commentaires = nombreElements($bdd, "commentaires WHERE id_news = " . $news_id);
    $commentaires_actives = true;
} else {
    $commentaires_actives = $donneesNews['commentaires'];
}
if ($commentaires_actives) {
    if ($nombre_commentaires == 1) {
        echo $nombre_commentaires . " commentaire";
    } else {
        echo $nombre_commentaires . " commentaires";
    }
} else {
    echo "Commentaires désactivés pour cet article";
}
?>