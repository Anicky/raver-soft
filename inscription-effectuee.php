<?php

require_once("bdd_config.php");
require_once("acces-compte.php");

if (isset($_POST['pseudo'])) {
    // Variables
    $pseudo = $_POST['pseudo'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $avatar = "";
    // Traitement
    if ($_SESSION['tokenValidation'] == $_POST['tokenValidation']) {
        if ($pseudo != "" && $email != "" && $password != "") {
            if (isset($_FILES['avatar'])) {
                $tailleMax = getParametre($bdd, "image_tailleMax_ko");
                $avatar = envoiImage($_FILES['avatar'], $tailleMax);
            }
            $requete = "INSERT INTO membres(pseudo, password, email, avatar) VALUES (?, ?, ?, ?)";
            $reponse = $bdd->prepare($requete);
            $reponse->bindValue(1, $pseudo, PDO::PARAM_STR);
            $reponse->bindValue(2, crypter($password), PDO::PARAM_STR);
            $reponse->bindValue(3, $email, PDO::PARAM_STR);
            $reponse->bindValue(4, $avatar, PDO::PARAM_STR);
            $reponse->execute();
            $reponse->closeCursor();
            ?>
            <script>
                $("#dialogbox").bind('dialogclose', function() {
                    window.location.href = "./";
                });
            </script>
            <p>Vous êtes bien inscrit.</p>
            <?php

            require_once("fonctions-mails.php");
            if (mail_inscription($pseudo, $password, $email)) {
                ?>
                <p>Un mail de confirmation d'inscription vient de vous être envoyé.</p>
                <?php

            } else {
                ?>
                <p>Le mail de confirmation d'inscription n'a pas pu être envoyé.</p>
                <?php

            }
        } else {
            ?>
            <p>Vous n'avez pas rempli tous les champs.</p>
            <?php

        }
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>