<?php

require_once("bdd_config.php");
require_once("acces-compte2.php");

if (isset($_POST['password'])) {
    // Variables
    $password = $_POST['password'];
    // Traitement
    if ($_SESSION['tokenValidation'] == $_POST['tokenValidation']) {
        if ($password != "") {
            $requete = "SELECT pseudo, email, password FROM membres WHERE id = ?";
            $reponse = $bdd->prepare($requete);
            $reponse->bindValue(1, $session_id, PDO::PARAM_INT);
            $reponse->execute();
            $donnees = $reponse->fetch();
            if ($donnees != null) {
                $pseudo = securite_sortie($donnees['pseudo']);
                $email = securite_sortie($donnees['email']);
                if (testerMotDePasse($password, $donnees['password']) == $donnees['password']) {
                    ?>
                    <script>
                        $("#dialogbox").dialog('option', 'buttons', { 
                            "Fermer" : function() {
                                $(this).dialog("close");
                            }
                        });
                        $("#dialogbox").bind('dialogclose', function() {
                            window.location.href = "./";
                        });
                    </script>
                    <?php

                    require_once("fonctions-mails.php");
                    if (mail_suppressionCompte($pseudo, $password, $email)) {
                        ?>
                        <p>Un mail vient de vous être envoyé pour que vous puissiez supprimer votre compte.</p>
                        <?php

                    } else {
                        ?>
                        <p>Une erreur s'est produite : l'envoi du mail pour que vous puissiez supprimer votre compte n'a pas pu être effectué.</p>
                        <?php

                    }
                } else {
                    ?>
                    <p>Le mot de passe que vous avez indiqué ne correspond pas à votre mot de passe actuel.</p>
                    <?php

                }
            } else {
                ?>
                <p>Impossible de trouver votre compte.</p>
                <?php

            }
            $reponse->closeCursor();
        } else {
            ?>
            <p>Vous n'avez pas rempli tous les champs.</p>
            <?php

        }
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>