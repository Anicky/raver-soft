<?php
require_once("bdd_config.php");
if (!isset($_SESSION['utilisateur'])) {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php
} else {
    ?>
<p>Pour supprimer votre compte, veuillez indiquer votre mot de passe actuel.<br /><br />
    Vous recevrez ensuite un email pour supprimer définitivement votre compte.<br /><br /></p>
    <form action="compte-supprimer-effectue.html" method="post" id="formSuppression">
        <label for="password">Mot de passe</label><br /><br />
        <input type="password" name="password" id="password" size="40" maxlength="100" /><br />
        <input type="hidden" name="tokenValidation" value="<?php echo $_SESSION['tokenValidation']; ?>" />
    </form>
    <script>
        $("#formSuppression").validate({
            rules: {
                password : {
                    required : true
                }
            },
            messages: { 
                password : {
                    required : "Vous devez rentrer votre mot de passe."
                }
            } 
        });
        $("#formSuppression").ajaxForm({
            target: "#dialogbox",
            beforeSubmit : function() {
                $("#dialogbox").dialog('option', 'buttons', {
                    "Fermer" : function() {
                        $(this).dialog("close");
                    }
                });
            }
        });
        $("#dialogbox").dialog('option', 'buttons', { 
            "Annuler" : function() {
                $(this).dialog("close");
            },
            "Valider" : function() {
                $("#formSuppression").submit();
            }
        });
    </script>
    <?php
}
?>