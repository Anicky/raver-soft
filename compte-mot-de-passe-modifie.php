<?php

require_once("bdd_config.php");
require_once("acces-compte2.php");

if (isset($_POST['mdp_ancien'])) {
    // Variables
    $mdp_ancien = $_POST['mdp_ancien'];
    $mdp_nouveau1 = $_POST['mdp_nouveau1'];
    $mdp_nouveau2 = $_POST['mdp_nouveau2'];
    // Traitement
    if ($_SESSION['tokenValidation'] == $_POST['tokenValidation']) {
        if (($mdp_ancien != "") && ($mdp_nouveau1 != "") && ($mdp_nouveau2 != "")) {
            if ($mdp_nouveau1 == $mdp_nouveau2) {

                $requete = "SELECT pseudo, password, email FROM membres WHERE id = ?";
                $reponse = $bdd->prepare($requete);
                $reponse->bindValue(1, $session_id, PDO::PARAM_INT);
                $reponse->execute();
                $donnees = $reponse->fetch();
                if ($donnees != null) {
                    $pseudo = $donnees['pseudo'];
                    $email = $donnees['email'];

                    if (testerMotDePasse($mdp_ancien, $donnees['password']) == $donnees['password']) {
                        $password = crypter($mdp_nouveau1);
                        $requete2 = "UPDATE membres SET password = ? WHERE id = ?";
                        $reponse2 = $bdd->prepare($requete2);
                        $reponse2->bindValue(1, $password, PDO::PARAM_STR);
                        $reponse2->bindValue(2, $session_id, PDO::PARAM_INT);
                        $reponse2->execute();
                        $reponse2->closeCursor();
                        ?>
                        <script>
                            $("#dialogbox2").dialog('option', 'buttons', { 
                                "Fermer" : function() {
                                    $(this).dialog("close");
                                }
                            });
                        </script>
                        <p>Votre mot de passe a bien été modifié.</p>
                        <?php

                        require_once("fonctions-mails.php");
                        mail_motDePasseModifie($pseudo, $mdp_nouveau1, $email);
                    } else {
                        ?>
                        <p>L'ancien mot de passe que vous avez indiqué ne correspond pas à votre mot de passe actuel.</p>
                        <?php

                    }
                } else {
                    ?>
                    <p>Impossible de trouver votre compte.</p>
                    <?php

                }
                $reponse->closeCursor();
            } else {
                ?>
                <p>Le mot de passe de confirmation que vous avez entré ne correspond pas au nouveau mot de passe demandé.</p>
                <?php

            }
        } else {
            ?>
            <p>Vous n'avez pas rempli tous les champs.</p>
            <?php

        }
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>