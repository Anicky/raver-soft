<?php
$pageTitre = "Suppression de votre compte";
include_once("haut.php");
?>
<h1><?php echo $pageTitre; ?></h1>
<div class="contenuPage">
    <?php
    if (isset($_GET['pseudo'])) {
        $pseudo = $_GET['pseudo'];
        $password = $_GET['password'];
        if (($pseudo != "") && ($password != "")) {
            $requete = "SELECT id, pseudo, password FROM membres";
            $reponse = $bdd->query($requete);
            $trouve = false;
            $id = "";
            while ((!$trouve) && ($donnees = $reponse->fetch())) {
                if ($pseudo == (crypterSansSalt($donnees['pseudo'])) && (testerMotDePasse($password, $donnees['password']))) {
                    $id = $donnees['id'];
                    $trouve = true;
                }
            }
            if ($trouve) {
                suppressionMembre($bdd, $id, $id);
                ?>
                <p>Votre compte vient d'être supprimé.</p>
                <?php
                session_destroy();
            } else {
                ?>
                <p>Impossible de trouver votre compte : vérifiez que vous avez bien copié le lien de l'email que vous avez reçu.</p>
                <?php
            }
            $reponse->closeCursor();
        } else {
            header("Location: " . URL . "page-introuvable.html");
            exit;
        }
    } else {
        header("Location: " . URL . "page-introuvable.html");
        exit;
    }
    ?>
</div>
<?php
include_once("google-analytics.php");
include_once("bas.php");
?>