<?php

if (!isset($bdd)) {
    require_once("bdd_config.php");
}

$champ = "";
if (isset($_GET['champ'])) {
    $champ = $_GET['champ'];
}

$valeur = "";
if (isset($_GET['valeur'])) {
    $valeur = $_GET['valeur'];
}

$id = "";
if (isset($_GET['id'])) {
    $id = $_GET['id'];
}

if (($champ == "pseudo") || ($champ == "email")) {
    $dispo = true;
    $requeteVerif = "SELECT * FROM membres";
    $reponseVerif = $bdd->query($requeteVerif);
    while (($donneesVerif = $reponseVerif->fetch()) && ($dispo)) {
        if ((($id != "") && (encodeUrl($donneesVerif['pseudo']) != $id)) || ($id == "")) {
            if (encodeUrl($donneesVerif[$champ]) == encodeUrl($valeur)) {
                $dispo = false;
            }
        }
    }
    if (!$dispo) {
        ?>
        <img src="img/refuse.png" alt="" />
        <?php

        if ($champ == "pseudo") {
            echo "Ce pseudo est déjà pris.";
        } elseif ($champ == "email") {
            echo "Cet email est déjà pris.";
        }
        ?>
        <script>
            $("#boutonSubmit").attr("disabled","true");
        </script>
        <?php

    }
}
?>