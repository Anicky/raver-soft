<?php

require_once("../bdd_config.php");
require_once("acces-admin.php");

$id = "";
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $requete = "SELECT publiee FROM news WHERE id = ?";
    $reponse = $bdd->prepare($requete);
    $reponse->bindValue(1, $id, PDO::PARAM_INT);
    $reponse->execute();
    $donnees = $reponse->fetch();
    if ($donnees != null) {
        $publiee = $donnees['publiee'];
        if ($publiee) {
            $requetePublication = "UPDATE news SET publiee = 0 WHERE id = ?";
            $reponsePublication = $bdd->prepare($requetePublication);
            $reponsePublication->bindValue(1, $id, PDO::PARAM_INT);
            $reponsePublication->execute();
            $reponsePublication->closeCursor();
        } else {
            $requetePublication = "UPDATE news SET publiee = 1 WHERE id = ?";
            $reponsePublication = $bdd->prepare($requetePublication);
            $reponsePublication->bindValue(1, $id, PDO::PARAM_INT);
            $reponsePublication->execute();
            $reponsePublication->closeCursor();
        }
    }
    $reponse->closeCursor();
}
header("Location: ./");
exit;
?>