<?php
require_once("../bdd_config.php");
require_once("acces-membre.php");
?>
<table class="tableau">
    <thead>
        <tr>
            <th>
                Nom
                <a href="javascript:void(0)" title="Trier par ordre croissant" onclick="trier('nom', 'desc')"><img src="img/fleche1_haut_over.png" alt="ASC" /></a>
                <a href="javascript:void(0)" title="Trier par ordre croissant" onclick="trier('nom', 'asc')"><img src="img/fleche1_bas_over.png" alt="DESC" /></a>
            </th>
            <th>
                Lien
                <a href="javascript:void(0)" title="Trier par ordre croissant" onclick="trier('url', 'desc')"><img src="img/fleche1_haut_over.png" alt="ASC" /></a>
                <a href="javascript:void(0)" title="Trier par ordre croissant" onclick="trier('url', 'asc')"><img src="img/fleche1_bas_over.png" alt="DESC" /></a>
            </th>
            <th>
                Type
                <a href="javascript:void(0)" title="Trier par ordre croissant" onclick="trier('type', 'desc')"><img src="img/fleche1_haut_over.png" alt="ASC" /></a>
                <a href="javascript:void(0)" title="Trier par ordre croissant" onclick="trier('type', 'asc')"><img src="img/fleche1_bas_over.png" alt="DESC" /></a>
            </th>
            <th></th>
            <?php
            if ($session_role == "Administrateur") {
                ?>
                <th></th>
                <?php
            }
            ?>
        </tr>
    </thead>
    <tbody>
        <?php
        $critere = "telechargements.nom";
        $ordre = "ASC";
        if (isset($_POST['critere'])) {
            $critere = securite_bdd($_POST['critere']);
            if ($critere == "type") {
                $critere = "types_telechargements.nom";
            } else if ($critere == "nom") {
                $critere = "telechargements.nom";
            }
        }
        if (isset($_POST['ordre'])) {
            if ($_POST['ordre'] == "asc") {
                $ordre = "ASC";
            } else if ($_POST['ordre'] == "desc") {
                $ordre = "DESC";
            }
        }
        $requete = "SELECT telechargements.id, telechargements.nom AS nomDownload, telechargements.url, extension, types_telechargements.nom AS nomType
            FROM telechargements, types_telechargements
            WHERE telechargements.id_type = types_telechargements.id
            ORDER BY " . $critere . " " . $ordre;
        $reponse = $bdd->query($requete);
        while ($donnees = $reponse->fetch()) {
            ?>
            <tr>
                <td><?php echo securite_sortie($donnees['nomDownload']); ?></td>
                <td><?php echo securite_sortie($donnees['url']) . "." . securite_sortie($donnees['extension']); ?></td>
                <td><?php echo securite_sortie($donnees['nomType']); ?></td>
                <td class="icone">
                    <a href="admin/telechargements-modifier-<?php echo $donnees['id']; ?>.html" title="Modifier des informations sur le téléchargement">
                        <img src="img/edit.png" alt="Modifier" />
                    </a>
                </td>
                <?php
                if ($session_role == "Administrateur") {
                    ?>
                    <td class="icone">
                        <a href="javascript:void(0)" onclick="supprimer('<?php echo $donnees['id']; ?>')" title="Supprimer le téléchargement">
                            <img src="img/delete.png" alt="Supprimer" />
                        </a>
                    </td>
                    <?php
                }
                ?>
            </tr>
            <?php
        }
        $reponse->closeCursor();
        ?>
    </tbody>
</table>
