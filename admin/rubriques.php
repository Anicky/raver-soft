<?php
$pageTitre = "Administration : Gestion des rubriques";
$pageRubriques = true;
$pageAdmin = true;
include_once("../haut.php");
require_once("acces-membre.php");
?>
<h1>Administration : Gestion des rubriques</h1>
<div class="contenuPage" id="infos_rubriques">
    <?php require_once("rubriques-infos.php"); ?>
</div>
<div class="contenuPage" id="liste_rubriques">
    <?php if ($session_role == "Administrateur") { ?>
        Vous pouvez modifier l'ordre des rubriques en faisant un cliquer-déposer.
        <?php
    }
    require_once("rubriques-liste.php");
    ?>
</div>
<div class="centre">
    <a class="bouton" href="admin/rubriques-ajouter.html" title="Ajouter une rubrique">
        <img src="img/add.png" alt="" />
        Ajouter une rubrique
    </a>
</div>
<?php
if ($session_role == "Administrateur") {
    ?>
    <div id="dialogbox">
        <?php require_once("../loading.php"); ?>
    </div>
    <div id="dialogbox2">
        <?php require_once("../loading.php"); ?>
    </div>
    <script>
        $("#listeRubriques").sortable({
            placeholder: 'highlight',
            update: function() {
                var ordre = $("#listeRubriques").sortable("serialize");
                $.post("admin/rubriques-ordre.html", ordre);
                $.get("menu.html", {}, function(html) {
                    $("nav").html(html);
                    ddsmoothmenu.init({
                        mainmenuid: "menu",
                        classname: "menu",
                        arrowswap: true
                    });
                });
            }
        });
        $("#listeRubriques").disableSelection();
        $(document).ready(function() {
            $("#dialogbox").dialog({
                autoOpen: false,
                modal: true,
                resizable: false,
                draggable: false,
                show: "fade",
                hide: "fade",
                title: "Suppression d'une rubrique",
                buttons: {
                    "Fermer": function() {
                        $(this).dialog("close");
                    }
                }
            });
            $("#dialogbox2").dialog({
                autoOpen: false,
                modal: true,
                resizable: false,
                draggable: false,
                show: "fade",
                hide: "fade",
                title: "Suppression d'une page",
                buttons: {
                    "Fermer": function() {
                        $(this).dialog("close");
                    }
                }
            });
        });
        function supprimer(id) {
            $("#dialogbox").dialog('option', 'buttons', {
                "Fermer": function() {
                    $(this).dialog("close");
                }
            });
            $.post("admin/rubriques-supprimer.html", {id: id}, function(html) {
                $("#dialogbox").html(html);
                $("#dialogbox").dialog('open');
            });
        }
        function supprimerPage(id) {
            $("#dialogbox2").dialog('option', 'buttons', {
                "Fermer": function() {
                    $(this).dialog("close");
                }
            });
            $.post("admin/pages-supprimer.html", {id: id}, function(html) {
                $("#dialogbox2").html(html);
                $("#dialogbox2").dialog('open');
            });
        }
    </script>
    <?php
}
include_once("../bas.php");
?>
