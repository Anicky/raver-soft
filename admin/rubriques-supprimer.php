<?php
require_once("../bdd_config.php");
require_once("acces-admin.php");

$id = "";
if (isset($_POST['id'])) {
    $id = $_POST['id'];
    $requete = "SELECT id FROM rubriques WHERE id = ?";
    $reponse = $bdd->prepare($requete);
    $reponse->bindValue(1, $id, PDO::PARAM_INT);
    $reponse->execute();
    $donnees = $reponse->fetch();
    if ($donnees != null) {
        ?>
        <script>
            $("#dialogbox").dialog('option', 'buttons', { 
                "Non" : function() {
                    $(this).dialog("close");
                },
                "Oui" : function() {
                    $.post("admin/rubriques-suppression.html", {id : '<?php echo $id; ?>'}, function(html) {
                        $("#dialogbox").dialog('option', 'buttons', {
                            "Fermer" : function() {
                                $(this).dialog("close");
                            }
                        });
                        $("#dialogbox").html(html);
                    });
                }
            });
        </script>
        <p>Etes-vous sûr de vouloir supprimer cette rubrique ?<br /><br />
            Attention ! Toutes les pages de la rubrique seront aussi supprimées.</p>
        <?php
    } else {
        ?>
        <p>La rubrique recherchée est introuvable.</p>
        <?php
    }
    $reponse->closeCursor();
} else {
    ?>
    <p>La rubrique recherchée est introuvable.</p>
    <?php
}
?>