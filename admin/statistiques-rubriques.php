<?php
require_once("../bdd_config.php");
require_once("acces-membre.php");

$typeGraphe = "column";
if (isset($_POST['type'])) {
    $typeGraphe = $_POST['type'];
}
$id = "";
if (isset($_POST['id'])) {
    $id = $_POST['id'];
}

$requete = "SELECT * FROM rubriques WHERE id = ?";
$reponse = $bdd->prepare($requete);
$reponse->bindValue(1, $id, PDO::PARAM_INT);
$reponse->execute();
$donnees = $reponse->fetch();
if ($donnees != null) {
    $j = 0;
    $id = $donnees['id'];
    $titre = securite_sortie($donnees['nom']);

    $pagePrincipaleExiste = false;
    $requetePagePrincipale = "SELECT * FROM pages_principales WHERE id_rubrique = ?";
    $reponsePagePrincipale = $bdd->prepare($requetePagePrincipale);
    $reponsePagePrincipale->bindValue(1, $id, PDO::PARAM_INT);
    $reponsePagePrincipale->execute();
    $donneesPagePrincipale = $reponsePagePrincipale->fetch();
    if ($donneesPagePrincipale != null) {
        $pagePrincipaleExiste = true;
    }
    $reponsePagePrincipale->closeCursor();

    $abscisses = "";
    $data = "";

    $requeteVues = "SELECT id, nom, vues FROM pages WHERE rubrique_id = ? ORDER BY ordre ASC";
    $reponseVues = $bdd->prepare($requeteVues);
    $reponseVues->bindValue(1, $id, PDO::PARAM_INT);
    $reponseVues->execute();
    $nombrePages = $reponseVues->rowCount();
    if (!$pagePrincipaleExiste) {
        $abscisses .= "\"" . $titre . "\"";
        $vue_nom = "vue";
        if (($donnees['vues'] == 0) || ($donnees['vues'] > 1)) {
            $vue_nom .= "s";
        }
        $data .= "{ name : \"" . $titre . " : <strong>" . $donnees['vues'] . " " . $vue_nom . "</strong>\", x : " . $j . ", y : " . $donnees['vues'] . "}";
        if ($nombrePages > 0) {
            $data .= ",\n";
            $abscisses .= ",\n";
            $j++;
        }
    }
    $vues = array();
    $titres = array();
    while ($donneesVues = $reponseVues->fetch()) {
        $vues[] = $donneesVues['vues'];
        $titres[] = securite_sortie($donneesVues['nom']);
    }
    for ($i = 0; $i < $nombrePages; $i++) {
        $abscisses .= "\"" . $titres[$i] . "\"";
        $vue_nom = "vue";
        if (($vues[$i] == 0) || ($vues[$i] > 1)) {
            $vue_nom .= "s";
        }
        $data .= "{ name : \"" . $titre . " : " . $titres[$i] . " : <strong>" . $vues[$i] . " " . $vue_nom . "</strong>\", x : " . $j . ", y : " . $vues[$i] . "}";
        if ($i < ($nombrePages - 1)) {
            $data .= ",\n";
            $abscisses .= ",\n";
        }
        $j++;
    }
    $reponseVues->closeCursor();
    ?>
    <script>
        $(document).ready(function() {
            new Highcharts.Chart({
                chart: {
                    renderTo: 'stats_rubrique_<?php echo $id; ?>',
                    type: '<?php echo $typeGraphe; ?>'
                },
                legend : {
                    enabled : false
                },
                title: {
                    text: 'Nombre de vues pour la rubrique <?php echo $titre; ?>'
                },
                xAxis: {
                    categories : [<?php echo $abscisses; ?>],
                    labels : {
                        y : +20
                    }
                },
                yAxis: {
                    endOnTick : true,
                    min : 0,
                    title: {
                        text: 'Nombre de vues'
                    },
                    plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                },
                tooltip: {
                    formatter: function() {
                        return this.point.name;
                    }
                },
                series: [{
                        data: [<?php echo $data; ?>]
                    }]
            });
        });
    </script>
    <?php
}
$reponse->closeCursor();
?>