<?php

require_once("../bdd_config.php");
require_once("acces-admin.php");

if (isset($_POST['style01'])) {
    // Variables
    $style01 = securite_bdd($_POST['style01']);
    $style01ie = securite_bdd($_POST['style01ie']);
    $menu = securite_bdd($_POST['cssmenu']);
    $jqueryui = securite_bdd($_POST['jqueryui']);
    $codemirror = securite_bdd($_POST['codemirror']);
    // Traitement
    if ($_SESSION['tokenValidation'] == $_POST['tokenValidation']) {
        if (($style01 != "") && ($jqueryui != "")) {
            // Style général
            $fichier = fopen("../css/style_01.css", "r+");
            ftruncate($fichier, 0);
            fwrite($fichier, $style01);
            fclose($fichier);
            // Style général (Internet Explorer)
            $fichier = fopen("../css/style_01_ie.css", "r+");
            ftruncate($fichier, 0);
            fwrite($fichier, $style01ie);
            fclose($fichier);
            // Menu
            $fichier = fopen("../css/menu.css", "r+");
            ftruncate($fichier, 0);
            fwrite($fichier, $menu);
            fclose($fichier);
            // jQuery UI
            $fichier = fopen("../css/jquery-ui.css", "r+");
            ftruncate($fichier, 0);
            fwrite($fichier, $jqueryui);
            fclose($fichier);
            // CodeMirror
            $fichier = fopen("../css/jquery-codemirror.css", "r+");
            ftruncate($fichier, 0);
            fwrite($fichier, $codemirror);
            fclose($fichier);
            ?>
            <p>Les styles CSS ont bien été sauvegardés.</p>
            <script>
                window.location.href = "admin/styles-css.html";
            </script>
            <?php

        } else {
            ?>
            <p>Vous n'avez pas rempli tous les champs.</p>
            <?php

        }
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>