<?php

require_once("../bdd_config.php");
require_once("acces-admin.php");
?>
<script>
    $("#dialogbox").dialog('option', 'buttons', {
        "Fermer": function() {
            $(this).dialog("close");
        }
    });
</script>
<?php

$id = "";
if (isset($_POST['id'])) {
    $id = $_POST['id'];
    $requete = "DELETE FROM rubriques WHERE id = ?";
    $reponse = $bdd->prepare($requete);
    $reponse->bindValue(1, $id, PDO::PARAM_INT);
    $reponse->execute();
    $reponse->closeCursor();
    ?>
    <script>
        $(document).ready(function() {
            $.get("admin/rubriques-liste.html", {}, function(html) {
                $("#liste_rubriques").html(html);
            });
            $.get("admin/rubriques-infos.html", {}, function(html) {
                $("#infos_rubriques").html(html);
            });
            $.get("menu.html", {}, function(html) {
                $("nav").html(html);
                ddsmoothmenu.init({
                    mainmenuid: "menu",
                    classname: "menu",
                    arrowswap: true
                });
            });
        });
    </script>
    <p>La rubrique a bien été supprimée.</p>
    <script>
        $("#dialogbox").dialog("close");
    </script>
    <?php

} else {
    ?>
    <p>Impossible de supprimer la rubrique.</p>
    <?php

}
?>