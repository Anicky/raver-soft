<?php
$pageTitre = "Administration : Gestion des paramètres";
$pageAdmin = true;
$pageParametres = true;
include_once("../haut.php");
require_once("acces-admin.php");
?>
<h1>Administration : Gestion des paramètres</h1>
<form method="post" action="admin/parametres-modification.html" class="centre" id="formParametres">
    <?php
    $nombre_news_page = 0;
    $nombre_mots_preview = 0;
    $image_tailleMax_ko = 0;
    $nombre_news_stats = 0;
    $news_afficher_avatar = 0;
    $inscription_ouverte = 0;
    $requete = "SELECT nom, valeur FROM parametres";
    $reponse = $bdd->query($requete);
    while ($donnees = $reponse->fetch()) {
        if ($donnees['nom'] == 'nombre_news_page') {
            $nombre_news_page = securite_sortie($donnees['valeur']);
        } else if ($donnees['nom'] == 'nombre_mots_preview') {
            $nombre_mots_preview = securite_sortie($donnees['valeur']);
        } else if ($donnees['nom'] == 'image_tailleMax_ko') {
            $image_tailleMax_ko = securite_sortie($donnees['valeur']);
        } else if ($donnees['nom'] == 'nombre_news_stats') {
            $nombre_news_stats = securite_sortie($donnees['valeur']);
        } else if ($donnees['nom'] == 'news_afficher_avatar') {
            $news_afficher_avatar = securite_sortie($donnees['valeur']);
        } else if ($donnees['nom'] == 'inscription_ouverte') {
            $inscription_ouverte = securite_sortie($donnees['valeur']);
        }
    }
    $reponse->closeCursor();
    ?>
    <div class="contenuPage">
        <table class="formulaire">
            <tr>
                <td class="label">
                    <label for="nombre_news_page">Nombre d'articles par page</label>
                </td>
                <td>
                    <input type="text" name="nombre_news_page" id="nombre_news_page" value="<?php echo $nombre_news_page; ?>" />
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="nombre_mots_preview">Nombre de mots pour un résumé d'article</label>
                </td>
                <td>
                    <input type="text" name="nombre_mots_preview" id="nombre_mots_preview" value="<?php echo $nombre_mots_preview; ?>" />
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="image_tailleMax_ko">Taille maximum (en Ko) pour l'envoi d'une image</label>
                </td>
                <td>
                    <input type="text" name="image_tailleMax_ko" id="image_tailleMax_ko" value="<?php echo $image_tailleMax_ko; ?>" />
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="nombre_news_stats">Nombre de news à afficher dans la partie Statistiques</label>
                </td>
                <td>
                    <input type="text" name="nombre_news_stats" id="nombre_news_stats" value="<?php echo $nombre_news_stats; ?>" />
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="news_afficher_avatar">Afficher l'avatar de l'auteur d'une news</label>
                </td>
                <td>
                    <div id="checkbox_news_avatar">
                        <input type="radio" name="news_afficher_avatar" id="news_avatar_non" value="0"<?php
                        if ($news_afficher_avatar == 0) {
                            echo "checked=\"checked\"";
                        }
                        ?> />
                        <label for="news_avatar_non">Non</label>
                        <input type="radio" name="news_afficher_avatar" id="news_avatar_oui" value="1"<?php
                        if ($news_afficher_avatar != 0) {
                            echo "checked=\"checked\"";
                        }
                        ?> />
                        <label for="news_avatar_oui">Oui</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="inscription_ouverte">Inscriptions ouvertes</label>
                </td>
                <td>
                    <div id="checkbox_inscription">
                        <input type="radio" name="inscription_ouverte" id="inscription_ouverte_non" value="0"<?php
                        if ($inscription_ouverte == 0) {
                            echo "checked=\"checked\"";
                        }
                        ?> />
                        <label for="inscription_ouverte_non">Non</label>
                        <input type="radio" name="inscription_ouverte" id="inscription_ouverte_oui" value="1"<?php
                        if ($inscription_ouverte != 0) {
                            echo "checked=\"checked\"";
                        }
                        ?> />
                        <label for="inscription_ouverte_oui">Oui</label>
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <input class="bouton" id="back" type="button" onclick="goToUrl('admin/parametres.html')" value="Annuler" />
    <input type="hidden" name="tokenValidation" value="<?php echo $_SESSION['tokenValidation']; ?>" />
    <input class="bouton" type="submit" value="Sauvegarder" />

</form>
<div id="dialogbox">
    <?php require_once("../loading.php"); ?>
</div>
<script>
        $(document).ready(function() {
            $("#checkbox_news_avatar").buttonset();
            $("#checkbox_inscription").buttonset();
            $("#dialogbox").dialog({
                autoOpen: false,
                modal: true,
                resizable: false,
                draggable: false,
                show: "fade",
                hide: "fade",
                title: "Gestion des paramètres",
                buttons: {
                    "Fermer": function() {
                        $(this).dialog("close");
                    }
                }
            });
        });
        $("#formParametres").validate({
            rules: {
                nombre_news_page: {
                    required: true
                },
                nombre_mots_preview: {
                    required: true
                },
                image_tailleMax_ko: {
                    required: true
                },
                nombre_news_stats: {
                    required: true
                }
            },
            messages: {
                nombre_news_page: {
                    required: "Vous devez remplir ce champ."
                },
                nombre_mots_preview: {
                    required: "Vous devez remplir ce champ."
                },
                image_tailleMax_ko: {
                    required: "Vous devez remplir ce champ."
                },
                nombre_news_stats: {
                    required: "Vous devez remplir ce champ."
                }
            }
        });
        $("#formParametres").ajaxForm({
            target: "#dialogbox",
            beforeSubmit: function() {
                $("#dialogbox").dialog('open');
            }
        });
</script>
<?php
include_once("../bas.php");
?>