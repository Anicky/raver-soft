<?php

require_once("../bdd_config.php");
require_once("acces-admin.php");

foreach ($_POST['page'] as $ordre => $id_page) {
    $position = $ordre + 1;
    $requete = "UPDATE pages SET ordre = ? WHERE id = ?";
    $reponse = $bdd->prepare($requete);
    $reponse->bindValue(1, $position, PDO::PARAM_INT);
    $reponse->bindValue(2, $id_page, PDO::PARAM_INT);
    $reponse->execute();
    $reponse->closeCursor();
}
?>