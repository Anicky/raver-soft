<?php
$pageTitre = "Administration : Statistiques";
$pageStatistiques = true;
$pageAdmin = true;
include_once("../haut.php");
require_once("acces-membre.php");
?>
<script src="js/jquery-highcharts.js" charset="utf-8"></script>
<h1>Administration : Statistiques</h1>

<div class="contenuPage">
    <div id="stats_articles">
        <?php
        require_once("statistiques-articles.php");
        ?>
    </div>
    <div class="stats_boutons">
        <a href="javascript:void(0)" class="bouton" onclick="stats_articles('line')">Lignes</a>
        <a href="javascript:void(0)" class="bouton" onclick="stats_articles('column')">Barres</a>
        <a href="javascript:void(0)" class="bouton" onclick="stats_articles('area')">Zones</a>
    </div>
</div>

<?php
$requete = "SELECT * FROM rubriques ORDER BY ordre ASC";
$reponse = $bdd->query($requete);
while ($donnees = $reponse->fetch()) {
    $j = 0;
    $id = $donnees['id'];
    $titre = securite_sortie($donnees['nom']);

    $pagePrincipaleExiste = false;
    $requetePagePrincipale = "SELECT * FROM pages_principales WHERE id_rubrique = ?";
    $reponsePagePrincipale = $bdd->prepare($requetePagePrincipale);
    $reponsePagePrincipale->bindValue(1, $id, PDO::PARAM_INT);
    $reponsePagePrincipale->execute();
    $donneesPagePrincipale = $reponsePagePrincipale->fetch();
    if ($donneesPagePrincipale != null) {
        $pagePrincipaleExiste = true;
    }
    $reponsePagePrincipale->closeCursor();

    $abscisses = "";
    $data = "";

    $requeteVues = "SELECT id, nom, vues FROM pages WHERE rubrique_id = ? ORDER BY ordre ASC";
    $reponseVues = $bdd->prepare($requeteVues);
    $reponseVues->bindValue(1, $id, PDO::PARAM_INT);
    $reponseVues->execute();
    $nombrePages = $reponseVues->rowCount();
    if (!$pagePrincipaleExiste) {
        $abscisses .= "\"" . $titre . "\"";
        $vue_nom = "vue";
        if (($donnees['vues'] == 0) || ($donnees['vues'] > 1)) {
            $vue_nom .= "s";
        }
        $data .= "{ name : \"" . $titre . " : <strong>" . $donnees['vues'] . " " . $vue_nom . "</strong>\", x : " . $j . ", y : " . $donnees['vues'] . "}";
        if ($nombrePages > 0) {
            $data .= ",\n";
            $abscisses .= ",\n";
            $j++;
        }
    }
    $vues = array();
    $titres = array();
    while ($donneesVues = $reponseVues->fetch()) {
        $vues[] = $donneesVues['vues'];
        $titres[] = securite_sortie($donneesVues['nom']);
    }
    for ($i = 0; $i < $nombrePages; $i++) {
        $abscisses .= "\"" . $titres[$i] . "\"";
        $vue_nom = "vue";
        if (($vues[$i] == 0) || ($vues[$i] > 1)) {
            $vue_nom .= "s";
        }
        $data .= "{ name : \"" . $titre . " : " . $titres[$i] . " : <strong>" . $vues[$i] . " " . $vue_nom . "</strong>\", x : " . $j . ", y : " . $vues[$i] . "}";
        if ($i < ($nombrePages - 1)) {
            $data .= ",\n";
            $abscisses .= ",\n";
        }
        $j++;
    }
    $reponseVues->closeCursor();
    ?>
    <div class="contenuPage">
        <div id="stats_rubrique_<?php echo $id; ?>">
            <?php
            require_once("statistiques-articles.php");
            ?>
        </div>
        <div class="stats_boutons">
            <a href="javascript:void(0)" class="bouton" onclick="stats_rubriques('<?php echo $id; ?>', 'column')">Barres</a>
            <a href="javascript:void(0)" class="bouton" onclick="stats_rubriques('<?php echo $id; ?>', 'pie')">Camembert</a>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            new Highcharts.Chart({
                chart: {
                    renderTo: 'stats_rubrique_<?php echo $id; ?>',
                    type: 'column'
                },
                legend : {
                    enabled : false
                },
                title: {
                    text: 'Nombre de vues pour la rubrique <?php echo $titre; ?>'
                },
                xAxis: {
                    categories : [<?php echo $abscisses; ?>],
                    labels : {
                        y : +20
                    }
                },
                yAxis: {
                    endOnTick : true,
                    min : 0,
                    title: {
                        text: 'Nombre de vues'
                    },
                    plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                },
                tooltip: {
                    formatter: function() {
                        return this.point.name;
                    }
                },
                series: [{
                        data: [<?php echo $data; ?>]
                    }]
            });
        });
    </script>
    <?php
}
$reponse->closeCursor();
?>

<div class="contenuPage">
    <div id="stats_telechargements">
        <?php
        require_once("statistiques-telechargements.php");
        ?>
    </div>
    <div class="stats_boutons">
        <a href="javascript:void(0)" class="bouton" onclick="stats_telechargements('column')">Barres</a>
        <a href="javascript:void(0)" class="bouton" onclick="stats_telechargements('pie')">Camembert</a>
    </div>
</div>

<div class="contenuPage">
    Nombre de rubriques : <strong><?php echo nombreElements($bdd, "rubriques"); ?></strong>
    <hr />
    Nombre d'articles : <strong><?php echo nombreElements($bdd, "news"); ?></strong><br />
    Nombre de commentaires : <strong><?php echo nombreElements($bdd, "commentaires"); ?></strong>
    <hr />
    Nombre de catégories : <strong><?php echo nombreElements($bdd, "tags"); ?></strong>
    <hr />
    Nombre de membres : <strong><?php echo nombreElements($bdd, "membres"); ?></strong>
    <ul>
        <li>Visiteurs : <strong><?php echo nombreElements($bdd, "membres, roles WHERE membres.id_role = roles.id AND roles.nom = 'Visiteur'"); ?></strong></li>
        <li>Membres Raver Soft : <strong><?php echo nombreElements($bdd, "membres, roles WHERE membres.id_role = roles.id AND roles.nom = 'Membre'"); ?></strong></li>
        <li>Administrateurs : <strong><?php echo nombreElements($bdd, "membres, roles WHERE membres.id_role = roles.id AND roles.nom = 'Administrateur'"); ?></strong></li>
    </ul>
    <hr />
    Nombre de types de téléchargements : <strong><?php echo nombreElements($bdd, "types_telechargements"); ?></strong><br />
    Nombre de téléchargements : <strong><?php echo nombreElements($bdd, "telechargements"); ?></strong>
</div>

<script>
    function stats_articles(type) {
        $.post("admin/statistiques-articles.html", {type : type}, function(html) {
            $("#stats_articles").html(html);
        });    
    }
    function stats_telechargements(type) {
        $.post("admin/statistiques-telechargements.html", {type : type}, function(html) {
            $("#stats_telechargements").html(html);
        });    
    }
    function stats_rubriques(id, type) {
        $.post("admin/statistiques-rubriques.html", {id : id, type : type}, function(html) {
            $("#stats_rubrique_" + id).html(html);
        });    
    }
</script>

<?php include_once("../bas.php"); ?>