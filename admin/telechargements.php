<?php
$pageTitre = "Administration : Gestion des téléchargements";
$pageTelechargements = true;
$pageAdmin = true;
include_once("../haut.php");
require_once("acces-membre.php");
?>
<h1>Administration : Gestion des téléchargements</h1>
<div class="contenuPage" id="infos_telechargements">
    <?php require_once("telechargements-infos.php"); ?>
</div>
<div class="contenuPage" id="liste_telechargements">
    <?php require_once("telechargements-liste.php"); ?>
</div>
<div class="centre">
    <a class="bouton" href="admin/telechargements-ajouter.html" title="Ajouter un téléchargement">
        <img src="img/add.png" alt="" />
        Ajouter un téléchargement
    </a>
</div>
<div id="dialogbox">
    <?php require_once("../loading.php"); ?>
</div>
<script>
    $(document).ready(function() {
        $("#dialogbox").dialog({
            autoOpen : false,
            modal : true,
            resizable : false,
            draggable : false,
            show : "fade",
            hide : "fade",
            title : "Suppression d'un téléchargement",
            buttons : {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            }
        });
    });
    function trier(critere, ordre) {
        $.post("admin/telechargements-liste.html", {critere : critere, ordre : ordre}, function(html) {
            $("#liste_telechargements").html(html);
        });   
    }
    function supprimer(id) {
        $("#dialogbox").dialog('option', 'buttons', { 
            "Fermer" : function() {
                $(this).dialog("close");
            }
        });
        $.post("admin/telechargements-supprimer.html", {id : id}, function(html) {
            $("#dialogbox").html(html);
            $("#dialogbox").dialog('open');
        });    
    }
</script>

<?php include_once("../bas.php"); ?>