<?php
require_once("../bdd_config.php");
require_once("acces-membre.php");
$pageRubriques = true;
$pageAdmin = true;

$id = "";
if (isset($_GET["id"])) {
    $id = $_GET["id"];
}
$idRubrique = "";
if (isset($_GET["idRubrique"])) {
    $idRubrique = $_GET["idRubrique"];
}

if (($id != "") || ($idRubrique != "")) {

    $pageTitre = "Administration : Ajouter une page";
    $nom = "";
    $texte = "";

    if ($id != "") {
        $requete = "SELECT nom, texte, rubrique_id FROM pages WHERE id = ?";
        $reponse = $bdd->prepare($requete);
        $reponse->bindValue(1, $id, PDO::PARAM_INT);
        $reponse->execute();
        $donnees = $reponse->fetch();
        if ($donnees != null) {
            $nom = securite_sortie($donnees['nom']);
            $pageTitre = "Modifier une page : " . $nom;
            $texte = $donnees['texte'];
            $idRubrique = $donnees['rubrique_id'];
        } else {
            header("Location: " . URL . "page-introuvable.html");
            exit;
        }
        $reponse->closeCursor();
    }
    include_once("../haut.php");
    ?>

    <script src="js/ckeditor/ckeditor.js" charset="utf-8"></script>
    <script src="js/ckeditor/adapters/jquery.js" charset="utf-8"></script>
    <script src="js/ckeditor/plugins/codemirror/js/codemirror.js" charset="utf-8"></script>

    <h1><?php echo $pageTitre; ?></h1>
    <form method="post" action="admin/pages-ajout.html" class="centre" id="ajouterPage" onSubmit="MirrorUpdate();">
        <div class="contenuPage">
            <table class="formulaire">

                <tr>
                    <td class="label">
                        <label for="nom">Nom</label>
                    </td>
                    <td>
                        <input type="text" name="nom" id="nom" value="<?php echo $nom; ?>" size="80" maxlength="100" />
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        <label for="rubrique">Rubrique</label>
                    </td>
                    <td>
                        <select name="rubrique" id="rubrique">
                            <?php
                            $requete = "SELECT id, nom FROM rubriques ORDER BY ordre ASC";
                            $reponse = $bdd->query($requete);
                            while ($donnees = $reponse->fetch()) {
                                ?>
                                <option value="<?php echo $donnees['id']; ?>"<?php
                        if ($idRubrique == $donnees['id']) {
                            echo " selected=\"selected\"";
                        }
                                ?>><?php echo $donnees['nom']; ?></option>
                                        <?php
                                    }
                                    $reponse->closeCursor();
                                    ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        <label for="texte">Contenu</label>
                    </td>
                    <td>
                        <textarea name="texte" id="texte"><?php echo $texte; ?></textarea>
                    </td>
                </tr>
            </table>
        </div>
        <input type="hidden" name="id" value="<?php echo $id; ?>" />
        <input id="back" class="bouton" type="button" onclick="goToUrl('admin/rubriques.html')" value="Annuler" />
        <input type="hidden" name="tokenValidation" value="<?php echo $_SESSION['tokenValidation']; ?>" />
        <input class="bouton" type="submit" value="Sauvegarder" id="boutonSubmit" />
    </form>
    <div id="dialogbox">
        <?php require_once("../loading.php"); ?>
    </div>
    <script>
        $(document).ready(function() {
            $("#texte").ckeditor();
            $("#dialogbox").dialog({
                autoOpen : false,
                modal : true,
                resizable : false,
                draggable : false,
                show : "fade",
                hide : "fade",
                title : "Ajout/Modification d'une page",
                buttons : {
                    "Fermer" : function() {
                        $(this).dialog("close");
                    }
                }
            });
        });
        $("#ajouterPage").validate({
            rules: {
                nom : {
                    required : true,
                    maxlength : 100
                },
                texte : {
                    required : true
                }
            },
            messages: { 
                nom : {
                    required : "Vous devez rentrer un titre.",
                    maxlength : "Le titre ne peut pas dépasser 255 caractères."
                },
                texte : {
                    required : "Vous devez rentrer du texte."
                }
            } 
        });
        $("#ajouterPage").ajaxForm({
            target: "#dialogbox",
            beforeSubmit : function() {
                $("#dialogbox").dialog('open');
            }
        });
        function MirrorUpdate() {
            for (i in CKEDITOR.instances) {
                CKEDITOR.instances[i].execCommand('mirrorSnapshot');
            }  
        }
    </script>
    <?php
} else {
    header("Location: " . URL . "page-introuvable.html");
    exit;
}
include_once("../bas.php");
?>