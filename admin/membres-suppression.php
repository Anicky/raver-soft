<?php

require_once("../bdd_config.php");
require_once("acces-admin.php");
?>
<script>
    $("#dialogbox").dialog('option', 'buttons', { 
        "Fermer" : function() {
            $(this).dialog("close");
        }
    });
</script>
<?php

$id = "";
if (isset($_POST['id'])) {
    $id = $_POST['id'];
    if ($id != $session_id) {
        suppressionMembre($bdd, $session_id, $id);
        ?>
        <script>
            $(document).ready(function() {
                $.get("admin/membres-liste.html", {}, function(html) {
                    $("#liste_membres").html(html);
                });
                $.get("admin/membres-infos.html", {}, function(html) {
                    $("#infos_membres").html(html);
                });
            });
        </script>
        <p>Le membre a bien été supprimé.</p>
        <script>
            $("#dialogbox").dialog("close");
        </script>
        <?php

    } else {
        ?>
        <p>Impossible de supprimer le membre.</p>
        <?php

    }
} else {
    ?>
    <p>Impossible de supprimer le membre.</p>
    <?php

}
?>