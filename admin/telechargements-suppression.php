<?php

require_once("../bdd_config.php");
require_once("acces-admin.php");
?>
<script>
    $("#dialogbox").dialog('option', 'buttons', { 
        "Fermer" : function() {
            $(this).dialog("close");
        }
    });
</script>
<?php

$id = "";
if (isset($_POST['id'])) {
    $id = $_POST['id'];
    $requete = "DELETE FROM telechargements WHERE id = ?";
    $reponse = $bdd->prepare($requete);
    $reponse->bindValue(1, $id, PDO::PARAM_INT);
    $reponse->execute();
    $reponse->closeCursor();
    ?>
    <script>
        $(document).ready(function() {
            $.get("admin/telechargements-liste.html", {}, function(html) {
                $("#liste_telechargements").html(html);
            });
            $.get("admin/telechargements-infos.html", {}, function(html) {
                $("#infos_telechargements").html(html);
            });
        });
    </script>
    <p>Le téléchargement a bien été supprimé.</p>
    <script>
        $("#dialogbox").dialog("close");
    </script>
    <?php

} else {
    ?>
    <p>Impossible de supprimer le téléchargement.</p>
    <?php

}
?>