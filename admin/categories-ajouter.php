<?php
require_once("../bdd_config.php");
require_once("acces-membre.php");
$pageCategories = true;
$pageAdmin = true;

$id = "";
if (isset($_GET["id"])) {
    $id = $_GET["id"];
}

$pageTitre = "Administration : Ajouter une catégorie";
$nom = "";

if ($id != "") {
    $requete = "SELECT nom FROM tags WHERE id = ?";
    $reponse = $bdd->prepare($requete);
    $reponse->bindValue(1, $id, PDO::PARAM_INT);
    $reponse->execute();
    $donnees = $reponse->fetch();
    if ($donnees != null) {
        $nom = securite_sortie($donnees['nom']);
        $pageTitre = "Modifier une catégorie : " . $nom;
    } else {
        header("Location: " . URL . "page-introuvable.html");
        exit;
    }
    $reponse->closeCursor();
}
include_once("../haut.php");
?>
<h1><?php echo $pageTitre; ?></h1>
<form method="post" action="admin/categories-ajout.html" class="centre" id="ajouterCategorie">
    <div class="contenuPage">
        <table class="formulaire">
            <tr>
                <td class="label">
                    <label for="nom">Nom</label>
                </td>
                <td>
                    <input type="text" name="nom" id="nom" value="<?php echo $nom; ?>" size="40" maxlength="100" />
                </td>
            </tr>
        </table>
    </div>
    <?php if ($id != "") {
        ?>
        <input type="hidden" name="id" value="<?php echo $id; ?>" />
    <?php }
    ?>
    <input id="back" class="bouton" type="button" onclick="goToUrl('admin/categories.html')" value="Annuler" />
    <input type="hidden" name="tokenValidation" value="<?php echo $_SESSION['tokenValidation']; ?>" />
    <input class="bouton" type="submit" value="Sauvegarder" id="boutonSubmit" />
</form>
<div id="dialogbox">
    <?php require_once("../loading.php"); ?>
</div>
<script>
    $(document).ready(function() {
        $("#dialogbox").dialog({
            autoOpen : false,
            modal : true,
            resizable : false,
            draggable : false,
            show : "fade",
            hide : "fade",
            title : "Ajout/Modification d'une catégorie",
            buttons : {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            }
        });
    });
    $("#ajouterCategorie").validate({
        rules: {
            nom : {
                required : true,
                maxlength : 100
            }
        },
        messages: { 
            nom : {
                required : "Vous devez rentrer un nom.",
                maxlength : "Le nom ne peut pas dépasser 100 caractères."
            }
        } 
    });
    $("#ajouterCategorie").ajaxForm({
        target: "#dialogbox",
        beforeSubmit : function() {
            $("#dialogbox").dialog('open');
        }
    });
</script>
<?php
include_once("../bas.php");
?>