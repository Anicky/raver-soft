<?php
require_once("../bdd_config.php");
require_once("acces-membre.php");

$typeGraphe = "column";
if (isset($_POST['type'])) {
    $typeGraphe = $_POST['type'];
}

$requete = "SELECT id, url, nom, nombre FROM telechargements ORDER BY nom ASC";
$reponse = $bdd->query($requete);
$nombreDownloads = $reponse->rowCount();
$data = "";
$abscisses = "";
$vues = array();
$titres = array();
while ($donnees = $reponse->fetch()) {
    $vues[] = $donnees['nombre'];
    $titres[] = securite_sortie($donnees['nom']);
}

$j = 0;
for ($i = 0; $i < $nombreDownloads; $i++) {
    $abscisses .= "\"" . $titres[$i] . "\"";
    $vue_nom = "téléchargement";
    if (($vues[$i] == 0) || ($vues[$i] > 1)) {
        $vue_nom .= "s";
    }
    $data .= "{ name : \"" . $titres[$i] . " : <strong>" . $vues[$i] . " " . $vue_nom . "</strong>\", x : " . $j . ", y : " . $vues[$i] . "}";
    if ($i < ($nombreDownloads - 1)) {
        $data .= ",\n";
        $abscisses .= ",\n";
    }
    $j++;
}
$reponse->closeCursor();
?>
<script>
    $(document).ready(function() {
        new Highcharts.Chart({
            chart: {
                renderTo: 'stats_telechargements',
                type: '<?php echo $typeGraphe; ?>'
            },
            legend : {
                enabled : false
            },
            title: {
                text: 'Téléchargements'
            },
            xAxis: {
                categories : [<?php echo $abscisses; ?>],
                labels : {
                    y : +20
                }
            },
            yAxis: {
                endOnTick : true,
                min : 0,
                title: {
                    text: 'Nombre de téléchargements'
                },
                plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
            },
            tooltip: {
                formatter: function() {
                    return this.point.name;
                }
            },
            series: [{
                    data: [<?php echo $data; ?>]
                }]
        });
    });
</script>