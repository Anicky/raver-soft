<?php

require_once("../bdd_config.php");
require_once("acces-membre.php");

if (isset($_POST['texte'])) {
    // Variables
    $nom = securite_bdd($_POST['nom']);
    $texte = securite_bdd($_POST['texte']);
    $rubrique_pagePrincipale = $_POST['pagePrincipale'];
    $id = "";
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    // Traitement
    if ($_SESSION['tokenValidation'] == $_POST['tokenValidation']) {
        if ($nom != "" && $texte != "") {
            if ($id == "") {
                // Ajout
                $requete = "SELECT MAX(ordre) AS ordreMax FROM rubriques";
                $reponse = $bdd->query($requete);
                $donnees = $reponse->fetch();
                $ordre = $donnees['ordreMax'] + 1;
                $reponse->closeCursor();

                $requete = "INSERT INTO rubriques(ordre, nom, url, texte) VALUES (?, ?, ?, ?)";
                $reponse = $bdd->prepare($requete);
                $reponse->bindValue(1, $ordre, PDO::PARAM_INT);
                $reponse->bindValue(2, $nom, PDO::PARAM_STR);
                $reponse->bindValue(3, encodeUrl($nom), PDO::PARAM_STR);
                $reponse->bindValue(4, $texte, PDO::PARAM_STR);
                $reponse->execute();
                $reponse->closeCursor();
                ?>
                <p>La rubrique a bien été ajoutée.</p>
                <?php

            } else {
                // Modification
                $requete = "UPDATE rubriques SET nom = ?, url = ?, texte = ? WHERE id = ?";
                $reponse = $bdd->prepare($requete);
                $reponse->bindValue(1, $nom, PDO::PARAM_STR);
                $reponse->bindValue(2, encodeUrl($nom), PDO::PARAM_STR);
                $reponse->bindValue(3, $texte, PDO::PARAM_STR);
                $reponse->bindValue(4, $id, PDO::PARAM_INT);
                $reponse->execute();
                $reponse->closeCursor();
                if ($rubrique_pagePrincipale != "") {
                    $pagePrincipale = getPagePrincipale($bdd, $id);
                    if ($pagePrincipale != "") {
                        // modif
                        $requete = "UPDATE pages_principales SET id_page = ? WHERE id_rubrique = ?";
                        $reponse = $bdd->prepare($requete);
                        $reponse->bindValue(1, $rubrique_pagePrincipale, PDO::PARAM_INT);
                        $reponse->bindValue(2, $id, PDO::PARAM_INT);
                        $reponse->execute();
                        $reponse->closeCursor();
                    } else {
                        // ajout
                        $requete = "INSERT INTO pages_principales (id_rubrique, id_page) VALUES (?, ?)";
                        $reponse = $bdd->prepare($requete);
                        $reponse->bindValue(1, $id, PDO::PARAM_INT);
                        $reponse->bindValue(2, $rubrique_pagePrincipale, PDO::PARAM_INT);
                        $reponse->execute();
                        $reponse->closeCursor();
                    }
                } else {
                    $pagePrincipale = getPagePrincipale($bdd, $id);
                    if ($pagePrincipale != "") {
                        // modif
                        $requete = "DELETE FROM pages_principales WHERE id_rubrique = ?";
                        $reponse = $bdd->prepare($requete);
                        $reponse->bindValue(1, $id, PDO::PARAM_INT);
                        $reponse->execute();
                        $reponse->closeCursor();
                    }
                }
                ?>
                <p>La rubrique a bien été modifiée.</p>
                <?php

            }
            ?>
            <script>
                window.location.href = "admin/rubriques.html";
            </script>
            <?php

        } else {
            ?>
            <p>Vous n'avez pas rempli tous les champs.</p>
            <?php

        }
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>