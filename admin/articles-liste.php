<?php
require_once("../bdd_config.php");
require_once("acces-membre.php");

$critere = "date";
$ordre = "DESC";
if (isset($_POST['critere'])) {
    $critere = securite_bdd($_POST['critere']);
}
if (isset($_POST['ordre'])) {
    if ($_POST['ordre'] == "asc") {
        $ordre = "ASC";
    } else if ($_POST['ordre'] == "desc") {
        $ordre = "DESC";
    }
}
$requete = "SELECT news.id AS idNews,
                    titre,
        DATE_FORMAT(date, '%d/%m/%Y à %Hh%i') AS dateFormatee,
        commentaires,
        publiee,
        id_auteur,
        auteur
        FROM news
        ORDER BY " . $critere . " " . $ordre;
$reponse = $bdd->query($requete);
$nombreNews = $reponse->rowCount();
if ($nombreNews > 0) {
    ?>
    <table class="tableau">
        <thead>
            <tr>
                <th></th>
                <th>
                    Date
                    <a href="javascript:void(0)" title="Trier par ordre croissant" onclick="trier('date', 'desc')"><img src="img/fleche1_haut_over.png" alt="ASC" /></a>
                    <a href="javascript:void(0)" title="Trier par ordre croissant" onclick="trier('date', 'asc')"><img src="img/fleche1_bas_over.png" alt="DESC" /></a>
                </th>
                <th>
                    Titre
                    <a href="javascript:void(0)" title="Trier par ordre croissant" onclick="trier('titre', 'desc')"><img src="img/fleche1_haut_over.png" alt="ASC" /></a>
                    <a href="javascript:void(0)" title="Trier par ordre croissant" onclick="trier('titre', 'asc')"><img src="img/fleche1_bas_over.png" alt="DESC" /></a>
                </th>
                <th>
                    Auteur
                    <a href="javascript:void(0)" title="Trier par ordre croissant" onclick="trier('auteur', 'desc')"><img src="img/fleche1_haut_over.png" alt="ASC" /></a>
                    <a href="javascript:void(0)" title="Trier par ordre croissant" onclick="trier('auteur', 'asc')"><img src="img/fleche1_bas_over.png" alt="DESC" /></a>
                </th>
                <th>
                    Commentaires
                </th>
                <th>
                    Catégories
                </th>
                <th></th>
                <?php
                if ($session_role == "Administrateur") {
                    ?>
                    <th></th>
                <?php } ?>
            </tr>
        </thead>
        <tbody>
            <?php
            while ($donnees = $reponse->fetch()) {
                $idNews = $donnees['idNews'];

                $idMembre = $donnees['id_auteur'];
                $pseudo = securite_sortie($donnees['auteur']);
                if ($pseudo == null) {
                    $requeteAuteur = "SELECT pseudo FROM membres WHERE id = ?";
                    $reponseAuteur = $bdd->prepare($requeteAuteur);
                    $reponseAuteur->bindValue(1, $idMembre, PDO::PARAM_INT);
                    $reponseAuteur->execute();
                    $donneesAuteur = $reponseAuteur->fetch();
                    if ($donneesAuteur != null) {
                        $pseudo = securite_sortie($donneesAuteur['pseudo']);
                    }
                    $reponseAuteur->closeCursor();
                }
                ?>
                <tr>
                    <td class="icone">
                        <?php
                        if ($session_role == "Administrateur") {
                            ?>
                            <a href="admin/articles-publier-<?php echo $donnees['idNews']; ?>.html" title="Publier/Enlever l'article">
                                <?php
                            }
                            if ($donnees['publiee']) {
                                ?>
                                <img src="img/flag_green.png" alt="Enlever" />
                                <?php
                            } else {
                                ?>
                                <img src="img/flag_red.png" alt="Publier" />
                                <?php
                            }
                            if ($session_role == "Administrateur") {
                                ?>
                            </a>
                        <?php } ?>
                    </td>
                    <td class="centre"><?php echo $donnees['dateFormatee']; ?></td>
                    <td><a href="news/<?php echo $idNews . "-" . encodeUrl($donnees['titre']); ?>.html" title="Lire la news et les commentaires"><?php echo securite_sortie($donnees['titre']); ?></a></td>
                    <td class="centre">
                        <?php
                        if ($idMembre != null) {
                            if (($session_role == "Administrateur") && ($idMembre != $session_id)) {
                                ?>
                                <a href="admin/membres-modifier-<?php echo $idMembre; ?>.html" title="Modifier des informations sur le membre">
                                    <?php
                                }
                                ?>
                                <strong><?php echo securite_sortie($pseudo); ?></strong>
                                <?php
                                if ($session_role == "Administrateur") {
                                    ?>
                                </a>
                                <?php
                            }
                        } else {
                            echo securite_sortie($pseudo);
                        }
                        ?></td>
                    <td class="centre"><?php
                $nombreCommentaires = 0;
                if ($donnees['commentaires']) {
                    $nombreCommentaires = nombreElements($bdd, "commentaires, news WHERE news.id = commentaires.id_news AND news.id = " . $idNews);
                            ?>
                            <img src="img/accept.png" alt="" /> Activés
                            <?php
                        } else {
                            ?>
                            <img src="img/refuse.png" alt="" /> Désactivés
                        <?php }
                        ?>
                        <strong>(<?php echo $nombreCommentaires; ?>)</strong></td>
                    <td>
                        <?php
                        $requeteCategories = "SELECT nom
                            FROM tags, news_tags
                            WHERE news_tags.id_tag = tags.id
                            AND news_tags.id_news = ?
                            ORDER BY nom ASC";
                        $reponseCategories = $bdd->prepare($requeteCategories);
                        $reponseCategories->bindValue(1, $idNews, PDO::PARAM_INT);
                        $reponseCategories->execute();
                        if ($reponseCategories->rowCount() > 0) {
                            ?>
                            <ul>
                                <?php
                                while ($donneesCategories = $reponseCategories->fetch()) {
                                    ?>
                                    <li><?php echo $donneesCategories['nom']; ?></li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                        <?php
                        $reponseCategories->closeCursor();
                        ?>

                    </td>
                    <td class="icone">
                        <a href="admin/articles-modifier-<?php echo $donnees['idNews']; ?>.html" title="Modifier l'article">
                            <img src="img/edit.png" alt="Modifier" />
                        </a>
                    </td>
                    <?php
                    if ($session_role == "Administrateur") {
                        ?>
                        <td class="icone">
                            <a href="javascript:void(0)" onclick="supprimer('<?php echo $donnees['idNews']; ?>')" title="Supprimer l'article">
                                <img src="img/delete.png" alt="Supprimer" />
                            </a>
                        </td>
                    <?php }
                    ?>
                </tr>
                <?php
            }
            $reponse->closeCursor();
            ?>
        </tbody>
    </table>
    <?php
} else {
    echo "Pas d'articles pour le moment.";
}
?>