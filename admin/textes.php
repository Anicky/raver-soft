<?php
$pageTitre = "Administration : Gestion des textes";
$pageAdmin = true;
$pageTextes = true;
include_once("../haut.php");
require_once("acces-admin.php");

$footer = "";
$description = "";
$keywords = "";

$requete = "SELECT nom, valeur FROM textes";
$reponse = $bdd->query($requete);
while ($donnees = $reponse->fetch()) {
    if (securite_sortie($donnees['nom']) == 'footer') {
        $footer = securite_sortie($donnees['valeur']);
    } else if (securite_sortie($donnees['nom']) == 'description') {
        $description = securite_sortie($donnees['valeur']);
    } else if (securite_sortie($donnees['nom']) == 'keywords') {
        $keywords = securite_sortie($donnees['valeur']);
    }
}
$reponse->closeCursor();
?>

<script src="js/ckeditor/ckeditor.js" charset="utf-8"></script>
<script src="js/ckeditor/adapters/jquery.js" charset="utf-8"></script>
<script src="js/ckeditor/plugins/codemirror/js/codemirror.js" charset="utf-8"></script>

<h1>Administration : Gestion des textes</h1>
<form method="post" action="admin/textes-modification.html" class="centre" id="formTextes" onSubmit="MirrorUpdate();">
    <div class="contenuPage">
        <h2>Bas de page</h2>
        <textarea name="footer" id="footer"><?php echo $footer; ?></textarea>
    </div>
    <div class="contenuPage">
        <h2><label for="description">Description</label></h2>
        <textarea name="description" id="description" cols="120" rows="3"><?php echo $description; ?></textarea>
    </div>
    <div class="contenuPage">
        <h2><label for="keywords">Mots clés</label></h2>
        <textarea name="keywords" id="keywords" cols="120" rows="3"><?php echo $keywords; ?></textarea>
    </div>
    <input class="bouton" id="back" type="button" onclick="goToUrl('admin/textes.html')" value="Annuler" />
    <input type="hidden" name="tokenValidation" value="<?php echo $_SESSION['tokenValidation']; ?>" />
    <input class="bouton" type="submit" value="Sauvegarder" />
</form>
<div id="dialogbox">
    <?php require_once("../loading.php"); ?>
</div>
<script>
    $(document).ready(function() {
        $("#footer").ckeditor({
            height : '100px'
        });
        $("#dialogbox").dialog({
            autoOpen : false,
            modal : true,
            resizable : false,
            draggable : false,
            show : "fade",
            hide : "fade",
            title : "Gestion des textes",
            buttons : {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            }
        });
    });
    $("#formTextes").ajaxForm({
        target: "#dialogbox",
        beforeSubmit : function() {
            $("#dialogbox").dialog('open');
        }
    });
    function MirrorUpdate() {
        for (i in CKEDITOR.instances) {
            CKEDITOR.instances[i].execCommand('mirrorSnapshot');
        }  
    }
</script>
<?php
include_once("../bas.php");
?>