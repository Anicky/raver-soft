<?php
$pageTitre = "Administration : Gestion des articles";
$pageArticles = true;
$pageAdmin = true;
include_once("../haut.php");
require_once("acces-membre.php");
?>

<h1>Administration : Gestion des articles</h1>
<div class="contenuPage" id="infos_articles">
    <?php require_once("articles-infos.php"); ?>
</div>
<div class="contenuPage" id="liste_articles">
    <?php require_once("articles-liste.php"); ?>
</div>
<div class="centre">
    <?php
    if ($session_role == "Administrateur") {
        ?>
        <input class="bouton" id="updateRSS" type="button" onclick="updateRSS()" value="Mettre à jour le fichier RSS" />
        <?php
    }
    ?>
    <a class="bouton" href="admin/articles-ajouter.html" title="Ajouter un article">
        <img src="img/add.png" alt="" />
        Ajouter un article
    </a>
</div>
<div id="dialogbox">
    <?php require_once("../loading.php"); ?>
</div>
<div id="dialogbox2">
    <?php require_once("../loading.php"); ?>
</div>
<script>
    $(document).ready(function() {
        $("#dialogbox").dialog({
            autoOpen : false,
            modal : true,
            resizable : false,
            draggable : false,
            show : "fade",
            hide : "fade",
            title : "Suppression d'un article",
            buttons : {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            }
        });
        $("#dialogbox2").dialog({
            autoOpen : false,
            modal : true,
            resizable : false,
            draggable : false,
            show : "fade",
            hide : "fade",
            title : "Mise à jour du fichier RSS",
            buttons : {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            }
        });
    });
    function trier(critere, ordre) {
        $.post("admin/articles-liste.html", {critere : critere, ordre : ordre}, function(html) {
            $("#liste_articles").html(html);
        });   
    }
    function supprimer(id) {
        $("#dialogbox").dialog('option', 'buttons', { 
            "Fermer" : function() {
                $(this).dialog("close");
            }
        });
        $.post("admin/articles-supprimer.html", {id : id}, function(html) {
            $("#dialogbox").html(html);
            $("#dialogbox").dialog('open');
        });    
    }
<?php
if ($session_role == "Administrateur") {
    ?>
            function updateRSS() {
                $.post("admin/rss-mise-a-jour.html", {}, function(html) {
                    $("#dialogbox2").html(html);
                    $("#dialogbox2").dialog('open');
                });
            }
    <?php
}
?>
</script>
<?php include_once("../bas.php"); ?>