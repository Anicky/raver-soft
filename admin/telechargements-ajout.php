<?php

require_once("../bdd_config.php");
require_once("acces-membre.php");

if (isset($_POST['nom'])) {
    // Variables
    $nom = securite_bdd($_POST['nom']);
    $id = "";
    $url = securite_bdd($_POST['url']);
    $type = $_POST['type'];
    $extension = "";
    $ok = false;
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    // Traitement
    if ($_SESSION['tokenValidation'] == $_POST['tokenValidation']) {
        if (($nom != "") && ($url != "") && ($type != "")) {
            // On cherche si le type existe, et si oui, on récupère son id.
            $requete = "SELECT id, nom FROM types_telechargements WHERE id = ?";
            $reponse = $bdd->prepare($requete);
            $reponse->bindValue(1, $type, PDO::PARAM_INT);
            $reponse->execute();
            $trouve = false;
            $donnees = $reponse->fetch();
            if ($donnees != null) {
                $trouve = true;
            }
            $reponse->closeCursor();
            if ($trouve) {
                $url_tableau = explode(".", $url);
                $url_base = $url_tableau[0];
                if (count($url_tableau) > 1) {
                    $extension = $url_tableau[1];
                }
                if ($id == "") {
                    // Ajout
                    $requete = "SELECT nom, url, extension FROM telechargements";
                    $reponse = $bdd->query($requete);
                    $trouve = false;
                    while (($donnees = $reponse->fetch()) && (!$trouve)) {
                        if ((encodeUrl($nom) == encodeUrl($donnees['nom'])) || (encodeUrl($url) == (encodeUrl($donnees['url']) . "." . encodeUrl($donnees['extension'])))) {
                            $trouve = true;
                        }
                    }
                    $reponse->closeCursor();
                    if (!$trouve) {
                        $requete = "INSERT INTO telechargements(url, extension, nom, id_type) VALUES (?, ?, ?, ?)";
                        $reponse = $bdd->prepare($requete);
                        $reponse->bindValue(1, $url_base, PDO::PARAM_STR);
                        $reponse->bindValue(2, $extension, PDO::PARAM_STR);
                        $reponse->bindValue(3, $nom, PDO::PARAM_STR);
                        $reponse->bindValue(4, $type, PDO::PARAM_INT);
                        $reponse->execute();
                        $reponse->closeCursor();
                        $ok = true;
                    }
                } else {
                    // Modification
                    // On regarde si il n'existe pas déjà le même nom et/ou le même lien
                    $requete = "SELECT nom, url, extension FROM telechargements WHERE id != ?";
                    $reponse = $bdd->prepare($requete);
                    $reponse->bindValue(1, $id, PDO::PARAM_INT);
                    $reponse->execute();
                    $trouve = false;
                    while (($donnees = $reponse->fetch()) && (!$trouve)) {
                        if ((encodeUrl($nom) == encodeUrl($donnees['nom'])) || (encodeUrl($url) == (encodeUrl($donnees['url']) . "." . encodeUrl($donnees['extension'])))) {
                            $trouve = true;
                        }
                    }
                    $reponse->closeCursor();
                    if (!$trouve) {

                        $requete = "UPDATE telechargements SET url = ?, extension = ?, nom = ?, id_type = ? WHERE id = ?";
                        $reponse = $bdd->prepare($requete);
                        $reponse->bindValue(1, $url_base, PDO::PARAM_STR);
                        $reponse->bindValue(2, $extension, PDO::PARAM_STR);
                        $reponse->bindValue(3, $nom, PDO::PARAM_STR);
                        $reponse->bindValue(4, $type, PDO::PARAM_INT);
                        $reponse->bindValue(5, $id, PDO::PARAM_INT);
                        $reponse->execute();
                        $reponse->closeCursor();
                        $ok = true;
                    }
                }
                if ($ok) {
                    if ($id == "") {
                        ?>
                        <p>Le téléchargement a bien été ajouté.</p>
                        <?php

                    } else {
                        ?>
                        <p>Le téléchargement a bien été modifié.</p>
                        <?php

                    }
                    ?>
                    <script>
                        window.location.href = "admin/telechargements.html";
                    </script>
                    <?php

                } else {
                    ?>
                    <p>Il existe déjà un téléchargement avec ce nom et/ou ce lien.</p>
                    <?php

                }
            } else {
                ?>
                <script>
                    window.location.href = "./";
                </script>
                <?php

            }
        } else {
            ?>
            <p>Vous n'avez pas rempli tous les champs.</p>
            <?php

        }
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>