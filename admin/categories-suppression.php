<?php

require_once("../bdd_config.php");
require_once("acces-admin.php");
?>
<script>
    $("#dialogbox").dialog('option', 'buttons', { 
        "Fermer" : function() {
            $(this).dialog("close");
        }
    });
</script>
<?php

$id = "";
if (isset($_POST['id'])) {
    $id = $_POST['id'];
    $requete = "DELETE FROM tags WHERE id = ?";
    $reponse = $bdd->prepare($requete);
    $reponse->bindValue(1, $id, PDO::PARAM_INT);
    $reponse->execute();
    $reponse->closeCursor();
    ?>
    <script>
        $(document).ready(function() {
            $.get("admin/categories-liste.html", {}, function(html) {
                $("#liste_categories").html(html);
            });
            $.get("admin/categories-infos.html", {}, function(html) {
                $("#infos_categories").html(html);
            });
        });
    </script>
    <p>La catégorie a bien été supprimée.</p>
    <script>
        $("#dialogbox").dialog("close");
    </script>
    <?php

} else {
    ?>
    <p>Impossible de supprimer la catégorie.</p>
    <?php

}
?>