<?php
$pageTitre = "Administration : Gestion des styles CSS";
$pageAdmin = true;
$pageStylesCSS = true;
include_once("../haut.php");
require_once("acces-admin.php");
?>
<h1><?php echo $pageTitre; ?></h1>
<form method="post" action="admin/styles-css-modification.html" class="centre" id="formStyles">
    <div class="contenuPage">
        <h2>Style général</h2>
        <textarea name="style01" id="style01" rows="25" cols="120"><?php
$fichier = fopen("../css/style_01.css", "r");
while ($ligne = fgets($fichier, 1024)) {
    echo $ligne;
}
fclose($fichier);
?>
        </textarea>
    </div>
    <div class="contenuPage">
        <h2>Style général (Internet Explorer)</h2>
        <textarea name="style01ie" id="style01ie" rows="25" cols="120"><?php
            $fichier = fopen("../css/style_01_ie.css", "r");
            while ($ligne = fgets($fichier, 1024)) {
                echo $ligne;
            }
            fclose($fichier);
?>
        </textarea>
    </div>
    <div class="contenuPage">
        <h2>Menu</h2>
        <textarea name="cssmenu" id="cssmenu" rows="25" cols="120"><?php
            $fichier = fopen("../css/menu.css", "r");
            while ($ligne = fgets($fichier, 1024)) {
                echo $ligne;
            }
            fclose($fichier);
?>
        </textarea>
    </div>
    <div class="contenuPage">
        <h2>jQuery UI</h2>
        <textarea name="jqueryui" id="jqueryui" rows="25" cols="120"><?php
            $fichier = fopen("../css/jquery-ui.css", "r");
            while ($ligne = fgets($fichier, 1024)) {
                echo $ligne;
            }
            fclose($fichier);
?>
        </textarea>
    </div>
    <div class="contenuPage">
        <h2>CodeMirror</h2>
        <textarea name="codemirror" id="codemirror" rows="25" cols="120"><?php
            $fichier = fopen("../css/jquery-codemirror.css", "r");
            while ($ligne = fgets($fichier, 1024)) {
                echo $ligne;
            }
            fclose($fichier);
?>
        </textarea>
    </div>
    <input class="bouton" id="back" type="button" onclick="goToUrl('admin/styles-css.html')" value="Annuler" />
    <input type="hidden" name="tokenValidation" value="<?php echo $_SESSION['tokenValidation']; ?>" />
    <input class="bouton" type="submit" value="Sauvegarder" />
</form>
<div id="dialogbox">
    <?php require_once("../loading.php"); ?>
</div>
<link rel="stylesheet" href="css/jquery-codemirror.css" />
<script src="js/jquery-codemirror.js" charset="utf-8"></script>
<script src="js/jquery-codemirror-css.js" charset="utf-8"></script>
<script>
    CodeMirror.fromTextArea(document.getElementById("style01"));
    CodeMirror.fromTextArea(document.getElementById("style01ie"));
    CodeMirror.fromTextArea(document.getElementById("cssmenu"));
    CodeMirror.fromTextArea(document.getElementById("jqueryui"));
    CodeMirror.fromTextArea(document.getElementById("codemirror"));
    $(document).ready(function() {
        $("#dialogbox").dialog({
            autoOpen : false,
            modal : true,
            resizable : false,
            draggable : false,
            show : "fade",
            hide : "fade",
            title : "Gestion des styles CSS",
            buttons : {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            }
        });
    });
    $("#formStyles").ajaxForm({
        target: "#dialogbox",
        beforeSubmit : function() {
            $("#dialogbox").dialog('open');
        }
    });
</script>
<?php
include_once("../bas.php");
?>