<?php
$pageTitre = "Administration : Gestion des catégories";
$pageCategories = true;
$pageAdmin = true;
include_once("../haut.php");
require_once("acces-membre.php");
?>
<h1>Administration : Gestion des catégories</h1>
<div class="contenuPage" id="infos_categories">
    <?php require_once("categories-infos.php"); ?>
</div>
<div class="contenuPage" id="liste_categories">
    <?php require_once("categories-liste.php"); ?>

</div>
<div class="centre">
    <a class="bouton" href="admin/categories-ajouter.html" title="Ajouter une catégorie">
        <img src="img/add.png" alt="" />
        Ajouter une catégorie
    </a>
</div>
<div id="dialogbox">
    <?php require_once("../loading.php"); ?>
</div>
<script>
    $(document).ready(function() {
        $("#dialogbox").dialog({
            autoOpen : false,
            modal : true,
            resizable : false,
            draggable : false,
            show : "fade",
            hide : "fade",
            title : "Suppression d'une catégorie",
            buttons : {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            }
        });
    });
    function trier(critere, ordre) {
        $.post("admin/categories-liste.html", {critere : critere, ordre : ordre}, function(html) {
            $("#liste_categories").html(html);
        });   
    }
    function supprimer(id) {
        $("#dialogbox").dialog('option', 'buttons', { 
            "Fermer" : function() {
                $(this).dialog("close");
            }
        });
        $.post("admin/categories-supprimer.html", {id : id}, function(html) {
            $("#dialogbox").html(html);
            $("#dialogbox").dialog('open');
        });    
    }
</script>

<?php include_once("../bas.php"); ?>