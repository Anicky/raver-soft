<?php
require_once("../bdd_config.php");
require_once("acces-admin.php");

$id = "";
if (isset($_POST['id'])) {
    $id = $_POST['id'];
    $requete = "SELECT id, pseudo FROM membres WHERE id = ?";
    $reponse = $bdd->prepare($requete);
    $reponse->bindValue(1, $id, PDO::PARAM_INT);
    $reponse->execute();
    $donnees = $reponse->fetch();
    if (($donnees != null) && ($donnees['id'] != $session_id)) {
        ?>
        <script>
            $("#dialogbox").dialog('option', 'buttons', { 
                "Non" : function() {
                    $(this).dialog("close");
                },
                "Oui" : function() {
                    $.post("admin/membres-suppression.html", {id : '<?php echo $id; ?>'}, function(html) {
                        $("#dialogbox").dialog('option', 'buttons', {
                            "Fermer" : function() {
                                $(this).dialog("close");
                            }
                        });
                        $("#dialogbox").html(html);
                    });
                }
            });
        </script>
        <p>Etes-vous sûr de vouloir supprimer ce membre ?</p>
        <?php
    } else {
        ?>
        <p>Le membre recherché est introuvable.</p>
        <?php
    }
    $reponse->closeCursor();
} else {
    ?>
    <p>Le membre recherché est introuvable.</p>
    <?php
}
?>