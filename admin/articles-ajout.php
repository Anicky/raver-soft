<?php

require_once("../bdd_config.php");
require_once("acces-membre.php");

if (isset($_POST['texte'])) {
    // Variables
    $titre = securite_bdd($_POST['titre']);
    $texte = securite_bdd($_POST['texte']);
    $commentairesActives = $_POST['commentairesActives'];
    $categories = "";
    if (isset($_POST['categories'])) {
        $categories = $_POST['categories'];
    }
    $id = "";
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    // Traitement
    if ($_SESSION['tokenValidation'] == $_POST['tokenValidation']) {
        if ($titre != "" && $texte != "" && $commentairesActives != "") {
            $ok = false;
            if ($id == "") {
                // Ajout                
                $requete = "INSERT INTO news(date, titre, texte, commentaires, id_auteur) VALUES (" . getHeureActuelle() . ", ?, ?, ?, ?)";
                $reponse = $bdd->prepare($requete);
                $reponse->bindValue(1, $titre, PDO::PARAM_STR);
                $reponse->bindValue(2, $texte, PDO::PARAM_STR);
                $reponse->bindValue(3, $commentairesActives, PDO::PARAM_BOOL);
                $reponse->bindValue(4, $session_id, PDO::PARAM_INT);
                $reponse->execute();
                $reponse->closeCursor();

                $requete = "SELECT id FROM news ORDER BY id DESC LIMIT 0,1";
                $reponse = $bdd->query($requete);
                $donnees = $reponse->fetch();
                $idNews = $donnees['id'];
                $reponse->closeCursor();
                foreach ($categories as $idCategorie) {
                    $requete = "INSERT INTO news_tags (id_news, id_tag) VALUES (?, ?)";
                    $reponse = $bdd->prepare($requete);
                    $reponse->bindValue(1, $idNews, PDO::PARAM_INT);
                    $reponse->bindValue(2, $idCategorie, PDO::PARAM_INT);
                    $reponse->execute();
                    $reponse->closeCursor();
                }
                $ok = true;
            } else {
                // Modification
                $requete = "UPDATE news SET titre = ?, texte = ?, commentaires = ? WHERE id = ?";
                $reponse = $bdd->prepare($requete);
                $reponse->bindValue(1, $titre, PDO::PARAM_STR);
                $reponse->bindValue(2, $texte, PDO::PARAM_STR);
                $reponse->bindValue(3, $commentairesActives, PDO::PARAM_BOOL);
                $reponse->bindValue(4, $id, PDO::PARAM_INT);
                $reponse->execute();
                $reponse->closeCursor();

                $requete = "SELECT id_tag FROM news_tags WHERE id_news = ?";
                $reponse = $bdd->prepare($requete);
                $reponse->bindValue(1, $id, PDO::PARAM_INT);
                $reponse->execute();
                $categoriesActuelles = array();
                while ($donnees = $reponse->fetch()) {
                    $categoriesActuelles[] = $donnees['id_tag'];
                }
                $reponse->closeCursor();

                // on enlève
                if (isset($categoriesActuelles)) {
                    foreach ($categoriesActuelles as $c) {
                        $trouve = false;
                        $i = 0;
                        while ((!$trouve) && ($i < count($categories))) {
                            if ($c == $categories[$i]) {
                                $trouve = true;
                            }
                            $i++;
                        }
                        if (!$trouve) {
                            $requete = "DELETE FROM news_tags WHERE id_news = ? AND id_tag = ?";
                            $reponse = $bdd->prepare($requete);
                            $reponse->bindValue(1, $id, PDO::PARAM_INT);
                            $reponse->bindValue(2, $c, PDO::PARAM_INT);
                            $reponse->execute();
                            $reponse->closeCursor();
                        }
                    }
                }
                // on ajoute
                foreach ($categories as $c) {
                    if (isset($categoriesActuelles)) {
                        $trouve = false;
                        $i = 0;
                        while ((!$trouve) && ($i < count($categoriesActuelles))) {
                            if ($c == $categoriesActuelles[$i]) {
                                $trouve = true;
                            }
                            $i++;
                        }
                        if (!$trouve) {
                            $requete = "INSERT INTO news_tags (id_news, id_tag) VALUES (?, ?)";
                            $reponse = $bdd->prepare($requete);
                            $reponse->bindValue(1, $id, PDO::PARAM_INT);
                            $reponse->bindValue(2, $c, PDO::PARAM_INT);
                            $reponse->execute();
                            $reponse->closeCursor();
                        }
                    } else {
                        $requete = "INSERT INTO news_tags (id_news, id_tag) VALUES (?, ?)";
                        $reponse = $bdd->prepare($requete);
                        $reponse->bindValue(1, $id, PDO::PARAM_INT);
                        $reponse->bindValue(2, $c, PDO::PARAM_INT);
                        $reponse->execute();
                        $reponse->closeCursor();
                    }
                }

                $ok = true;
            }
            if ($ok) {
                if ($id == "") {
                    ?>
                    <p>L'article a bien été ajouté.</p>
                    <?php

                } else {
                    ?>
                    <p>L'article a bien été modifié.</p>
                    <?php

                }
                ?>
                <script>
                    window.location.href = "admin/";
                </script>
                <?php

            }
        } else {
            ?>
            <p>Vous n'avez pas rempli tous les champs.</p>
            <?php

        }
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>