<?php
require_once("../bdd_config.php");
require_once("acces-admin.php");
$pageMembres = true;
$pageAdmin = true;

$id = "";
if (isset($_GET["id"])) {
    $id = $_GET["id"];
}

$pageTitre = "Administration : Ajouter un membre";
$pseudo = "";
$email = "";
$avatar = "";
$role = "Visiteur";

$image_tailleMax_ko = getParametre($bdd, "image_tailleMax_ko");

if ($id != "") {
    $requete = "SELECT membres.id AS idMembre, pseudo, email, avatar, roles.nom AS nomRole
        FROM membres, roles
        WHERE membres.id_role = roles.id
        AND membres.id = ?";
    $reponse = $bdd->prepare($requete);
    $reponse->bindValue(1, $id, PDO::PARAM_INT);
    $reponse->execute();
    $donnees = $reponse->fetch();
    if (($donnees != null) && ($donnees['idMembre'] != $session_id)) {
        $pageTitre = "Modifier un membre : " . securite_sortie($donnees['pseudo']);
        $pseudo = securite_sortie($donnees['pseudo']);
        $email = securite_sortie($donnees['email']);
        $avatar = securite_sortie($donnees['avatar']);
        $role = securite_sortie($donnees['nomRole']);
    } else {
        header("Location: " . URL . "page-introuvable.html");
        exit;
    }
    $reponse->closeCursor();
}
include_once("../haut.php");
?>
<h1><?php echo $pageTitre; ?></h1>
<form method="post" action="admin/membres-ajout.html" class="centre" enctype="multipart/form-data" id="ajouterMembre">
    <div class="contenuPage">
        <table class="formulaire">
            <tr>
                <td class="label">
                    <label for="pseudo">Pseudo</label>
                </td>
                <td>
                    <input type="text" name="pseudo" id="pseudo" value="<?php echo $pseudo; ?>" size="40" maxlength="100" onkeyup="verifier_infos()" />
                    <div id="verification_pseudo" class="verification"></div>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="email">Email</label>
                </td>
                <td>
                    <input type="text" name="email" id="email" value="<?php echo $email; ?>" size="40" maxlength="255" onkeyup="verifier_infos()" />
                    <div id="verification_email" class="verification"></div>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="avatar">Avatar</label>
                </td>
                <td>
                    <div id="checkbox_avatar">
                        <input type="radio" name="selection_avatar" id="selection_avatar_aucun" value="aucun"<?php
if ($avatar == "") {
    echo "checked=\"checked\"";
}
?> />
                        <label for="selection_avatar_aucun">Pas d'avatar</label>
                        <?php if ($avatar != "") { ?>
                            <input type="radio" name="selection_avatar" id="selection_avatar_garder" value="garder"<?php
                        if ($avatar != "") {
                            echo "checked=\"checked\"";
                        }
                            ?> />
                            <label for="selection_avatar_garder">Garder l'avatar actuel</label>
                        <?php } ?>
                        <input type="radio" name="selection_avatar" id="selection_avatar_choisir" value="choisir" />
                        <label for="selection_avatar_choisir">Choisir un avatar</label>
                    </div>
                    <div id="avatar_garder">
                        <?php if ($avatar != "") {
                            ?>
                            <img src="img/avatars/<?php echo $avatar; ?>" alt="Avatar introuvable" />
                            <?php
                        }
                        ?>
                    </div>
                    <div id="avatar_choisir">
                        <input type="file" name="avatar" id="avatar" size="40" />
                        <div class="warning">La taille maximum pour l'envoi d'une image est de <?php echo $image_tailleMax_ko; ?> Ko.</div>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="role">Type de compte</label>
                </td>
                <td>
                    <select name="role" id="role">
                        <?php
                        $requeteRole = "SELECT * FROM roles ORDER BY id ASC";
                        $reponseRole = $bdd->query($requeteRole);
                        while ($donneesRole = $reponseRole->fetch()) {
                            ?>
                            <option<?php
                        if ($donneesRole['nom'] == $role) {
                            echo " selected=\"selected\"";
                        }
                            ?>><?php echo securite_sortie($donneesRole['nom']); ?></option>
                                <?php
                            }
                            $reponseRole->closeCursor();
                            ?>
                    </select>
                </td>
            </tr>
        </table>
    </div>
    <?php if ($id != "") {
        ?>
        <input type="hidden" name="id" value="<?php echo $id; ?>" />
    <?php }
    ?>
    <input id="back" class="bouton" type="button" onclick="goToUrl('admin/membres.html')" value="Annuler" />
    <input type="hidden" name="tokenValidation" value="<?php echo $_SESSION['tokenValidation']; ?>" />
    <input class="bouton" type="submit" value="Sauvegarder" id="boutonSubmit" />
</form>
<?php
if ($avatar != "") {
    ?>
    <script>
        $("#avatar_garder").show();
    </script>
    <?php
}
?>
<div id="dialogbox">
    <?php require_once("../loading.php"); ?>
</div>
<script>
    $(document).ready(function() {
        $("#checkbox_avatar").buttonset();
        $("#dialogbox").dialog({
            autoOpen : false,
            modal : true,
            resizable : false,
            draggable : false,
            show : "fade",
            hide : "fade",
            title : "Ajout/Modification d'un membre",
            buttons : {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            }
        });
    });
    $("input:radio[name=selection_avatar]").change(function() {
        choix_avatar = $(this).val();
        if (choix_avatar == 'aucun') {
            $("#avatar_garder").hide();
            $("#avatar_choisir").hide();
        } else if (choix_avatar == 'garder') {
            $("#avatar_garder").show();
            $("#avatar_choisir").hide();
        } else if (choix_avatar == 'choisir') {
            $("#avatar_garder").hide();
            $("#avatar_choisir").show();
        }
    });
    $("#ajouterMembre").validate({
        rules: {
            pseudo : {
                required : true,
                maxlength : 100
            },
            email : {
                required : true,
                email : true,
                maxlength : 255
            }
        },
        messages: { 
            pseudo : {
                required : "Vous devez rentrer un pseudo.",
                maxlength : "Le pseudo ne peut pas dépasser 100 caractères."
            },
            email : {
                required : "Vous devez rentrer un email valide.",
                email : "Vous devez rentrer un email valide.",
                maxlength : "L'email ne peut pas dépasser 255 caractères."
            }
        } 
    });
    $("#ajouterMembre").ajaxForm({
        target: "#dialogbox",
        beforeSubmit : function() {
            $("#dialogbox").dialog('open');
        }
    });
    function verifier_infos() {
        $("#boutonSubmit").removeAttr("disabled");
        var idMembre = '<?php echo encodeUrl($pseudo); ?>';
        var pseudo = $("#pseudo").val();
        var email = $("#email").val();
        if (pseudo != "" || email != "") {
            if (idMembre != '') {
                $.get("membres-verification.html", {champ : 'pseudo', valeur : pseudo, id : idMembre}, function(html) {
                    $("#verification_pseudo").html(html);
                });
                $.get("membres-verification.html", {champ : 'email', valeur : email, id : idMembre}, function(html) {
                    $("#verification_email").html(html);
                });
            } else {
                $.get("membres-verification.html", {champ : 'pseudo', valeur : pseudo}, function(html) {
                    $("#verification_pseudo").html(html);
                });
                $.get("membres-verification.html", {champ : 'email', valeur : email}, function(html) {
                    $("#verification_email").html(html);
                });
            }
        }
    }
</script>
<?php
include_once("../bas.php");
?>