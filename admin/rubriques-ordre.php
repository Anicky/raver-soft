<?php

require_once("../bdd_config.php");
require_once("acces-admin.php");

foreach ($_POST['rubrique'] as $ordre => $id_rubrique) {
    $position = $ordre + 1;
    $requete = "UPDATE rubriques SET ordre = ? WHERE id = ?";
    $reponse = $bdd->prepare($requete);
    $reponse->bindValue(1, $position, PDO::PARAM_INT);
    $reponse->bindValue(2, $id_rubrique, PDO::PARAM_INT);
    $reponse->execute();
    $reponse->closeCursor();
}
?>