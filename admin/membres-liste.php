<?php
require_once("../bdd_config.php");
require_once("acces-admin.php");
?>
<table class="tableau">
    <thead>
        <tr>
            <th>
                Pseudo
                <a href="javascript:void(0)" title="Trier par ordre croissant" onclick="trier('pseudo', 'desc')"><img src="img/fleche1_haut_over.png" alt="ASC" /></a>
                <a href="javascript:void(0)" title="Trier par ordre croissant" onclick="trier('pseudo', 'asc')"><img src="img/fleche1_bas_over.png" alt="DESC" /></a>
            </th>
            <th>
                Email
                <a href="javascript:void(0)" title="Trier par ordre croissant" onclick="trier('email', 'desc')"><img src="img/fleche1_haut_over.png" alt="ASC" /></a>
                <a href="javascript:void(0)" title="Trier par ordre croissant" onclick="trier('email', 'asc')"><img src="img/fleche1_bas_over.png" alt="DESC" /></a>
            </th>
            <th>
                Type de compte
                <a href="javascript:void(0)" title="Trier par ordre croissant" onclick="trier('role', 'desc')"><img src="img/fleche1_haut_over.png" alt="ASC" /></a>
                <a href="javascript:void(0)" title="Trier par ordre croissant" onclick="trier('role', 'asc')"><img src="img/fleche1_bas_over.png" alt="DESC" /></a>
            </th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php
        $critere = "pseudo";
        $ordre = "ASC";
        if (isset($_POST['critere'])) {
            $critere = securite_bdd($_POST['critere']);
        }
        if (isset($_POST['ordre'])) {
            if ($_POST['ordre'] == "asc") {
                $ordre = "ASC";
            } else if ($_POST['ordre'] == "desc") {
                $ordre = "DESC";
            }
        }
        $requete = "SELECT pseudo,
            email,
            membres.id AS idMembre,
            nom AS role
            FROM membres, roles
            WHERE roles.id = membres.id_role
            ORDER BY " . $critere . " " . $ordre;
        $reponse = $bdd->query($requete);
        while ($donnees = $reponse->fetch()) {
            ?>
            <tr>
                <td><?php echo securite_sortie($donnees['pseudo']); ?></td>
                <td><?php echo securite_sortie($donnees['email']); ?></td>
                <td><?php echo securite_sortie($donnees['role']); ?></td>
                <td class="icone"><?php if (securite_sortie($donnees['idMembre']) != $session_id) { ?>
                        <a href="admin/membres-modifier-<?php echo $donnees['idMembre']; ?>.html" title="Modifier des informations sur le membre">
                            <img src="img/edit.png" alt="Modifier" />
                        </a>
                    <?php } ?></td>
                <td class="icone"><?php if (securite_sortie($donnees['idMembre']) != $session_id) { ?>
                        <a href="javascript:void(0)" onclick="supprimer('<?php echo $donnees['idMembre']; ?>')" title="Supprimer le membre">
                            <img src="img/delete.png" alt="Supprimer" />
                        </a>
                    <?php } ?></td>
            </tr>
            <?php
        }
        $reponse->closeCursor();
        ?>
    </tbody>
</table>
