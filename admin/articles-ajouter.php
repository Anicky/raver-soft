<?php
require_once("../bdd_config.php");
require_once("acces-membre.php");
$pageArticles = true;
$pageAdmin = true;

$id = "";
if (isset($_GET["id"])) {
    $id = $_GET["id"];
}

$pageTitre = "Administration : Ajouter un article";
$titre = "";
$texte = "";
$commentairesActives = true;
$idNews = "";

if ($id != "") {
    $requete = "SELECT id, titre, texte, commentaires FROM news WHERE id = ?";
    $reponse = $bdd->prepare($requete);
    $reponse->bindValue(1, $id, PDO::PARAM_INT);
    $reponse->execute();
    $donnees = $reponse->fetch();
    if ($donnees != null) {
        $idNews = $donnees['id'];
        $pageTitre = "Modifier un article : " . securite_sortie($donnees['titre']);
        $titre = securite_sortie($donnees['titre']);
        $texte = $donnees['texte'];
        $commentairesActives = securite_sortie($donnees['commentaires']);
    } else {
        header("Location: " . URL . "page-introuvable.html");
        exit;
    }
    $reponse->closeCursor();
}
include_once("../haut.php");
?>

<script src="js/ckeditor/ckeditor.js" charset="utf-8"></script>
<script src="js/ckeditor/adapters/jquery.js" charset="utf-8"></script>
<script src="js/ckeditor/plugins/codemirror/js/codemirror.js" charset="utf-8"></script>

<h1><?php echo $pageTitre; ?></h1>
<form method="post" action="admin/articles-ajout.html" class="centre" id="ajouterArticle" onSubmit="MirrorUpdate();">
    <div class="contenuPage">
        <table class="formulaire">
            <tr>
                <td class="label">
                    <label for="titre">Titre</label>
                </td>
                <td>
                    <input type="text" name="titre" id="titre" value="<?php echo $titre; ?>" size="100" maxlength="255" />
                </td>
            </tr>
            <tr>
                <td class="label">
                    Commentaires activés
                </td>
                <td>
                    <div id="radio_commentaires">
                        <label for="commentaires_oui">Oui</label>
                        <input type="radio" name="commentairesActives" id="commentaires_oui" value="1"<?php
if ($commentairesActives) {
    echo " checked=\"checked\"";
}
?> />
                        <label for="commentaires_non">Non</label>
                        <input type="radio" name="commentairesActives" id="commentaires_non" value="0"<?php
                               if (!$commentairesActives) {
                                   echo " checked=\"checked\"";
                               }
?> />
                    </div>
                </td>
            </tr>
            <tr>
                <td class="label">Catégorie(s)</td>
                <td>
                    <div id="checkbox_categories">
                        <?php
                        if ($id != "") {
                            $requete = "SELECT tags.id AS idTag
                                FROM tags, news_tags
                                WHERE news_tags.id_tag = tags.id
                                AND news_tags.id_news = ?";
                            $reponse = $bdd->prepare($requete);
                            $reponse->bindValue(1, $idNews, PDO::PARAM_INT);
                            $reponse->execute();
                            $categorie = array();
                            while ($donnees = $reponse->fetch()) {
                                $categorie[] = securite_sortie($donnees['idTag']);
                            }
                            $reponse->closeCursor();
                        }
                        $requete = "SELECT id, nom FROM tags ORDER BY nom ASC";
                        $reponse = $bdd->query($requete);
                        while ($donnees = $reponse->fetch()) {
                            $categorieId = securite_sortie($donnees['id']);
                            $categorieNom = securite_sortie($donnees['nom']);
                            ?>
                            <input type="checkbox" name="categories[]" value="<?php echo $categorieId; ?>" id="categorie_<?php echo $categorieId; ?>"
                            <?php
                            if (isset($categorie)) {
                                $trouve = false;
                                $i = 0;
                                while ($i < count($categorie) && !$trouve) {
                                    if ($categorie[$i] == $categorieId) {
                                        $trouve = true;
                                        echo " checked=\"checked\"";
                                    }
                                    $i++;
                                };
                            }
                            ?> />
                            <label for="categorie_<?php echo $categorieId; ?>"><?php echo $categorieNom; ?></label>
                            <?php
                        }
                        $reponse->closeCursor();
                        ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="texte">Contenu</label>
                </td>
                <td>
                    <p>La preview d'un article fait : <?php echo getParametre($bdd, "nombre_mots_preview"); ?> mots
                        <br />Pour couper la preview avant (par exemple pour éviter la coupure en plein milieu d'une phrase), écrivez <strong>[more]</strong>.</p>

                    <textarea name="texte" id="texte"><?php echo $texte; ?></textarea>
                </td>
            </tr>
        </table>
    </div>
    <?php if ($id != "") {
        ?>
        <input type="hidden" name="id" value="<?php echo $idNews; ?>" />
    <?php }
    ?>
    <input id="back" class="bouton" type="button" onclick="goToUrl('admin/')" value="Annuler" />
    <input type="hidden" name="tokenValidation" value="<?php echo $_SESSION['tokenValidation']; ?>" />
    <input class="bouton" type="submit" value="Sauvegarder" id="boutonSubmit" />
</form>
<div id="dialogbox">
    <?php require_once("../loading.php"); ?>
</div>
<script>
    $(document).ready(function() {
        $("#texte").ckeditor();
        $("#radio_commentaires").buttonset();
        $("#checkbox_categories").buttonset();
        $("#dialogbox").dialog({
            autoOpen : false,
            modal : true,
            resizable : false,
            draggable : false,
            show : "fade",
            hide : "fade",
            title : "Ajout/Modification d'un article",
            buttons : {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            }
        });
    });
    $("#ajouterArticle").validate({
        rules: {
            titre : {
                required : true,
                maxlength : 255
            },
            texte : {
                required : true
            },
            commentairesActives : {
                required : true
            },
            "categories[]" : {
                required : true
            }
        },
        messages: { 
            titre : {
                required : "Vous devez rentrer un titre.",
                maxlength : "Le titre ne peut pas dépasser 255 caractères."
            },
            texte : {
                required : "Vous devez rentrer du texte."
            },
            commentairesActives : {
                required : "Vous devez indiquer si les commentaires sont activés ou non."
            },
            "categories[]" : {
                required : "Vous devez sélectionner au moins une catégorie."
            }
        } 
    });
    $("#ajouterArticle").ajaxForm({
        target: "#dialogbox",
        beforeSubmit : function() {
            $("#dialogbox").dialog('open');
        }
    });
    function MirrorUpdate() {
        for (i in CKEDITOR.instances) {
            CKEDITOR.instances[i].execCommand('mirrorSnapshot');
        }  
    }
</script>
<?php
include_once("../bas.php");
?>