<?php

require_once("../bdd_config.php");
require_once("acces-membre.php");

if (isset($_POST['nom'])) {
    // Variables
    $nom = securite_bdd($_POST['nom']);
    $id = "";
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    // Traitement
    if ($_SESSION['tokenValidation'] == $_POST['tokenValidation']) {
        if ($nom != "") {
            $requete = "SELECT nom FROM tags";
            $reponse = $bdd->query($requete);
            $trouve = false;
            while (($donnees = $reponse->fetch()) && (!$trouve)) {
                if (encodeUrl($nom) == encodeUrl($donnees['nom'])) {
                    $trouve = true;
                }
            }
            $reponse->closeCursor();
            if (!$trouve) {
                $ok = false;
                if ($id == "") {
                    // Ajout
                    $requete = "INSERT INTO tags(nom, url) VALUES (?, ?)";
                    $reponse = $bdd->prepare($requete);
                    $reponse->bindValue(1, $nom, PDO::PARAM_STR);
                    $reponse->bindValue(2, encodeUrl($nom), PDO::PARAM_STR);
                    $reponse->execute();
                    $reponse->closeCursor();
                    $ok = true;
                } else {
                    // Modification
                    $requete = "UPDATE tags SET nom = ?, url = ? WHERE id = ?";
                    $reponse = $bdd->prepare($requete);
                    $reponse->bindValue(1, $nom, PDO::PARAM_STR);
                    $reponse->bindValue(2, encodeUrl($nom), PDO::PARAM_STR);
                    $reponse->bindValue(3, $id, PDO::PARAM_INT);
                    $reponse->execute();
                    $reponse->closeCursor();
                    $ok = true;
                }
                if ($ok) {
                    if ($id == "") {
                        ?>
                        <p>La catégorie a bien été ajoutée.</p>
                        <?php

                    } else {
                        ?>
                        <p>La catégorie a bien été modifiée.</p>
                        <?php

                    }
                    ?>
                    <script>
                        window.location.href = "admin/categories.html";
                    </script>
                    <?php

                }
            } else {
                ?>
                <p>Il existe déjà une catégorie avec ce nom.</p>
                <?php

            }
        } else {
            ?>
            <p>Vous n'avez pas rempli tous les champs.</p>
            <?php

        }
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>