<?php

require_once("../bdd_config.php");
require_once("acces-admin.php");

if (isset($_POST['pseudo'])) {
    // Variables
    $pseudo = securite_bdd($_POST['pseudo']);
    $email = securite_bdd($_POST['email']);
    $role = securite_bdd($_POST['role']);
    $avatar = "";
    $id = "";
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    // Traitement
    if ($_SESSION['tokenValidation'] == $_POST['tokenValidation']) {
        if ($pseudo != "" && $email != "" && $role != "") {
            $ok = false;

            $requete = "SELECT * FROM roles WHERE nom = ?";
            $reponse = $bdd->prepare($requete);
            $reponse->bindValue(1, $role, PDO::PARAM_STR);
            $reponse->execute();
            $donnees = $reponse->fetch();
            if ($donnees != null) {
                $role = $donnees['id'];
                $reponse->closeCursor();
                if ($id == "") {
                    // Ajout
                    $passwordNonCrypte = chaineAleatoire(8);
                    $password = crypter($passwordNonCrypte);
                    if (isset($_FILES['avatar'])) {
                        $tailleMax = getParametre($bdd, "image_tailleMax_ko");
                        $avatar = envoiImage($_FILES['avatar'], $tailleMax);
                    }
                    $requete = "INSERT INTO membres(pseudo, password, email, avatar, id_role) VALUES (?, ?, ?, ?, ?)";
                    $reponse = $bdd->prepare($requete);
                    $reponse->bindValue(1, $pseudo, PDO::PARAM_STR);
                    $reponse->bindValue(2, $password, PDO::PARAM_STR);
                    $reponse->bindValue(3, $email, PDO::PARAM_STR);
                    $reponse->bindValue(4, $avatar, PDO::PARAM_STR);
                    $reponse->bindValue(5, $role, PDO::PARAM_INT);
                    $reponse->execute();
                    $reponse->closeCursor();
                    $ok = true;
                } else {
                    // Modification
                    if ($_POST['selection_avatar'] == "garder") {
                        $requete = "UPDATE membres SET pseudo = ?, email = ?, id_role = ? WHERE id = ?";
                        $reponse = $bdd->prepare($requete);
                        $reponse->bindValue(1, $pseudo, PDO::PARAM_STR);
                        $reponse->bindValue(2, $email, PDO::PARAM_STR);
                        $reponse->bindValue(3, $role, PDO::PARAM_INT);
                        $reponse->bindValue(4, $id, PDO::PARAM_INT);
                        $reponse->execute();
                        $reponse->closeCursor();
                    } else if ($_POST['selection_avatar'] == "choisir") {
                        if (isset($_FILES['avatar'])) {
                            $tailleMax = getParametre($bdd, "image_tailleMax_ko");
                            $avatar = envoiImage($_FILES['avatar'], $tailleMax);
                        }
                        $requete = "UPDATE membres SET pseudo = ?, email = ?, id_role = ?, avatar = ? WHERE id = ?";
                        $reponse = $bdd->prepare($requete);
                        $reponse->bindValue(1, $pseudo, PDO::PARAM_STR);
                        $reponse->bindValue(2, $email, PDO::PARAM_STR);
                        $reponse->bindValue(3, $role, PDO::PARAM_INT);
                        $reponse->bindValue(4, $avatar, PDO::PARAM_STR);
                        $reponse->bindValue(5, $id, PDO::PARAM_INT);
                        $reponse->execute();
                        $reponse->closeCursor();
                    } else {
                        $requete = "UPDATE membres SET pseudo = ?, email = ?, id_role = ?, avatar = '' WHERE id = ?";
                        $reponse = $bdd->prepare($requete);
                        $reponse->bindValue(1, $pseudo, PDO::PARAM_STR);
                        $reponse->bindValue(2, $email, PDO::PARAM_STR);
                        $reponse->bindValue(3, $role, PDO::PARAM_INT);
                        $reponse->bindValue(4, $id, PDO::PARAM_INT);
                        $reponse->execute();
                        $reponse->closeCursor();
                    }
                    $ok = true;
                }
            }
            if ($ok) {
                ?>
                <?php if ($id == "") {
                    ?>
                    <p>Le membre a bien été ajouté.</p>
                    <?php

                    require_once("../fonctions-mails.php");
                    if (mail_inscription($pseudo, $passwordNonCrypte, $email)) {
                        ?>
                        <p>Un mail de confirmation d'inscription a été envoyé au membre ajouté.</p>
                        <script>
                            window.location.href = "admin/membres.html";
                        </script>
                        <?php

                    } else {
                        ?>
                        <p>Le mail de confirmation d'inscription n'a pas pu être envoyé au membre ajouté.</p>
                        <script>
                            $("#dialogbox").bind('dialogclose', function() {
                                window.location.href = "admin/membres.html";
                            });
                        </script>
                        <?php

                    }
                } else {
                    ?>
                    <p>Le membre a bien été modifié.</p>
                    <script>
                        window.location.href = "admin/membres.html";
                    </script>
                    <?php

                }
            }
        } else {
            ?>
            <p>Vous n'avez pas rempli tous les champs.</p>
            <?php

        }
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>