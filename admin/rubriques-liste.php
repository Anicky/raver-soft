<?php
require_once("../bdd_config.php");
require_once("acces-membre.php");
?>
<ul id="listeRubriques"<?php if ($session_role == "Administrateur") { ?> class="modifiable" <?php } ?>>
    <?php
    $requete = "SELECT id, nom FROM rubriques ORDER BY ordre ASC";
    $reponse = $bdd->query($requete);
    $i = 0;
    while ($donnees = $reponse->fetch()) {
        $i++;
        ?>
        <li id="rubrique_<?php echo $donnees['id']; ?>"><?php echo securite_sortie($donnees['nom']); ?>
            <a href="admin/pages-ajouter-<?php echo $donnees['id']; ?>.html" title="Ajouter une page"><img src="img/add.png" alt="Ajouter une page" /></a>
            <a href="admin/rubriques-modifier-<?php echo $donnees['id']; ?>.html" title="Modifier des informations sur la rubrique"><img src="img/edit.png" alt="Modifier" /></a>
            <?php
            if ($session_role == "Administrateur") {
                ?>
                <a href="javascript:void(0)" onclick="supprimer('<?php echo $donnees['id']; ?>')" title="Supprimer la rubrique"><img src="img/delete.png" alt="Supprimer" /></a>
                <?php
            }
            $requeteSousRubrique = "SELECT id, nom FROM pages WHERE rubrique_id = ? ORDER BY ordre ASC";
            $reponseSousRubrique = $bdd->prepare($requeteSousRubrique);
            $reponseSousRubrique->bindValue(1, $donnees['id'], PDO::PARAM_INT);
            $reponseSousRubrique->execute();
            if ($reponseSousRubrique->rowCount() > 0) {
                ?>
                <ul id="listePages_<?php echo $i; ?>"<?php if ($session_role == "Administrateur") { ?> class="modifiable" <?php } ?>>
                    <?php
                    while ($donneesSousRubrique = $reponseSousRubrique->fetch()) {
                        ?>
                        <li id="page_<?php echo $donneesSousRubrique['id']; ?>">
                            <?php echo securite_sortie($donneesSousRubrique['nom']); ?>
                            <a href="admin/pages-modifier-<?php echo $donneesSousRubrique['id']; ?>.html" title="Modifier des informations sur la page"><img src="img/edit.png" alt="Modifier" /></a>
                            <?php
                            if ($session_role == "Administrateur") {
                                ?>
                                <a href="javascript:void(0)" onclick="supprimerPage('<?php echo $donneesSousRubrique['id']; ?>')" title="Supprimer la page"><img src="img/delete.png" alt="Supprimer" /></a>
                                <?php
                            }
                            ?>
                        </li>
                        <?php
                    }
                    if ($session_role == "Administrateur") {
                        ?>
                        <script>
                        $("#listePages_<?php echo $i; ?>").sortable({
                            placeholder: 'highlight',
                            update: function() {
                                var ordre = $("#listePages_<?php echo $i; ?>").sortable("serialize");
                                $.post("admin/pages-ordre.html", ordre);
                                $.get("menu.html", {}, function(html) {
                                    $("nav").html(html);
                                    ddsmoothmenu.init({
                                        mainmenuid: "menu",
                                        classname: "menu",
                                        arrowswap: true
                                    });
                                });
                            }
                        });
                        $("#listePages_<?php echo $i; ?>").disableSelection();
                        </script>
                        <?php
                    }
                    ?>
                </ul>
                <?php
            }
            $reponseSousRubrique->closeCursor();
            ?>
        </li>
        <?php
    }
    $reponse->closeCursor();
    ?>
</ul>