<?php
require_once("../bdd_config.php");
require_once("acces-admin.php");
?>
Nombre de membres : <strong><?php echo nombreElements($bdd, "membres"); ?></strong>
<ul>
    <li>Visiteurs : <strong><?php echo nombreElements($bdd, "membres, roles WHERE membres.id_role = roles.id AND roles.nom = 'Visiteur'"); ?></strong></li>
    <li>Membres Raver Soft : <strong><?php echo nombreElements($bdd, "membres, roles WHERE membres.id_role = roles.id AND roles.nom = 'Membre'"); ?></strong></li>
    <li>Administrateurs : <strong><?php echo nombreElements($bdd, "membres, roles WHERE membres.id_role = roles.id AND roles.nom = 'Administrateur'"); ?></strong></li>
</ul>