<?php
require_once("../bdd_config.php");
require_once("acces-membre.php");
$pageTelechargements = true;
$pageAdmin = true;

$id = "";
if (isset($_GET["id"])) {
    $id = $_GET["id"];
}

$pageTitre = "Administration : Ajouter un téléchargement";
$nom = "";
$idType = "";
$url = "";

if ($id != "") {
    $requete = "SELECT nom, url, extension, id_type FROM telechargements WHERE id = ?";
    $reponse = $bdd->prepare($requete);
    $reponse->bindValue(1, $id, PDO::PARAM_INT);
    $reponse->execute();
    $donnees = $reponse->fetch();
    if ($donnees != null) {
        $nom = securite_sortie($donnees['nom']);
        $url = securite_sortie($donnees['url']) . "." . securite_sortie($donnees['extension']);
        $idType = $donnees['id_type'];
    } else {
        header("Location: " . URL . "page-introuvable.html");
        exit;
    }
    $reponse->closeCursor();
}
include_once("../haut.php");
?>
<h1><?php echo $pageTitre; ?></h1>
<form method="post" action="admin/telechargements-ajout.html" class="centre" id="ajouterTelechargement">
    <div class="contenuPage">
        <table class="formulaire">
            <tr>
                <td class="label">
                    <label for="nom">Nom</label>
                </td>
                <td>
                    <input type="text" name="nom" id="nom" value="<?php echo $nom; ?>" size="40" maxlength="100" />
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="type">Type</label>
                </td>
                <td>
                    <select name="type" id="type">
                        <?php
                        $requete = "SELECT id, nom FROM types_telechargements ORDER BY nom ASC";
                        $reponse = $bdd->query($requete);
                        while ($donnees = $reponse->fetch()) {
                            ?>
                            <option value="<?php echo encodeUrl($donnees['id']); ?>"<?php if ($donnees['id'] == $idType) {
                            echo "selected=\"selected\"";
                        } ?>><?php echo securite_sortie($donnees['nom']); ?></option>
                            <?php
                        }
                        $reponse->closeCursor();
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="url">Lien</label>
                </td>
                <td>
                    <input type="text" name="url" id="url" value="<?php echo $url; ?>" size="40" maxlength="100" />
                </td>
            </tr>
        </table>
    </div>
    <?php if ($id != "") {
        ?>
        <input type="hidden" name="id" value="<?php echo $id; ?>" />
    <?php }
    ?>
    <input id="back" class="bouton" type="button" onclick="goToUrl('admin/telechargements.html')" value="Annuler" />
    <input type="hidden" name="tokenValidation" value="<?php echo $_SESSION['tokenValidation']; ?>" />
    <input class="bouton" type="submit" value="Sauvegarder" id="boutonSubmit" />
</form>
<div id="dialogbox">
    <?php require_once("../loading.php"); ?>
</div>
<script>
    $(document).ready(function() {
        $("#dialogbox").dialog({
            autoOpen : false,
            modal : true,
            resizable : false,
            draggable : false,
            show : "fade",
            hide : "fade",
            title : "Ajout/Modification d'un téléchargement",
            buttons : {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            }
        });
    });
    $("#ajouterTelechargement").validate({
        rules: {
            nom : {
                required : true,
                maxlength : 100
            }
        },
        messages: { 
            nom : {
                required : "Vous devez rentrer un nom.",
                maxlength : "Le nom ne peut pas dépasser 100 caractères."
            }
        } 
    });
    $("#ajouterTelechargement").ajaxForm({
        target: "#dialogbox",
        beforeSubmit : function() {
            $("#dialogbox").dialog('open');
        }
    });
</script>
<?php
include_once("../bas.php");
?>