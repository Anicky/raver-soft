<?php

$fichier = "../news.rss";
$fichierInput = fopen($fichier, "w");
if ($fichierInput != "") {
    $rss_titre = "Raver Soft";
    $rss_description = "Les dernières news de Raver Soft";
    $rss_limite = 10;

    $flux = "<?xml version='1.0' encoding='UTF-8'?>\n";
    $flux .= "<rss version='2.0'>\n";
    $flux .= "    <channel>  \n";
    $flux .= "        <title>" . $rss_titre . "</title>\n";
    $flux .= "        <link>" . URL . "</link>\n";
    $flux .= "        <description>" . $rss_description . "</description>  \n";

    $requete = "SELECT id,
        titre,
        UNIX_TIMESTAMP(date) AS dateTimestamp,
        texte
        FROM news
        WHERE publiee = true
        ORDER BY date DESC
        LIMIT 0,?";
    $reponse = $bdd->prepare($requete);
    $reponse->bindValue(1, $rss_limite, PDO::PARAM_INT);
    $reponse->execute();

    while ($donnees = $reponse->fetch()) {
        $flux .= "        <item>\n";
        $flux .= "            <title>" . securite_sortie($donnees['titre']) . "</title>\n";
        $flux .= "            <link>" . URL . "news/" . $donnees['id'] . "-" . encodeUrl($donnees['titre']) . ".html</link>\n";
        $flux .= "            <guid isPermaLink='false'>" . $donnees['id'] . "</guid>\n";
        $flux .= "            <description>" . str_replace("[more]", "", securite_sortie($donnees['texte'])) . "</description>\n";
        $flux .= "            <pubDate>" . date('D, j M Y H:i:s', $donnees['dateTimestamp']) . " GMT</pubDate>\n";
        $flux .= "        </item>\n";
    }
    $reponse->closeCursor();

    $flux .= "    </channel>\n";
    $flux .= "</rss>\n";

    fwrite($fichierInput, $flux);
    fclose($fichierInput);
}
?>