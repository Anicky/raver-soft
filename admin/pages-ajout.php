<?php

require_once("../bdd_config.php");
require_once("acces-membre.php");

if (isset($_POST['texte'])) {
    // Variables
    $nom = securite_bdd($_POST['nom']);
    $texte = securite_bdd($_POST['texte']);
    $rubrique = $_POST['rubrique'];
    $id = "";
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    // Traitement
    if ($_SESSION['tokenValidation'] == $_POST['tokenValidation']) {
        if ($nom != "" && $texte != "" && $rubrique != "") {
            if ($id == "") {
                // Ajout
                $requete = "SELECT MAX(ordre) AS ordreMax FROM pages WHERE rubrique_id = ?";
                $reponse = $bdd->prepare($requete);
                $reponse->bindValue(1, $rubrique, PDO::PARAM_INT);
                $reponse->execute();
                $donnees = $reponse->fetch();
                $ordre = $donnees['ordreMax'] + 1;
                $reponse->closeCursor();

                $requete = "INSERT INTO pages(ordre, nom, url, texte, rubrique_id) VALUES (?, ?, ?, ?, ?)";
                $reponse = $bdd->prepare($requete);
                $reponse->bindValue(1, $ordre, PDO::PARAM_INT);
                $reponse->bindValue(2, $nom, PDO::PARAM_STR);
                $reponse->bindValue(3, encodeUrl($nom), PDO::PARAM_STR);
                $reponse->bindValue(4, $texte, PDO::PARAM_STR);
                $reponse->bindValue(5, $rubrique, PDO::PARAM_INT);
                $reponse->execute();
                $reponse->closeCursor();
                ?>
                <p>La page a bien été ajoutée.</p>
                <?php

            } else {
                // Modification
                $requete = "UPDATE pages SET nom = ?, url = ?, texte = ?, rubrique_id = ? WHERE id = ?";
                $reponse = $bdd->prepare($requete);
                $reponse->bindValue(1, $nom, PDO::PARAM_STR);
                $reponse->bindValue(2, encodeUrl($nom), PDO::PARAM_STR);
                $reponse->bindValue(3, $texte, PDO::PARAM_STR);
                $reponse->bindValue(4, $rubrique, PDO::PARAM_INT);
                $reponse->bindValue(5, $id, PDO::PARAM_INT);
                $reponse->execute();
                $reponse->closeCursor();
                ?>
                <p>La page a bien été modifiée.</p>
                <?php

            }
            ?>
            <script>
                window.location.href = "admin/rubriques.html";
            </script>
            <?php

        } else {
            ?>
            <p>Vous n'avez pas rempli tous les champs.</p>
            <?php

        }
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>