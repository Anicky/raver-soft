<?php
require_once("../bdd_config.php");
require_once("acces-membre.php");

$typeGraphe = "line";
if (isset($_POST['type'])) {
    $typeGraphe = $_POST['type'];
}

$nombre_news_stats = getParametre($bdd, "nombre_news_stats");
$requete = "SELECT id, titre, vues, DATE_FORMAT(date, '%d/%m/%Y (%Hh%i)') AS dateFormatee FROM news ORDER BY date DESC LIMIT 0, " . $nombre_news_stats . ";";
$reponse = $bdd->query($requete);
$nombreNews = $reponse->rowCount();
$data = "";
$data2 = "";
$abscisses = "";
$vues = array();
$commentaires = array();
$titres = array();
$dates = array();
while ($donnees = $reponse->fetch()) {
    $vues[] = $donnees['vues'];
    $titres[] = securite_sortie($donnees['titre']);
    $dates[] = $donnees['dateFormatee'];
    $commentaires[] = nombreElements($bdd, "commentaires WHERE id_news = " . securite_sortie($donnees['id']) . ";");
}

$j = 0;
for ($i = ($nombreNews - 1); $i >= 0; $i--) {
    $abscisses .= "\"" . $dates[$i] . "\"";
    $vue_nom = "vue";
    if (($vues[$i] == 0) || ($vues[$i] > 1)) {
        $vue_nom .= "s";
    }
    $commentaire_nom = "commentaire";
    if (($commentaires[$i] == 0) || ($commentaires[$i] > 1)) {
        $commentaire_nom .= "s";
    }
    $data .= "{ name : \"" . $titres[$i] . " : <strong>" . $vues[$i] . " " . $vue_nom . "</strong>\", x : " . $j . ", y : " . $vues[$i] . "}";
    $data2 .= "{ name : \"" . $titres[$i] . " : <strong>" . $commentaires[$i] . " " . $commentaire_nom . "</strong>\", x : " . $j . ", y : " . $commentaires[$i] . "}";
    if ($i > 0) {
        $data .= ",\n";
        $data2 .= ",\n";
        $abscisses .= ",\n";
    }
    $j++;
}
$reponse->closeCursor();
?>
<script>
    $(document).ready(function() {
        new Highcharts.Chart({
            chart: {
                renderTo: 'stats_articles',
                type: '<?php echo $typeGraphe; ?>'
            },
            title: {
                text: 'Nombre de vues/commentaires sur les <?php echo $nombre_news_stats; ?> dernières news'
            },
            xAxis: {
                categories : [<?php echo $abscisses; ?>],
                labels : {
                    y : +20
                }
            },
            yAxis: {
                endOnTick : true,
                min : 0,
                title: {
                    text: 'Nombre de vues/commentaires'
                },
                plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
            },
            tooltip: {
                formatter: function() {
                    return this.point.name;
                }
            },
            series: [{
                    name : "Nombre de vues",
                    data: [<?php echo $data; ?>]
                }, {
                    name : "Nombre de commentaires",
                    data: [<?php echo $data2; ?>]
                }]
        });
    });
</script>