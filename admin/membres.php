<?php
$pageTitre = "Administration : Gestion des membres";
$pageMembres = true;
$pageAdmin = true;
include_once("../haut.php");
require_once("acces-admin.php");
?>
<h1>Administration : Gestion des membres</h1>
<div class="contenuPage" id="infos_membres">
    <?php require_once("membres-infos.php"); ?>
</div>
<div class="contenuPage" id="liste_membres">
    <?php require_once("membres-liste.php"); ?>
</div>
<div class="centre">
    <a class="bouton" href="admin/membres-ajouter.html" title="Ajouter un membre">
        <img src="img/add.png" alt="" />
        Ajouter un membre
    </a>
</div>
<div id="dialogbox">
    <?php require_once("../loading.php"); ?>
</div>
<script>
    $(document).ready(function() {
        $("#dialogbox").dialog({
            autoOpen : false,
            modal : true,
            resizable : false,
            draggable : false,
            show : "fade",
            hide : "fade",
            title : "Suppression d'un membre",
            buttons : {
                "Fermer" : function() {
                    $(this).dialog("close");
                }
            }
        });
    });
    function trier(critere, ordre) {
        $.post("admin/membres-liste.html", {critere : critere, ordre : ordre}, function(html) {
            $("#liste_membres").html(html);
        });   
    }
    function supprimer(id) {
        $("#dialogbox").dialog('option', 'buttons', { 
            "Fermer" : function() {
                $(this).dialog("close");
            }
        });
        $.post("admin/membres-supprimer.html", {id : id}, function(html) {
            $("#dialogbox").html(html);
            $("#dialogbox").dialog('open');
        });    
    }
</script>

<?php include_once("../bas.php"); ?>