<?php

require_once("../bdd_config.php");
require_once("acces-admin.php");

if (isset($_POST['nombre_news_page'])) {
    // Variables
    $nombre_news_page = $_POST['nombre_news_page'];
    $nombre_mots_preview = $_POST['nombre_mots_preview'];
    $image_tailleMax_ko = $_POST['image_tailleMax_ko'];
    $nombre_news_stats = $_POST['nombre_news_stats'];
    $news_afficher_avatar = $_POST['news_afficher_avatar'];
    $inscription_ouverte = $_POST['inscription_ouverte'];
    // Traitement
    if ($_SESSION['tokenValidation'] == $_POST['tokenValidation']) {
        if (($nombre_news_page != "") && ($nombre_mots_preview != "") && ($image_tailleMax_ko != "") && ($nombre_news_stats != "") && ($news_afficher_avatar != "") && ($inscription_ouverte != "")) {
            // Nombre de news par page
            $requete = "UPDATE parametres SET valeur = ? WHERE nom = 'nombre_news_page'";
            $reponse = $bdd->prepare($requete);
            $reponse->bindValue(1, $nombre_news_page, PDO::PARAM_INT);
            $reponse->execute();
            $reponse->closeCursor();

            // Nombre de mots par preview
            $requete = "UPDATE parametres SET valeur = ? WHERE nom = 'nombre_mots_preview'";
            $reponse = $bdd->prepare($requete);
            $reponse->bindValue(1, $nombre_mots_preview, PDO::PARAM_INT);
            $reponse->execute();
            $reponse->closeCursor();

            // Taille maximum d'une image
            $requete = "UPDATE parametres SET valeur = ? WHERE nom = 'image_tailleMax_ko'";
            $reponse = $bdd->prepare($requete);
            $reponse->bindValue(1, $image_tailleMax_ko, PDO::PARAM_INT);
            $reponse->execute();
            $reponse->closeCursor();
            
            // Nombre de news à afficher dans la partie Statistiques
            $requete = "UPDATE parametres SET valeur = ? WHERE nom = 'nombre_news_stats'";
            $reponse = $bdd->prepare($requete);
            $reponse->bindValue(1, $nombre_news_stats, PDO::PARAM_INT);
            $reponse->execute();
            $reponse->closeCursor();
            
            // Afficher l'avatar de l'auteur d'une news
            $requete = "UPDATE parametres SET valeur = ? WHERE nom = 'news_afficher_avatar'";
            $reponse = $bdd->prepare($requete);
            $reponse->bindValue(1, $news_afficher_avatar, PDO::PARAM_INT);
            $reponse->execute();
            $reponse->closeCursor();
            
            // Autoriser les inscriptions
            $requete = "UPDATE parametres SET valeur = ? WHERE nom = 'inscription_ouverte'";
            $reponse = $bdd->prepare($requete);
            $reponse->bindValue(1, $inscription_ouverte, PDO::PARAM_INT);
            $reponse->execute();
            $reponse->closeCursor();
            ?>
            <p>Les paramètres ont bien été sauvegardés.</p>
            <?php

        } else {
            ?>
            <p>Vous n'avez pas rempli tous les champs.</p>
            <?php

        }
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>