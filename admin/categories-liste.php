<?php
require_once("../bdd_config.php");
require_once("acces-membre.php");
?>
<table class="tableau">
    <thead>
        <tr>
            <th>
                Nom
                <a href="javascript:void(0)" title="Trier par ordre croissant" onclick="trier('nom', 'desc')"><img src="img/fleche1_haut_over.png" alt="ASC" /></a>
                <a href="javascript:void(0)" title="Trier par ordre croissant" onclick="trier('nom', 'asc')"><img src="img/fleche1_bas_over.png" alt="DESC" /></a>
            </th>
            <th></th>
            <?php
            if ($session_role == "Administrateur") {
                ?>
                <th></th>
                <?php
            }
            ?>
        </tr>
    </thead>
    <tbody>
        <?php
        $critere = "nom";
        $ordre = "ASC";
        if (isset($_POST['critere'])) {
            $critere = securite_bdd($_POST['critere']);
        }
        if (isset($_POST['ordre'])) {
            if ($_POST['ordre'] == "asc") {
                $ordre = "ASC";
            } else if ($_POST['ordre'] == "desc") {
                $ordre = "DESC";
            }
        }
        $requete = "SELECT id, nom FROM tags ORDER BY " . $critere . " " . $ordre;
        $reponse = $bdd->query($requete);
        while ($donnees = $reponse->fetch()) {
            ?>
            <tr>
                <td class="centre"><?php echo securite_sortie($donnees['nom']); ?></td>
                <td class="icone">
                    <a href="admin/categories-modifier-<?php echo $donnees['id']; ?>.html" title="Modifier la catégorie">
                        <img src="img/edit.png" alt="Modifier" />
                    </a>
                </td>
                <?php
                if ($session_role == "Administrateur") {
                    ?>
                    <td class="icone">
                        <a href="javascript:void(0)" onclick="supprimer('<?php echo $donnees['id']; ?>')" title="Supprimer la catégorie">
                            <img src="img/delete.png" alt="Supprimer" />
                        </a>
                    </td>
                    <?php
                }
                ?>
            </tr>
            <?php
        }
        $reponse->closeCursor();
        ?>
    </tbody>
</table>