<?php

require_once("../bdd_config.php");
require_once("acces-admin.php");

if (isset($_POST['footer'])) {
    // Variables
    $footer = securite_bdd($_POST['footer']);
    $description = securite_bdd($_POST['description']);
    $keywords = securite_bdd($_POST['keywords']);
    // Traitement
    if ($_SESSION['tokenValidation'] == $_POST['tokenValidation']) {
        if (($footer != "") && ($description != "") && ($keywords != "")) {
            $requete = "UPDATE textes SET valeur = ? WHERE nom = 'footer'";
            $reponse = $bdd->prepare($requete);
            $reponse->bindValue(1, $footer, PDO::PARAM_STR);
            $reponse->execute();
            $reponse->closeCursor();

            $requete = "UPDATE textes SET valeur = ? WHERE nom = 'description'";
            $reponse = $bdd->prepare($requete);
            $reponse->bindValue(1, $description, PDO::PARAM_STR);
            $reponse->execute();
            $reponse->closeCursor();

            $requete = "UPDATE textes SET valeur = ? WHERE nom = 'keywords'";
            $reponse = $bdd->prepare($requete);
            $reponse->bindValue(1, $keywords, PDO::PARAM_STR);
            $reponse->execute();
            $reponse->closeCursor();
            ?>
            <p>Les textes ont bien été sauvegardés.</p>
            <?php

        } else {
            ?>
            <p>Vous n'avez pas rempli tous les champs.</p>
            <?php

        }
    } else {
        ?>
        <script>
            window.location.href = "./";
        </script>
        <?php

    }
} else {
    ?>
    <script>
        window.location.href = "./";
    </script>
    <?php

}
?>